{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Elementary Climate Calculations with SymPy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](./img/lab05-header-bkgd.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "❖ Objectives\n",
    "\n",
    "-   Apply scientific modeling across a series of case studies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Foundations of Climate Modeling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Climate modeling seeks to describe the forward evolution of climate according to mass and energy flows and balances.  Climate can be construed to model any combination of temperature, precipitation, glacial mass, radiation, oceanography, and more.  Climate models vary from \"zeroth dimensional\" energy balances to complex multidimensional grids tracking mass and energy control volumes.\n",
    "\n",
    "![](https://blogs.ei.columbia.edu/wp-content/uploads/2012/02/newglobe-300x240.jpg)\n",
    "\n",
    "(Image courtesy Krajick2012.)\n",
    "\n",
    "(We draw on [Wikipedia](https://en.wikipedia.org/wiki/Climate_model), [IPCC](https://web.archive.org/web/20030325024912/http://www.grida.no/climate/ipcc_tar/wg1/258.htm), and below-cited sources for elements of this lesson.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Zeroth-Dimensional Modeling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A zero-dimensional model considers the Earth as a single point subject to energy flux.  Balancing incoming and outgoing energy leads to this model,\n",
    "\n",
    "$$\n",
    "(1-a)s \\pi r^2\n",
    "= \n",
    "4 \\pi r^2 \\epsilon \\sigma T^4\n",
    "$$\n",
    "\n",
    "where the left-hand side represents solar energy (incoming) and the right-hand side represents radiated black-body heat (outgoing) based on an \"average\" temperature of Earth.  Other parameters are:\n",
    "\n",
    "- $s$, the solar constant, or the incoming solar radiation per unit area.  $s = 1367 \\,\\textrm{W}\\cdot\\textrm{m}^{-2}$.\n",
    "\n",
    "- $a$, the Earth's average albedo (reflectivity).  $a = 0.3$.\n",
    "\n",
    "- $r$, the Earth's radius. $r = 6.371 \\times 10^{6} \\,\\textrm{m}$.\n",
    "\n",
    "- $\\sigma$, the Stefan-Boltzmann constant.  $\\sigma = 5.67 \\times 10^{-8} \\,\\textrm{J}\\cdot\\textrm{K}^{-4}\\cdot\\textrm{m}^{-2}\\cdot\\textrm{s}^{-1}$.\n",
    "\n",
    "- $\\epsilon$, the Earth's effective emissivity.  $\\epsilon = 0.612$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Define the equation using SymPy and symbolic values for each parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy\n",
    "s,a,r,sigma,eps,T = sympy.S('s,a,r,sigma,epsilon,T')\n",
    "\n",
    "lhs = (1-a)*s*sympy.pi*r**2\n",
    "rhs = 4*sympy.pi*r**2*e*sigma*T**4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Solve the equation for temperature (symbolically).  Since there are four answers, set `T_ans` equal to the positive real one only."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T_ans = sympy.solve(lhs-rhs,T)[1]\n",
    "print(T_ans)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Solve the equation for temperature (numerically).  (If you have not seen how to do this yet in class, you can use `.subs` to substitute a value for a variable in SymPy.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T_final = float(T_ans.subs(s,1367).subs(a,0.3).subs(r,6.371e6).subs(sigma,5.67e-8).subs(eps,0.612))\n",
    "print(T_final)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "calc_freq-test2",
     "locked": true,
     "points": 1,
     "schema_version": 3,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "# it should pass this test---do NOT edit this cell\n",
    "from numpy import isclose\n",
    "assert isclose(T_final,288.15)\n",
    "print('Success!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This widget allows you to manipulate the parameters and see the system response as a result of different albedos, emissivities, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "from math import ceil\n",
    "\n",
    "x = []\n",
    "y = []\n",
    "\n",
    "def plot_temperature(s,a):\n",
    "    r = 6.371e6\n",
    "    sigma = 5.67e-8\n",
    "    eps = 0.612\n",
    "    T = 2**0.5*(-a*s/(eps*sigma) + s/(eps*sigma))**(1/4)/2\n",
    "    x.append(len(x))\n",
    "    y.append(T)\n",
    "    plt.plot(x,y,'r.')\n",
    "    plt.plot(len(x),T,'ro')\n",
    "    plt.text(len(x),T,f'  {T-273:.1f} °C')\n",
    "    plt.xlim((0,ceil(len(x)/100)*100))\n",
    "    plt.ylim((0,400))\n",
    "    plt.show()\n",
    "\n",
    "interact(plot_temperature,s=(0,2500),a=(0,1,0.01))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### One-Dimensional Modeling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A one-dimensional climate model typically expands on the zeroth-dimensional radiative picture by including convection as well.  We will implement a model based on Mann's work (summarized in Miller2020a and Sokolik2007) which introduces a thickness to the atmosphere.\n",
    "\n",
    "Our first model didn't separate the Earth from solar radiation, but modeled it as a single point.  We now introduce an \"atmosphere,\" or at least a \"distance\" between the top of the atmosphere and the bottom (the Earth).  We therefore need to model three energy balances:\n",
    "\n",
    "1.  The top of the atmosphere (with temperature $T_{e}$).\n",
    "2.  The atmosphere itself.\n",
    "3.  The Earth's surface (with temperature $T_{S}$.\n",
    "\n",
    "![](https://www.e-education.psu.edu/meteo469/sites/www.e-education.psu.edu.meteo469/files/lesson05/Earth2Fig.gif)\n",
    "\n",
    "(Image courtesy Miller2020a.)\n",
    "\n",
    "We now have to consider the atmosphere with radiation and convection.  Radiation can be difficult to model because of the scattering and absorption of different wavelengths of energy.\n",
    "\n",
    "![](http://www.severewx.com/Radiation/atmtrans.gif)\n",
    "\n",
    "One assumption we can make when building this model is that the Earth acts as a \"gray body,\" or partway between a [blackbody](https://en.wikipedia.org/wiki/Black_body) (perfect radiator) and \"white body\" (perfect reflector).  This makes things easier because then all of the radiation scatters the same way (and we don't have to model frequency as well).  Furthermore, we won't have any absorption in the atmosphere in our model.\n",
    "\n",
    "At the top of the atmosphere, we have the following balance:\n",
    "\n",
    "$$\n",
    "\\frac{s(1-a)}{4}\n",
    "=\n",
    "\\sigma \\epsilon T_{e}^{4} + (1-\\epsilon)\\sigma T_{S}^{4}\n",
    "$$\n",
    "\n",
    "which is a balance of incoming radiation on the left and outgoing radiation on the right.\n",
    "\n",
    "The atmospheric layer has the following balance:\n",
    "\n",
    "$$\n",
    "\\sigma \\epsilon T_{S}^{4}\n",
    "=\n",
    "2\\sigma\\epsilon T_{e}^{4}\n",
    "\\textrm{.}\n",
    "$$\n",
    "\n",
    "The Earth's surface has the following balance:\n",
    "\n",
    "$$\n",
    "\\frac{s(1-a)}{4} + \\sigma\\epsilon T_{e}^{4}\n",
    "=\n",
    "\\sigma T_{S}^{4}\n",
    "\\textrm{.}\n",
    "$$\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Solve for $T_{S}$ and $T_{e}$ using SympPy (and perhaps some algebra by hand) and symbolic values for each parameter.  You should be able to solve for $T_{S}$ without reference to $T_{e}$, and for $T_{e}$ as an equation in $T_{S}$.  Select positive real solutions whenever a choice arises."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "p2f",
     "locked": false,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "import sympy\n",
    "s,a,sigma,eps,Te,Ts = sympy.S('s,a,sigma,epsilon,T_e,T_s')\n",
    "\n",
    "toa_lhs = (1-a)*s/4\n",
    "toa_rhs = sigma*eps*Te**4 + (1-eps)*sigma*Ts**4\n",
    "\n",
    "atm_lhs = sigma*eps*Ts**4\n",
    "atm_rhs = 2*sigma*eps*Te**4\n",
    "\n",
    "sur_lhs = (1-a)*s/4 + sigma*eps*Te**4\n",
    "sur_rhs = sigma*eps*Ts**4\n",
    "\n",
    "Te_1 = sympy.solve(atm_lhs-atm_rhs,Te)\n",
    "Ts_1 = sympy.solve((toa_lhs-toa_rhs).subs(Te,Te_1[1]),Ts)\n",
    "\n",
    "print(Ts_1[3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "p2f-test",
     "locked": true,
     "points": 1,
     "schema_version": 3,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "# it should pass this test---do NOT edit this cell\n",
    "from numpy import isclose\n",
    "test_Ts_1 = 215.4856\n",
    "assert isclose(float(Ts_1[3].subs(a,0.78).subs(eps,0.77).subs(sigma,5.67e-8).subs(s,1367)),test_Ts_1)\n",
    "print('Success!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This widget allows you to manipulate the parameters and see the system response as a result of different albedos and emissivities, etc.  In particular, try these cases:\n",
    "\n",
    "1.  $\\epsilon = 0$ (no greenhouse effect)\n",
    "2.  $\\epsilon = 1$ (perfect greenhouse effect, fully absorptive atmosphere)\n",
    "3.  $\\epsilon = 0.77$ (\"actual\" greenhouse effect on Earth)\n",
    "4.  Mercury:  $a = 0.06$\n",
    "5.  Venus:  $a = 0.78$\n",
    "6.  Earth:  $a = 0.78$\n",
    "7.  Mars:  $a = 0.17$\n",
    "8.  Jupiter:  $a = 0.45$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "from math import ceil\n",
    "\n",
    "x = []\n",
    "y1 = []\n",
    "y2 = []\n",
    "\n",
    "def plot_temperature(s,a):\n",
    "    r = 6.371e6\n",
    "    sigma = 5.67e-8\n",
    "    eps = 0.612\n",
    "    Ts = ((a-1)*s/(2*eps*sigma - 4*sigma))**(1/4)\n",
    "    Te = Ts/2**0.25\n",
    "    x.append(len(x))\n",
    "    y1.append(Ts)\n",
    "    y2.append(Te)\n",
    "    plt.plot(x,y1,'r.')\n",
    "    plt.plot(x,y2,'b.')\n",
    "    plt.plot(len(x),Ts,'ro')\n",
    "    plt.plot(len(x),Te,'bo')\n",
    "    plt.text(len(x),Ts,f'  {Ts-273:.1f} °C')\n",
    "    plt.text(len(x),Te,f'  {Te-273:.1f} °C')\n",
    "    plt.xlim((0,ceil(len(x)/100)*100))\n",
    "    plt.ylim((0,400))\n",
    "    plt.show()\n",
    "\n",
    "interact(plot_temperature,s=(0,2500),a=(0,1,0.01))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Three-Dimensional Global Climate Models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Full climate modeling most commonly slices the Earth's atmosphere and surface layers into many tiny prisms (\"cells\").  Each prism is modeled with a temperature, a flux rate, a heat capacity, etc.  This modeling involves many kinds of physics:\n",
    "\n",
    "- fluid mechanics, to capture convection and turbulence within the atmosphere and the oceans\n",
    "    - meteorology, to capture precipitation and atmospheric capacity\n",
    "- heat transfer, to describe transfer between model cells\n",
    "- radiative modeling, including varying rates of absorption by different gases and reflection by diverse surface types\n",
    "- mass transfer, to describe water and gas transfer\n",
    "\n",
    "![](https://www.gfdl.noaa.gov/wp-content/uploads/pix/model_development/climate_modeling/climatemodel.png)\n",
    "\n",
    "(Image courtesy NOAA.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### References"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Krajick2012](https://blogs.ei.columbia.edu/2012/02/17/global-climate-modeling-for-the-masses-you-can-try-this-at-home/)\n",
    "\n",
    "- [Manabe1964](https://journals.ametsoc.org/doi/abs/10.1175/1520-0469%281964%29021%3C0361%3ATEOTAW%3E2.0.CO%3B2)  S. Manabe, R. F. Strickler, 1964:  Thermal Equilibrium of the Atmosphere with a Convective Adjustment.  _Journal of the Atmospheric Sciences 24,_ 241–259.  doi:\n",
    "\n",
    "- [Manabe1967](https://journals.ametsoc.org/doi/abs/10.1175/1520-0469(1967)024%3C0241%3ATEOTAW%3E2.0.CO%3B2)  S. Manabe, R. T. Wetherald, 1967:  Thermal Equilibrium of the Atmosphere with a Given Distribution of Relative Humidity.  _Journal of the Atmospheric Sciences 21,_ 361–385.  doi:10.1175/1520-0469(1964)021<0361:TEOTAW>2.0.CO;2\n",
    "\n",
    "\n",
    "- [Miller2020a](https://www.e-education.psu.edu/meteo469/node/198)\n",
    "\n",
    "- [Miller2020b](https://www.e-education.psu.edu/meteo469/node/212)\n",
    "\n",
    "- [Sokolik2007](http://irina.eas.gatech.edu/EAS8803_FALL2007/), [Lecture 24](http://irina.eas.gatech.edu/EAS8803_FALL2007/Lecture24.pdf)\n",
    "\n",
    "see also Wang1976 and Wang1980\n",
    "\n",
    "- [Wang1976](https://science.sciencemag.org/content/194/4266/685).  W. C. Wang, Y. L. Yung, A. A. Lacis, T. Mo, J. E. Hansen, 1976: Greenhouse Effects due to Man-Made Perturbations of Trace Gases.  _Science 194_ (4266), 685–690.  doi:10.1126/science.194.4266.685\n",
    "\n",
    "- [Wang1980](https://pubs.giss.nasa.gov/abs/wa03100m.html).  W. C. Wang, P.H. Stone, 1980: Effect of ice-albedo feedback on global sensitivity in a one-dimensional radiative-convective climate model. _J. Atmos. Sci., 37_, 545–552, doi:10.1175/1520-0469(1980)037<0545:EOIAFO>2.0.CO;2.-   Which language is the worst match, and its value of $f$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
