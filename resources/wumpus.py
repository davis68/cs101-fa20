help_msg = '''
Hunt the Wumpus
---------------
Controls:

m # = move to room number (e.g., "m 5")
s # = shoot into up to five room numbers (e.g., "s 2,3,4,3,2")
h   = display this help message
q,x = quit, exit
'''

def help():
    print(help_msg)

def place_obstacles(cave):
    from numpy.random import choice
    bat0,bat1,pit0,pit1 = choice(list(cave.keys()),4,replace=False)
    return bat0,bat1,pit0,pit1
    
def move_wumpus(cave,wumpus):
    # Move the Wumpus randomly.
    from numpy.random import choice
    new_room = choice([wumpus] + cave[wumpus]['cnxn'])
    return new_room

def make_cave():
    '''
    Make a cave.  A cave is a dodecahedron of rooms, with each room carrying
    an attribute such as bats or a pit.
    '''
    cave = { 1: [2,10,11],  2: [1,3,12],   3: [2,4,13],   4: [3,5,13],
             5: [4,6,15],   6: [5,7,16],   7: [6,8,17],   8: [7,9,18],
             9: [8,10,19], 10: [1,9,20],  11: [1,13,19], 12: [2,14,20],
            13: [3,11,15], 14: [4,12,16], 15: [5,13,17], 16: [6,14,18],
            17: [7,15,19], 18: [8,16,20], 19: [9,11,17], 20: [10,12,18]}
    return cave

def move_hunter(cave,hunter,action):
    tokens = action.split()
    if len(tokens) != 2:
        print('Invalid move, please try again.')
        return hunter
    try:
        target = int(tokens[1])
    except TypeError:
        print('Invalid room, please try again.')
        return False,None
    if target not in cave[hunter]:
        print('Invalid move, that room is not connected.')
        return hunter
    return target

def shoot_arrow(cave,hunter,wumpus,action):
    tokens = action.split()
    if len(tokens) != 2:
        print('Invalid shot, please try again.')
    try:
        target = int(tokens[1])
    except TypeError:
        print('Invalid room, please try again.')
    if target not in cave[hunter]:
        print('Invalid move, that room is not connected.')
    if target == wumpus:
        print('Frabjous day!  Thou hast slain the Wumpus and freed these lands from its foul presence.  Thou are hailed a knight of the realm!')
        exit()
    else:
        print('Thou hast erred in thy aim.')
        #move_wumpus(cave,wumpus)

def check_room(cave,hunter,wumpus,bats,pits):
    # Check for hunter and wumpus together.
    if hunter == wumpus:
        print('Red eyes gleam in the dark as the Wumpus pounces upon thee, its prey!  No more shalt thou hunt the Wumpus for it hath hunted thee.')
        exit()
    # Check for bats.
    if hunter in bats:
        print('Thou hearest a whistling and skittering, then claws grab thee and wrench thee suddenly airborne.  Thou art cast into a different room by giant bats!')
        from numpy.random import choice
        hunter = choice(list(cave.keys()))
        hunter = check_room(cave,hunter,wumpus,bats,pits)
    # Check for bottomless pits.
    if hunter in pits:
        print('As thou stepp\'st forward in the dark, a lurch in thy stomach reveals that there is no sure footing underneath!  Thou hast fallen into a bottomless pit, and shalt fall forever.')
        exit()
    from numpy import any
    # Check for adjacent to Wumpus.
    if wumpus in cave[hunter]:
        print('A stench most foul and unseemly permeates the air.')
    # Check for adjacent to bats.
    if bats[0] in cave[hunter] or bats[1] in cave[hunter]:
        print('Thou hearest the high-pitched squeaks of bats.')
    # Check for adjacent to a bottomless pit.
    if pits[0] in cave[hunter] or pits[1] in cave[hunter]:
        print('A chill wind drafts through this cavern.')
    return hunter
    

def main():
    # Initialize game
    cave = make_cave()
    bats_and_pits = place_obstacles(cave)
    bats = bats_and_pits[:2]
    pits = bats_and_pits[2:]
    
    # Main event loop
    open_rooms = list(filter(lambda x: x not in bats_and_pits,list(cave.keys())))
    from numpy.random import choice
    hunter,wumpus = choice(open_rooms,2,replace=False)
    
    while True:
        # Process input.
        hunter = check_room(cave,hunter,wumpus,bats,pits)
        print(f'Thou art in room {hunter}.  Thou canst espy passages to rooms {cave[hunter]}.')
        action = input('? ').lower()
        if action[0] == 'h':
            # Display help message.
            help()
        elif action[0] == 'm':
            # Move the hunter.
            hunter = move_hunter(cave,hunter,action)
        elif action[0] == 's':
            # Shoot the arrow.
            shoot_arrow(cave,hunter,wumpus,action)
        elif action[0] in ('q','x'):
            print('Thou mayest return anytime to hunt the foul Wumpus, brave hunter!')
            break
        else:
            print('Invalid action.  Press "h" for help.')

if __name__ == '__main__':
    main()
