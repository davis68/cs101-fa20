from math import log as ln

def present_amount( A0,p,n ):
    '''
    Calculate money after compounding A0 money for n days at p percent interest.
    '''
    return A0 * ( 1 + p / ( 360.0 * 100 ) ) ** n

def initial_amount( A,p,n ):
    return A * ( 1 + p / ( 360.0 * 100 ) ) ** -n

def days( A0,A,p ):
    return ln( A/A0 ) / ln( 1 + p / ( 360.0 * 100 ) )

def annual_rate( A0,A,n ):
    return 360 * 100 * ( ( A / A0 ) ** ( 1 / n ) - 1 )

def test_present_amount():
    A0,p,n = 2.0,5,730
    A_expected = 2.213398
    A_computed = present_amount( A0,p,n )
    import numpy as np
    assert np.isclose( A_expected,A_computed )

def test_initial_amount():
    A,p,n = 2.213398,5,730
    A0_expected = 2.0
    A0_computed = initial_amount( A,p,n )
    import numpy as np
    assert np.isclose( A0_expected,A0_computed )

def test_days():
    A,A0,p = 2.213398,2.0,5
    n_expected = 730
    n_computed = days( A0,A,p )
    import numpy as np
    assert np.isclose( n_expected,n_computed )

def test_annual_rate():
    A,A0,n = 2.213398,2.0,730
    p_expected = 5
    p_computed = annual_rate( A0,A,n )
    import numpy as np
    assert np.isclose( p_expected,p_computed )

def main():
    test_present_amount()

if __name__ == '__main__':
    main()

