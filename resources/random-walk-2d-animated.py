import numpy as np

n = 10001
num_steps = 50000   # total iterations
heat = 0.10
starting_guess = np.array( ( ( 5,10 ), ),dtype=np.float64 )
frame_mult = 100
from math import pi
xmin,xmax = -4*pi,4*pi
ymin,ymax = -4*pi,4*pi

def f( xy ):
    x = xy[:,0]
    y = xy[:,1]
    return np.sin( x ) + 0.01 * x ** 2 + 0.01 * y ** 2

# Random walk algorithm begins here. ###############################
xy = starting_guess[ : ]
s = 1000/(n-1)    # step size

best_xy = xy[ : ]
best_f = f( best_xy )
steps = np.zeros( ( num_steps+1,3 ) )
fxy = f( xy )
steps[ 0,: ] = np.hstack( ( xy,np.reshape( fxy,(fxy.shape[0],1) ) ) )
from math import tau  #tau, or 2*pi
for i in range( num_steps+1 ):
    # Take a random step, equal chance in any direction.
    chance = np.random.uniform()
    dir = s*np.array( ( np.cos( chance*tau ),np.sin( chance*tau ) ) )
    trial_xy = xy + dir

    if f( trial_xy ) < best_f:
        # If the solution improves, accept it.
        best_f = f( trial_xy )
        best_xy = trial_xy[ : ]
        xy = trial_xy[ : ]
    else:
        # If the solution does not improve, sometimes accept it.
        chance = np.random.uniform()
        if chance < heat:
            xy = trial_xy[ : ]

    # Log steps for plotting later.
    fxy = f( xy )
    steps[ i,: ] = np.hstack( ( xy[:],np.reshape( fxy[:],(fxy.shape[0],1) ) ) )

####################################################################
# Plot random walk statically.
def f( x,y ):
    return np.sin( x ) + 0.01 * x ** 2 + 0.01 * y ** 2

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = Axes3D( fig )

n = 201
XX = np.linspace( xmin,xmax,n ) # set up a grid in x
X = np.reshape( XX,(XX.shape[0],1) )
YY = np.linspace( ymin,ymax,n ) # set up a grid in x
Y = np.reshape( YY,(YY.shape[0],1) )
XX,YY = np.meshgrid( X,Y )   # make a grid of coordinate points
ZZ = f( XX,YY )
ax.plot_surface( XX,YY,ZZ,rstride=1,cstride=1,cmap=cm.viridis )
ax.plot( steps[ :,0 ],steps[ :,1 ],steps[ :,2 ],'bx' )
best_f = f( best_xy[ :,0 ],best_xy[ :,1 ] )
ax.plot( best_xy[ :,0 ],best_xy[ :,1 ],best_f,'ro' )
plt.show()


####################################################################
# Plot random walk as an animation in time.
def f( x,y ):
    return np.sin( x ) + 0.01 * x ** 2 + 0.01 * y ** 2

import matplotlib.pyplot as plt
from matplotlib import cm
fig,ax = plt.subplots()

n = 201
XX = np.linspace( xmin,xmax,n ) # set up a grid in x
X = np.reshape( XX,(XX.shape[0],1) )
YY = np.linspace( ymin,ymax,n ) # set up a grid in x
Y = np.reshape( YY,(YY.shape[0],1) )
XX,YY = np.meshgrid( X,Y )   # make a grid of coordinate points
ZZ = f( XX,YY )

lh = []
rh = []
ln, = plt.plot( lh,rh,'b.' )

def init():
    ax.set_xlim( 1.1*xmin,1.1*xmax )
    ax.set_ylim( 1.1*ymin,1.1*ymax )
    ax.contourf( XX,YY,ZZ,levels=21,cmap=cm.viridis )
    ax.plot( best_xy[ :,0 ],best_xy[ :,1 ],'ro' )
    return ln,

frame_mult = 10
def update( frame ):
    f = frame * frame_mult
    if f > steps.shape[ 0 ]: return ln,
    lh.append( steps[f,0] )
    rh.append( steps[f,1] )
    #ln.set_data( steps[ f,0 ],steps[ f,1 ] )
    ln.set_data( lh,rh )
    return ln,

from matplotlib.animation import FuncAnimation
ani = FuncAnimation( fig,update,frames=range(num_steps//frame_mult),
                     init_func=init,blit=True,repeat=False,
                     interval=5,save_count=num_steps//frame_mult )

plt.show()

ani.save( f'rwalk-{num_steps}@{frame_mult}.mp4' )
print('ready')