-
    type: PythonCodeQuestion
    title: "Brute Force"
    id: quiz3__brute_force1
    value: 4
    timeout: 30
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Cracking a combination lock

        In a musty corner of an abandoned attic, you have discovered a foot locker.  It's heavy, so it's full, but the leather and wood frame are still holding up after all these years.  What could be inside?

        The foot locker is secured by a combination lock.  It appears to require three numbers in the range $[0,49]$.

        ![](repo:./img/quiz3__combination_lock.png)

        You happen to have a small mechanical device on hand which can spin through each three-number combination rapidly, if only you can write a program to guide it.

        We provide a testing function `check_lock(lock )` which accepts a three-number guess for `lock`.  `check_lock` returns `True` if the combination number matches the internal combination.

        Compose a function `crack_lock()` which scans all possible combinations and returns the correct one as a `tuple` or `list` or `array`.  For instance, one valid guess would be `( 11,13,21 )`; another would be `( 19,20,13 )` or `( 22,45,22 )`.

        **Your submission should include a function `crack_lock()`.**  `check_lock` is already defined for your use.  The code randomly generates a combination, so you should run your code several times to make sure you are searching the entire problem space.


    setup_code: |
        def set_lock():
            from numpy.random import choice
            global __real_value
            __real_value = tuple( choice( [ x for x in range( 50 ) ],3,replace=True ) )
        set_lock()

        from numpy import ndarray as array,allclose
        def check_lock( lock ):
            if not type( lock ) in ( tuple,list,array ):
                raise Exception( 'Combination lock should be a tuple or list or array.' )
            return allclose(lock,__real_value)

    initial_code: |

    names_for_user: [ set_lock,check_lock,__real_value ]

    names_from_user: [ crack_lock,__real_value ]

    test_code: |
        points = 0.0

        try:
            import inspect

            from numpy import isclose
            import numpy as np

            # 10% for function definition
            # test functions exist
            if not inspect.isfunction( crack_lock ):
                feedback.add_feedback( 'You don\'t seem to have defined the correct function `crack_lock`.' )
            sub_args = inspect.getargspec( crack_lock ).args
            if len( sub_args ) != 0:
                feedback.add_feedback( 'Your function `crack_lock` doesn\'t seem to have the correct number of parameters.' )
            points += 0.10

            if points < 0.10:
                feedback.finish( points,'You need to define the proper function first.' )

            # 90% for `crack_lock` correctness
            # Test for a fully random value.
            from numpy import isclose,allclose
            user_answer = feedback.call_user( crack_lock )
            feedback.add_feedback( f'Your code found the answer "{user_answer}".' )
            feedback.add_feedback( f'In this case, the correct PIN is "{__real_value}".' )
            if user_answer is None:
                feedback.add_feedback( "Your function is not returning any value at all for this case.  Make sure that you are considering the entire set of possibilities." )
            if user_answer is not None and allclose(__real_value,user_answer):
                points += 0.3
                feedback.add_feedback( 'Your value for `crack_lock()` is correct for the actual value.  +30%' )
            else:
                feedback.add_feedback( 'Your value for `crack_lock()` is incorrect for a trio of random values.' )

            # Test for repeated values.
            #global __real_value
            __real_value = [np.random.choice([ x for x in range( 50 ) ])] * 3
            user_answer_rep = feedback.call_user( crack_lock )
            feedback.add_feedback( f'Your code found the answer "{user_answer_rep}".' )
            feedback.add_feedback( f'In this case, the correct PIN is "{__real_value}".' )
            if user_answer_rep is None:
                feedback.add_feedback( "Your function is not returning any value at all for this case.  Make sure that you are considering the entire set of possibilities.  Repeated values needed to be considered as well." )
            if user_answer_rep is not None and allclose(__real_value,user_answer_rep):
                points += 0.3
                feedback.add_feedback( 'Your value for `crack_lock()` is correct for the actual value.  +30%' )
            else:
                feedback.add_feedback( "Your function is not correct for a trio of repeated values." )

            # Test for repeated values.
            __real_value = [0,49,np.random.choice(range(50))]
            np.random.shuffle(__real_value)
            user_answer_rep = feedback.call_user( crack_lock )
            feedback.add_feedback( f'Your code found the answer "{user_answer_rep}".' )
            feedback.add_feedback( f'In this case, the correct PIN is "{__real_value}".' )
            if user_answer_rep is None:
                feedback.add_feedback( "Your function is not returning any value at all for this case.  Make sure that you are considering the entire set of possibilities.  Repeated values needed to be considered as well." )
            if user_answer_rep is not None and allclose(__real_value,user_answer_rep):
                points += 0.3
                feedback.add_feedback( 'Your value for `crack_lock()` is correct for the actual value.  +30%' )
            else:
                feedback.add_feedback( "Your function is not correct for a trio of repeated values." )

            if np.isclose( points,1.0 ):
                feedback.finish( points,"Congratulations! Your code passes all of our tests." )
            else:
                feedback.finish( points,"It looks like your code still needs some work to be viable." )

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( points,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        def crack_lock():
            from itertools import product
            for trial in product( range( 50 ),repeat=3 ):
                if check_lock( trial ):
                    return trial
