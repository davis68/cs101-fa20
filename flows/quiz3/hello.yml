-
    type: Page
    id: quiz3__welcome
    content: |

        #   `quiz3` :: Numerical Python

        <div class="alert alert-danger">
        Please read this and all other pages in full before proceeding.  You are responsible for this information.
        </div>

        The goal of this section of the course is that students will be able to embody problem solving strategies as algorithms.  This quiz preview will test your understanding and your ability to complete this task in the allotted time.

        As a refresher, you have learned several methods of solving equations for zeroes, minima, and maxima:

        -   Graphical method
        -   Brute-force search (in `scipy.optimize` and directly)
        -   Hill-climbing (greedy) search
        -   Random walk (Monte Carlo) search
        -   Library tools in `scipy.optimize` such as Newton's method and basin hopping

        You should be prepared to use any one of these to solve the upcoming problems on this quiz.

        Please make sure to read each question carefully before answering.  You may use the Python interpreter to work out your answer.  Not all questions are worth the same amount of points.

        You should be using CBTF Zoom proctoring as part of this quiz.  If you are not logged into CBTF Zoom proctoring, your quiz will not be considered valid.

        You may access and use Python during the exam:

        1.  Use the Python or IPython command line (or Qt Console from Anaconda Navigator).
        2.  Use the Jupyter notebook.
        3.  Use an online service like repl.it or pythonanywhere.com.  (These aren't recommended since they may not have all libraries installed.)
        4.  Use an integrated development environment installed on your own machine, such as Spyder or PyCharm.

        Code questions will provide you feedback if you _submit_ them instead of _saving_ them.  You can use this feature to correct and improve your code during the exam period.  **Make sure you are answering each question with the right type of result:  `float`, function, etc.**

        There is an option at the end of the quiz to upload a record of your working session or notebook.  This will not affect your grade, but may be useful in reviewing your quiz later with a TA if you have trouble understanding a particular exercise.

        You may submit this quiz at the end to see your raw mark, but be careful to not accidentally submit early or your quiz is over immediately.
