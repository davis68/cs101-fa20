{% from "yaml-macros.jinja" import quiz_header,indented_include %}
title: "quiz 3"
description: |
    # quiz 3
rules:
    tags:
    -   regular
    -   conflict1
    -   conflict2
    -   conflict3
    -   notforcredit

    start:
    -
        if_has_role: [instructor, ta]
        may_start_new_session: True
        may_list_existing_sessions: True
        tag_session: notforcredit
    -
        if_signed_in_with_matching_exam_ticket: True
        if_has_role: [student]  # for primary access during exam
        if_after: 2020-11-16 20:00:00
        if_before: 2020-11-16 23:50:00
        if_has_fewer_sessions_than: 1
        may_start_new_session: True
        may_list_existing_sessions: True
        default_expiration_mode: end
        tag_session: conflict1
    -
        if_signed_in_with_matching_exam_ticket: True
        if_has_role: [student]  # for primary access during exam
        if_after: 2020-11-16 19:00:00
        if_before: 2020-11-16 22:50:00
        if_has_fewer_sessions_than: 1
        may_start_new_session: True
        may_list_existing_sessions: True
        default_expiration_mode: end
        tag_session: regular
    -
        if_signed_in_with_matching_exam_ticket: True
        if_has_role: [student]  # for primary access during exam
        if_after: 2020-11-17 08:00:00
        if_before: 2020-11-17 11:50:00
        if_has_fewer_sessions_than: 1
        may_start_new_session: True
        may_list_existing_sessions: True
        default_expiration_mode: end
        tag_session: conflict2
    -
        if_signed_in_with_matching_exam_ticket: True
        if_has_role: [student]  # for primary access during exam
        if_after: 2020-11-19 19:55:00
        if_before: 2020-11-19 23:50:00
        if_has_fewer_sessions_than: 1
        may_start_new_session: True
        may_list_existing_sessions: True
        default_expiration_mode: end
        tag_session: conflict3
    -
        may_start_new_session: False
        may_list_existing_sessions: True

    access:
    -
        if_has_role: [instructor, ta]
        permissions: [view, submit_answer, change_answer, see_answer_before_submission, end_session, see_correctness]
    -
        if_has_role: [unenrolled]
        permissions: [view]
        message: |
            You do not seem to be enrolled in the course website, which is why
            you cannot make changes below. Please verify that you are signed in,
            and then return to the class web page and find the big blue "Enroll"
            button near the top.
    -
        if_has_role: [student]
        if_after: 2020-11-16 20:00:00
        if_before: 2020-11-16 23:50:00
        permissions: [view, submit_answer, change_answer, see_session_time, lock_down_as_exam_session, end_session]
    -
        if_has_role: [student]
        if_after: 2020-11-16 19:00:00
        if_before: 2020-11-16 22:50:00
        permissions: [view, submit_answer, change_answer, see_session_time, lock_down_as_exam_session, end_session]
    -
        if_has_role: [student]
        if_after: 2020-11-17 08:00:00
        if_before: 2020-11-17 11:50:00
        permissions: [view, submit_answer, change_answer, see_session_time, lock_down_as_exam_session, end_session]
    -
        if_has_role: [student]
        if_after: 2020-11-19 19:55:00
        if_before: 2020-11-19 23:50:00
        permissions: [view, submit_answer, change_answer, see_session_time, lock_down_as_exam_session, end_session]
    -
        permissions: []

    grade_identifier: quiz3
    grade_aggregation_strategy: max_grade

    grading:
    # Full-credit deadline
    -
        if_has_tag: regular
        description: "Full Credit"
        due: 2020-11-16 20:50:00
        generates_grade: True
        credit_percent: 100
    # Full-credit deadline
    -
        if_has_tag: conflict1
        description: "Full Credit (Conflict)"
        due: 2020-11-16 21:50:00
        generates_grade: True
        credit_percent: 100
    # Full-credit deadline
    -
        if_has_tag: conflict2
        description: "Full Credit (Conflict)"
        due: 2020-11-17 09:50:00
        generates_grade: True
        credit_percent: 100
    -
        if_has_tag: conflict3
        description: "Full Credit (Conflict)"
        due: 2020-11-19 21:50:00
        generates_grade: True
        credit_percent: 100
    -
        credit_percent: 0

groups:
-
    id: welcome
    shuffle: False
    max_page_count: 1
    pages:
        {{ indented_include( "flows/quiz3/hello.yml",8 ) }}

-
    id: quiz3__eqn_methods
    shuffle: True
    max_page_count: 2
    pages:
        {{ indented_include( "flows/quiz3/eqn_solving0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/eqn_solving1.yml",8 ) }}
        {{ indented_include( "flows/quiz3/eqn_solving2.yml",8 ) }}

-
    id: quiz3__brute
    shuffle: False
    max_page_count: 1
    pages:
        {{ indented_include( "flows/quiz3/brute_force1.yml",8 ) }}

-
    id: quiz3__newton
    shuffle: True
    max_page_count: 2
    pages:
        {{ indented_include( "flows/quiz3/newton0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/newton1.yml",8 ) }}
        {{ indented_include( "flows/quiz3/newton2.yml",8 ) }}

-
    id: quiz3__quad
    shuffle: True
    max_page_count: 2
    pages:
        {{ indented_include( "flows/quiz3/quad0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/quad1.yml",8 ) }}
        {{ indented_include( "flows/quiz3/quad2.yml",8 ) }}

-
    id: quiz3__zeroes
    shuffle: True
    max_page_count: 2
    pages:
        {{ indented_include( "flows/quiz3/zero0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/zero1.yml",8 ) }}

-
    id: quiz3__minima
    shuffle: True
    max_page_count: 3
    pages:
        {{ indented_include( "flows/quiz3/min0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/min1.yml",8 ) }}
        {{ indented_include( "flows/quiz3/min2.yml",8 ) }}
        {{ indented_include( "flows/quiz3/min3.yml",8 ) }}
        {{ indented_include( "flows/quiz3/min4.yml",8 ) }}

-
    id: quiz3__maxima
    shuffle: True
    max_page_count: 2
    pages:
        {{ indented_include( "flows/quiz3/max0.yml",8 ) }}
        {{ indented_include( "flows/quiz3/max1.yml",8 ) }}

-
    id: honorcode #XXX TODO upload
    shuffle: False
    max_page_count: 1
    pages:
        {{ indented_include( "flows/quiz1/upload.yml",8 ) }}
