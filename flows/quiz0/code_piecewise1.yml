-
    type: PythonCodeQuestion
    id: quiz0__code_piecewise1
    value: 2
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Evaluate a triangle wave

        The triangle wave is commonly used in signal processing and 8-bit audio.  The behavior of a triangle wave over one period from $(0,4)$ is

        $$
        f(x)
        =
        \begin{cases}
        x & \text{if}\, 0 \leq x \leq 1 \\
        2-x & \text{if}\, 1 < x \leq 3 \\
        x-4 & \text{if}\, 3 \leq x \leq 4 \\
        0   & \text{otherwise}
        \end{cases}
        \text{.}
        $$

        ![](repo:./img/quiz0__triangle_wave.png)

        <!--
        import numpy as np
        import matplotlib.pyplot as plt
        def triangle(x):
            if 0 <= x <= 1:
                return x
            elif 1 < x <= 3:
                return 2-x
            elif 3 < x <= 4:
                return x-4
            else:
                return 0
        x = np.linspace(-1,5,250)
        y = np.zeros(len(x))
        for i in range(len(x)):
            y[i] = triangle(x[i])
        plt.plot(x,y)
        plt.show()
        -->

        **Compose a function `triangle` which accepts a value `x` and returns the corresponding triangle-wave response.**

        Your submission should include a function named `triangle`.


    setup_code: |

    names_for_user: []

    names_from_user: [ triangle ]

    test_code: |
        import inspect
        from numpy import isclose

        points = 0.0

        # test functions exist
        try:
            if (triangle is None) or not inspect.isfunction( triangle ):
                feedback.add_feedback( 'You don\'t seem to have defined the correct function `triangle`.' )
                raise TypeError
            else:
                feedback.add_feedback( 'You have defined a function `triangle`.' )
                points += 0.10
        except TypeError:
            feedback.finish( points,'You need to define the proper function first.  Make sure you\'re using the name specified in the problem statement.' )
        sub_args = inspect.getargspec( triangle ).args
        if len( sub_args ) != 1:
            feedback.add_feedback( 'Your function `triangle` doesn\'t seem to have the correct number of parameters.' )
        else:
            feedback.add_feedback( 'Your function `triangle` has the correct number of parameters.' )
            points += 0.10

        if points < 0.20:
            feedback.finish( points,'You need to define the proper function first.' )

        from math import pi
        def test_triangle(x):
            if 0 <= x <= 1:
                return x
            elif 1 < x <= 3:
                return 2-x
            elif 3 < x <= 4:
                return x-4
            else:
                return 0

        try:
            if not isclose( feedback.call_user( triangle,-0.5 ),test_triangle( -0.5 ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(-0.5)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(-0.5)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,0.0 ),test_triangle( 0.0 ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(-0.0)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(-0.0)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,0.5 ),test_triangle( 0.5 ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(0.5)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(0.5)`.' )
                points += 0.1
            from math import pi
            if not isclose( feedback.call_user( triangle,1 ),test_triangle( 1 ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(1)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(1)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,pi/2 ),test_triangle( pi/2 ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(-pi/2)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(pi/2)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,pi ),test_triangle( pi ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(pi)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(pi)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,1.5*pi ),test_triangle( 1.5*pi ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(1.5*pi)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(1.5*pi)`.' )
                points += 0.1
            if not isclose( feedback.call_user( triangle,2*pi ),test_triangle( 2*pi ),rtol=1e-6 ):
                feedback.add_feedback( '`triangle` is incorrect for `triangle(2*pi)`.' )
            else:
                feedback.add_feedback( '`triangle` is correct for `triangle(2*pi)`.' )
                points += 0.1

        except (NameError) as err:
            feedback.finish( points,'%s\nYou refer to a variable or function that doesn\'t exist.' % err )
        except (ValueError,TypeError) as err:
            feedback.finish( points,'%s\nYou seem to be using something in the wrong way.' % err )
        except Exception as err:
            feedback.finish( points,'%s\nYour code doesn\'t seem to be executable.'%err )

        if points < 1.0:
            feedback.finish( points,'Check your expression defining `triangle`.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )


    correct_code: |
        def triangle(x):
            if 0 <= x <= 1:
                return x
            elif 1 < x <= 3:
                return 2-x
            elif 3 < x <= 4:
                return x-4
            else:
                return 0
