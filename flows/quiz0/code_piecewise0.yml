-
    type: PythonCodeQuestion
    id: quiz0__code_piecewise0
    value: 2
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Evaluate a square wave

        The square wave is commonly used in signal processing and 8-bit audio.  The behavior of a square wave over one period is

        $$
        f(x)
        =
        \begin{cases}
        -1            & \text{if}\, 0 \leq x \leq \pi \\
        \hphantom{-}1 & \text{if}\, \pi < x \leq 2\pi \\
        \hphantom{-}0 & \text{otherwise}
        \end{cases}
        \text{.}
        $$

        ![](repo:./img/quiz0__square_wave.png)

        <!--
        import numpy as np
        import matplotlib.pyplot as plt
        from math import pi
        def square( x ):
            if 0 <= x <= pi:
                return -1
            elif pi < x <= 2*pi:
                return +1
            else:
              return 0
        x = np.linspace(-1,2*pi+1,250)
        y = np.zeros(len(x))
        for i in range(len(x)):
            y[i] = square(x[i])
        plt.plot(x,y)
        plt.show()
        -->

        **Compose a function `square` which accepts a value `x` and returns the corresponding square-wave response.**

        Your submission should include a function named `square`.


    setup_code: |

    names_for_user: []

    names_from_user: [ square ]

    test_code: |
        import inspect
        from numpy import isclose

        points = 0.0

        # test functions exist
        try:
          if (square is None) or not inspect.isfunction( square ):
              feedback.add_feedback( 'You don\'t seem to have defined the correct function `square`.' )
              raise TypeError
          else:
              feedback.add_feedback( 'You have defined a function `square`.' )
              points += 0.10
        except TypeError:
            feedback.finish( points,'You need to define the proper function first.  Make sure you\'re using the name specified in the problem statement.' )
        sub_args = inspect.getargspec( square ).args
        if len( sub_args ) != 1:
            feedback.add_feedback( 'Your function `square` doesn\'t seem to have the correct number of parameters.' )
        else:
            feedback.add_feedback( 'Your function `square` has the correct number of parameters.' )
            points += 0.10

        if points < 0.20:
            feedback.finish( points,'You need to define the proper function first.' )

        if isclose(feedback.call_user(square,0),0,rtol=1e-6):
            from math import pi
            def test_square( x ):
                if 0 < x <= pi:
                    return -1
                elif pi < x <= 2*pi:
                    return +1
                else:
                    return 0

            try:
                if not isclose( feedback.call_user( square,-0.5 ),test_square( -0.5 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(-0.5)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(-0.5)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,0.0 ),test_square( 0.0 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(0.0)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(0.0)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,0.5 ),test_square( 0.5 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(0.5)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(0.5)`.' )
                    points += 0.1
                from math import pi
                if not isclose( feedback.call_user( square,1 ),test_square( 1 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(1)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(1)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,pi/2 ),test_square( pi/2 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(-pi/2)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(pi/2)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,pi ),test_square( pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(pi)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,1.5*pi ),test_square( 1.5*pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(1.5*pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(1.5*pi)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,2*pi ),test_square( 2*pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(2*pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(2*pi)`.' )
                    points += 0.1

            except (NameError) as err:
                feedback.finish( points,'%s\nYou refer to a variable or function that doesn\'t exist.' % err )
            except (ValueError,TypeError) as err:
                feedback.finish( points,'%s\nYou seem to be using something in the wrong way.' % err )
            except Exception as err:
                feedback.finish( points,'%s\nYour code doesn\'t seem to be executable.'%err )
        else:
            from math import pi
            def test_square( x ):
                if 0 <= x <= pi:
                    return -1
                elif pi < x <= 2*pi:
                    return +1
                else:
                    return 0

            try:
                if not isclose( feedback.call_user( square,-0.5 ),test_square( -0.5 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(-0.5)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(-0.5)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,0.0 ),test_square( 0.0 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(0.0)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(0.0)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,0.5 ),test_square( 0.5 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(0.5)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(0.5)`.' )
                    points += 0.1
                from math import pi
                if not isclose( feedback.call_user( square,1 ),test_square( 1 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(1)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(1)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,pi/2 ),test_square( pi/2 ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(-pi/2)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(pi/2)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,pi ),test_square( pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(pi)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,1.5*pi ),test_square( 1.5*pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(1.5*pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(1.5*pi)`.' )
                    points += 0.1
                if not isclose( feedback.call_user( square,2*pi ),test_square( 2*pi ),rtol=1e-6 ):
                    feedback.add_feedback( '`square` is incorrect for `square(2*pi)`.' )
                else:
                    feedback.add_feedback( '`square` is correct for `square(2*pi)`.' )
                    points += 0.1

            except (NameError) as err:
                feedback.finish( points,'%s\nYou refer to a variable or function that doesn\'t exist.' % err )
            except (ValueError,TypeError) as err:
                feedback.finish( points,'%s\nYou seem to be using something in the wrong way.' % err )
            except Exception as err:
                feedback.finish( points,'%s\nYour code doesn\'t seem to be executable.'%err )

        if points < 1.0:
            feedback.finish( points,'Check your expression defining `square`.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )


    correct_code: |
        from math import pi
        def square( x ):
            if 0 < x <= pi:
                return -1
            elif pi < x <= 2*pi:
                return +1
            else:
                return 0
