-
    type: PythonCodeQuestion
    id: quiz0__code_bessel_j
    value: 2
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Evaluate the Zeroth-Order Bessel Function of the First Kind

        The Bessel functions occur in the solution of differential equations.  They describe, among other things, the modes of a swinging chain.

        The Bessel function of the first kind is

        $$
        J_n(x)
        =
        \sum_{m=0}^\infty \frac{(-1)^m}{m! \Gamma(m+n+1)} {\left(\frac{x}{2}\right)}^{2m+n}
        $$

        where $\Gamma$ `gamma` is a generalized factorial.  $n$ is commonly an integer such as 0 or 1.

        ![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Bessel_Functions_%281st_Kind%2C_n%3D0%2C1%2C2%29.svg/500px-Bessel_Functions_%281st_Kind%2C_n%3D0%2C1%2C2%29.svg.png)

        **Compose a function `j0` (lower-case) which calculates the value of the expression for $J_0$ at a given `x` (i.e., $n=0$).**  You do not need to take the sum very high:  the **first three terms only ($m \in (0, 1, 2)$)** will do for our purposes.  Thus, you do not need a loop.

        You will need the _gamma function_ $\Gamma$, which is already available as `gamma`.  You may import `factorial` from `math`.

        If your expression is correct, then the second term ($m=1$) for an `x` of `0.5` should be about `-0.0625` (term not sum!).

    setup_code: |
        from scipy.special import gamma

    names_for_user: [ gamma ]

    names_from_user: [ j0 ]

    test_code: |
        points = 0.0

        import inspect
        from numpy import isclose

        # test functions exist
        if j0 is None or not inspect.isfunction( j0 ):
            feedback.add_feedback( 'You don\'t seem to have defined the correct function `j0`.' )
        else:
            feedback.add_feedback( 'You have defined a function `j0`.' )
            points += 0.10
        if points < 0.10:
            if type( j0 ) is float:
                feedback.add_feedback( 'Don\'t define a float, define a function.' )
            feedback.finish( points,'You need to define the proper function first.' )
        sub_args = inspect.getargspec( j0 ).args
        if len( sub_args ) != 1:
            feedback.add_feedback( 'Your function `j0` doesn\'t seem to have the correct number of parameters.  Do not import `j0` from a special functions library.' )
        else:
            feedback.add_feedback( 'Your function `j0` has the correct number of parameters.' )
            points += 0.10

        if points < 0.20:
            feedback.finish( points,'You need to define the proper function first.' )

        # test function accuracy
        def test_j0( x ):
            from scipy.special import gamma
            from math import factorial
            term0 = ((-1)**0)/(factorial(0)*gamma(0+1))*(x/2)**(2*0)
            term1 = ((-1)**1)/(factorial(1)*gamma(1+1))*(x/2)**(2*1)
            term2 = ((-1)**2)/(factorial(2)*gamma(2+1))*(x/2)**(2*2)
            return term0+term1+term2

        try:
            if not isclose( feedback.call_user( j0,1 ),test_j0( 1 ),rtol=1e-6 ):
                feedback.add_feedback( '`j0` is incorrect for j0(1).' )
            else:
                feedback.add_feedback( '`j0` is correct for j0(1).' )
                points += 0.2
            if not isclose( feedback.call_user( j0,0.5 ),test_j0( 0.5 ),rtol=1e-6 ):
                feedback.add_feedback( '`j0` is incorrect for j0(0.5).' )
            else:
                feedback.add_feedback( '`j0` is correct for j0(0.5).' )
                points += 0.2
            if not isclose( feedback.call_user( j0,0 ),test_j0( 0 ),rtol=1e-6 ):
                feedback.add_feedback( '`j0` is incorrect for j0(0).' )
            else:
                feedback.add_feedback( '`j0` is correct for j0(0).' )
                points += 0.2
            if not isclose( feedback.call_user( j0,1.5 ),test_j0( 1.5 ),rtol=1e-6 ):
                feedback.add_feedback( '`j0` is incorrect for j0(1.5).' )
            else:
                feedback.add_feedback( '`j0` is correct for j0(1.5).' )
                points += 0.2

        except (NameError) as err:
            feedback.finish( points,'%s\nYou refer to a variable or function that doesn\'t exist.' % err )
        except (ValueError,TypeError) as err:
            feedback.finish( points,'%s\nYou seem to be using something in the wrong way.' % err )

        if points < 1.0:
            feedback.finish( points,'Check your expression defining `j0`.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        def j0( x ):
            from scipy.special import gamma
            from math import factorial
            term0 = ((-1)**0)/(factorial(0)*gamma(0+1))*(x/2)**(2*0)
            term1 = ((-1)**1)/(factorial(1)*gamma(1+1))*(x/2)**(2*1)
            term2 = ((-1)**2)/(factorial(2)*gamma(2+1))*(x/2)**(2*2)
            return term0+term1+term2
