{% from "yaml-macros.jinja" import homework_header %}
{{ homework_header(8,lesson_release=15,due_offset=16) }}

pages:
-
    type: Page
    id: hw08__welcome
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Welcome to CS 101.

        This assignment will seek to test the following objectives:

        `lesson 17`:

        {{ indented_include( "modules/numerics/calculus/objectives.yml",0 ) }}

        `lesson 18`:

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include( "modules/numerics/equation/objectives.yml",0 ) }}


-
    type: PythonCodeQuestion
    title: Fraction
    id: fraction_conversion
    value: 10
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Converting a decimal to a fraction

        Although computers generally represent numbers as decimals, it is often convenient to express values as fractions.  If we desire to convert a decimal to a fraction, we need to follow a two-step process:

        1.  Starting with the number itself, multiply by ten repeatedly until no values remain after the decimal point.  This is the numerator.  Keep the multiplier as the denominator.

        2.  Simplify the fraction.

        For instance, for 0.75:

        1.  Multiply by ten repeatedly:

            $$
            0.75 \times 10 \times 10 = 75
            $$

            Thus the numerator is 75 and the denominator is $10 \times 10 = 100$.  You should do this until there the fractional portion is zero.

        2.  Simplify the fraction:

            1.  Find the factors of the numerator and the denominator.

                $$
                75 \rightarrow \{ 1,3,5,15,25,75 \} \\
                100 \rightarrow \{ 1,2,4,5,10,20,25,50,100 \}
                $$

            2.  Find the greatest common factor of each and divide both by it.

                $$
                75 \div 25 = 3 \\
                100 \div 25 = 4
                $$

                $$
                0.75 \rightarrow \frac{3}{4}
                $$


        Write a function `factors( n )` which accepts a number `n` and returns a `list` containing all of the factors of that number.  (This is a common task.  _Write your own code_—don't just google it!)

        Write a function `fraction( n )` which accepts a decimal number `n` and returns a `tuple` `( numerator,denominator )` of the resulting fraction representation[.](https://en.wikipedia.org/wiki/Metal_umlaut)  <!-- egg -->

        **Your submission should include two functions `factors( n )` and `fraction( n )`.**


    initial_code: |
        def factors( n ):
            pass

        def fraction( n ):
            n_str = str( n )
            decimal_part = n_str[ n_str.find( '.' )+1: ]
            # 1. Multiply by ten repeatedly (to make all of decimal positive).
            numer = n * 10 ** ???
            denom = ???

            # 2. Find factors.
            numer_factors = ???
            denom_factors = ???
            factor = 1
            # ??? find greatest common factor of both numerator and denominator
            # ??? divide both by GCF before returning them
            return ( numer,denom )

    names_for_user: []

    names_from_user: [ factors,fraction ]

    test_code: |
        points = 0.0

        try:
            import inspect

            from numpy import isclose
            import numpy as np

            # test functions exist
            if not inspect.isfunction( factors ):
                feedback.add_feedback( 'You don\'t seem to have defined the correct function `factors`.' )
            points += 0.025
            sub_args = inspect.getargspec( factors ).args
            if len( sub_args ) != 1:
                feedback.add_feedback( 'Your function `factors` doesn\'t seem to have the correct number of parameters.' )
            points += 0.025

            if not inspect.isfunction( fraction ):
                feedback.add_feedback( 'You don\'t seem to have defined the correct function `fraction`.' )
            points += 0.025
            sub_args = inspect.getargspec( fraction ).args
            if len( sub_args ) != 1:
                feedback.add_feedback( 'Your function `fraction` doesn\'t seem to have the correct number of parameters.' )
            points += 0.025

            if points < 0.10:
                feedback.finish( points,'You need to define the proper functions first.' )

            # test function accuracy
            def test_factors( n ):
                n = int( n )
                return [ i for i in range( 1,n//2+1 ) if not n%i ] + [ n ]

            def test_fraction( n ):
                # 1. Multiply by ten repeatedly.
                n_str = str( n )
                decimal_part = n_str[ n_str.find( '.' )+1: ]
                numer = n * 10 ** len( decimal_part )
                denom = 10 ** len( decimal_part )

                # 2. Find factors.
                numer_factors = sorted( test_factors( numer ) )
                denom_factors = sorted( test_factors( denom ) )
                factor = 1
                for i in range( len( numer_factors ) ):
                    if numer_factors[ i ] in denom_factors:
                        factor = numer_factors[ i ]
                numer = int( numer / factor )
                denom = int( denom / factor )
                return ( numer,denom )

            # 30% for `factors`
            from numpy import isclose,allclose
            if not allclose( test_factors( 10 ), feedback.call_user( factors,10 ) ):
                feedback.add_feedback( 'Your values for `factors( 10 )` are incorrect.' )
            points += 0.15
            if not allclose( test_factors( 12 ), feedback.call_user( factors,12 ) ):
                feedback.add_feedback( 'Your values for `factors( 12 )` are incorrect.' )
            points += 0.15

            # 60% for `fraction`
            if not allclose( test_fraction( 0.75 ), feedback.call_user( fraction,0.75 ) ):
                feedback.add_feedback( 'Your values for `fraction( 0.75 )` are incorrect.' )
            points += 0.30
            if not allclose( test_fraction( 0.666 ), feedback.call_user( fraction,0.666 ) ):
                feedback.add_feedback( 'Your values for `fraction( 0.666 )` are incorrect.' )
            points += 0.30

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( points,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your solution.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        def factors( n ):
            factors = []
            n = int( n )
            for i in range( 1,n+1 ):
                if n % i == 0:
                    factors.append( i )
            return factors

        def fraction( n ):
            # 1. Multiply by ten repeatedly.
            n_str = str( n )
            decimal_part = n_str[ n_str.find( '.' )+1: ]
            numer = n * 10 ** len( decimal_part )
            denom = 10 ** len( decimal_part )

            # 2. Find factors.
            numer_factors = sorted( factors( numer ) )
            denom_factors = sorted( factors( denom ) )
            factor = 1
            for i in range( len( numer_factors ) ):
                if numer_factors[ i ] in denom_factors:
                    factor = numer_factors[ i ]
            numer = int( numer / factor )
            denom = int( denom / factor )
            return ( numer,denom )


-
    type: PythonCodeQuestion
    id: hw09__simulate
    value: 10
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Simulating bouncing with different gravity

        For this homework assignment, you will write a simplified model of a bouncing ball using `numpy`. Assume the ball is dropped on Earth under constant acceleration $g = 9.81\frac{m}{s^2}$ from the limit of its atmosphere, a height of $250 \text{km}$. Model the ball's motion for one hour and 15 minutes (include second $0$ and second $4,500$ in your data points). After the initial state, simulate 5,000 updates to the state (for a total of $5,001$ points.) Your simulation should use `float64` `numpy` arrays for time (`t`) and height (`y`). Time should be represented in seconds and height should be represented in meters[.](https://scirate.com/arxiv/2003.14321)  <!-- egg -->

        **Note that because the initial velocity is zero, `y[0]` and `y[1]` will _both_ be 250,000 when you set up the simulation per our numerical scheme.**

        ##   Bouncing

        To simulate bouncing, we'll make some simplifying assumptions (since collision detection can be complicated).  If the ball's height is ever less than or equal to `0`, we will assume that the ball hit the ground before the time step we are simulating and already started bouncing.  You should:

        -   instantly set its height to `0`
        -   update its velocity to 90% (0.9) of its velocity and reverse the direction of travel—in this case, please use the now-current velocity, i.e., `vc[ i ] = -0.9 * vc[ i ]`

        Count the number of times the ball bounces in an integer variable named `bounces`.

        Plotting `y` v. `t` may be useful for you to understand what results your code is producing.

        <div class="alert alert-warning">
        DO NOT try to write your code here.  Debugging it here will be very difficult.  You should write and test your code on your own computer.
        </div>

        Write a program to calculate the problem as specified above.

        **Your submission should include arrays `t` and `y` of the proper dimensions and values, and an `int` named `bounces`.**


    setup_code: |

    names_for_user: []

    names_from_user: [ t,y,bounces ]

    test_code: |
        try:
            import numpy as np

            n = 5000        # number of data points to plot
            start = 0.0     # start time of simulation
            end = 4500.0    # ending time of simulation
            g = -9.81       # acceleration of gravity

            tc = np.linspace( start,end,n+1,dtype=np.float64 )  # time, s
            yc = np.zeros( n+1,dtype=np.float64 )               # height, m
            vc = np.zeros( n+1,dtype=np.float64 )               # velocity, m/s
            yc[ 0 ] = 250000.0     # initialize height

            cbounces = 0
            for i in range( 1,n+1 ):
                vc[ i ] = vc[ i-1 ] + g * ( tc[ i ] - tc[ i-1 ] )
                yc[ i ] = yc[ i-1 ] + vc[ i-1 ]*( tc[ i ] - tc[ i-1 ] )
                if yc[ i ] <= 0.0:
                    cbounces += 1
                    yc[ i ] = 0.0
                    vc[ i ] = -0.9 * vc[ i ]

            tx = np.linspace( start,end,n+1,dtype=np.float64 )  # time, s
            yx = np.zeros( n+1,dtype=np.float64 )               # height, m
            vx = np.zeros( n+1,dtype=np.float64 )               # velocity, m/s
            yx[ 0 ] = 250000.0     # initialize height
            dtx = tx[1]-tx[0]

            xbounces = 0
            for i in range( 1,n+1 ):
                vx[ i ] = vx[ i-1 ] + g * dtx
                yx[ i ] = yx[ i-1 ] + vx[ i-1 ]*dtx
                if yx[ i ] <= 0.0:
                    xbounces += 1
                    yx[ i ] = 0.0
                    vx[ i ] = -0.9 * vx[ i ]

            if not type( t ) is np.ndarray or not type( y ) is np.ndarray:
                feedback.finish( 0,"You must use numpy arrays for t and y." )
            if t.shape!=( 5001, ) or y.shape!=( 5001, ):
                feedback.finish( 0,"Both t and y should be 1 dimensional and contain exactly 5,001 data points." )
            if not np.allclose( t,tc,rtol=0.01 ):
                feedback.finish( 0.5,"Your t values are incorrect." )
            if not type( t[ 0 ] ) is np.float64 or not type( y[ 0 ] ) is np.float64:
                feedback.finish( 0.5,"Both t and y should be float64 arrays." )
            if not ( np.allclose( y,yc,rtol=0.01 ) or np.allclose( y,yx,rtol=0.01 ) ):
                feedback.finish( 0.75,"Your y values are incorrect.  Are you updating from the *previous* time step's velocity as specified?  Is your value for `g` the same as specified in the problem statement?" )
            if not ( bounces == cbounces or bounces == xbounces ):
                feedback.finish( 0.9,"Your count of bounces is incorrect." )

            feedback.add_feedback( "Your simulation data are all correct." )

            feedback.finish( 1.0,"Congratulations! Your code passes all of our tests." )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        # Parameters of simulation
        n=5000          # number of data points to plot
        start=0.0       # start time of simulation
        end=4500.0      # ending time of simulation
        g=-9.81         # acceleration of gravity

        # State variable initialization
        t=np.linspace(start,end,n+1)    # time in seconds
        y=np.zeros(n+1)                 # height in meters
        v=np.zeros(n+1)                 # velocity in m/s
        y[0]=250000.0                   # initialize height to 1.0

        bounces=0
        for i in range(1,n+1):
            v[i]=v[i-1]+g*(t[i]-t[i-1])
            y[i]=y[i-1]+v[i-1]*(t[i]-t[i-1])
            if y[i]<=0.0:
                bounces+=1
                y[i]=0.0
                v[i]=-0.9*v[i]

        plt.plot(t,y,'m:')
        plt.show()


-
    type: PythonCodeQuestion
    id: simulate
    value: 10
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Simulating bouncing with different gravity

        For this homework assignment, you will write a simplified model of a bouncing ball using numpy. Assume the ball is dropped on Venus under constant acceleration $g = 8.87\frac{m}{s^2}$ from the limit of its atmosphere, a height of $250 \text{km}$. Model the ball's motion for one hour and 15 minutes (include second $0$ and second $4,500$ in your data points). After the initial state, simulate 5,000 updates to the state (for a total of $5,001$ points.) Your simulation should use `float64` `numpy` arrays for time (`t`) and height (`y`). Time should be represented in seconds and height should be represented in meters.

        ##   Bouncing

        To simulate bouncing, we'll make some simplifying assumptions (since collision detection can be complicated).  If the ball's height is ever less than or equal to `0`, we will assume that the ball hit the ground before the time step we are simulating and already started bouncing.  You should:

        -   instantly set its height to `0`
        -   update its velocity to 90% (0.9) of its velocity and reverse the direction of travel—in this case, please use the now-current velocity, i.e., `vc[ i ] = -0.9 * vc[ i ]`

        Count the number of times the ball bounces in an integer variable named `bounces`.

        Plotting `y` v. `t` may be useful for you to understand what results your code is producing.

        <div class="alert alert-warning">
        DO NOT try to write your code here.  Debugging it via RELATE will be very difficult.  You should write and test your code on your own computer.
        </div>

        Write a program to calculate the problem as specified above.

        **Your submission should include arrays `t` and `y` of the proper dimensions and values, and an `int` named `bounces`.**


    setup_code: |

    names_for_user: []

    names_from_user: [ t,y,bounces ]

    test_code: |
        try:
            import numpy as np

            n = 5000        # number of data points to plot
            start = 0.0     # start time of simulation
            end = 4500.0    # ending time of simulation
            g = -8.87       # acceleration of gravity

            tc = np.linspace( start,end,n+1,dtype=np.float64 )  # time, s
            yc = np.zeros( n+1,dtype=np.float64 )               # height, m
            vc = np.zeros( n+1,dtype=np.float64 )               # velocity, m/s
            yc[ 0 ] = 250000.0     # initialize height

            cbounces = 0
            for i in range( 1,n+1 ):
                vc[ i ] = vc[ i-1 ] + g * ( tc[ i ] - tc[ i-1 ] )
                yc[ i ] = yc[ i-1 ] + vc[ i-1 ]*( tc[ i ] - tc[ i-1 ] )
                if yc[ i ] <= 0.0:
                    cbounces += 1
                    yc[ i ] = 0.0
                    vc[ i ] = -0.9 * vc[ i ]

            tx = np.linspace( start,end,n+1,dtype=np.float64 )  # time, s
            yx = np.zeros( n+1,dtype=np.float64 )               # height, m
            vx = np.zeros( n+1,dtype=np.float64 )               # velocity, m/s
            yx[ 0 ] = 250000.0     # initialize height
            dtx = tx[1]-tx[0]

            xbounces = 0
            for i in range( 1,n+1 ):
                vx[ i ] = vx[ i-1 ] + g * dtx
                yx[ i ] = yx[ i-1 ] + vx[ i-1 ]*dtx
                if yx[ i ] <= 0.0:
                    xbounces += 1
                    yx[ i ] = 0.0
                    vx[ i ] = -0.9 * vx[ i ]

            if not type( t ) is np.ndarray or not type( y ) is np.ndarray:
                feedback.finish( 0,"You must use numpy arrays for t and y." )
            if t.shape!=( 5001, ) or y.shape!=( 5001, ):
                feedback.finish( 0,"Both t and y should be 1 dimensional and contain exactly 5,001 data points." )
            if not np.allclose( t,tc,rtol=0.01 ):
                feedback.finish( 0.5,"Your t values are incorrect." )
            if not type( t[ 0 ] ) is np.float64 or not type( y[ 0 ] ) is np.float64:
                feedback.finish( 0.5,"Both t and y should be float64 arrays." )
            if not ( np.allclose( y,yc,rtol=0.01 ) or np.allclose( y,yx,rtol=0.01 ) ):
                feedback.finish( 0.75,"Your y values are incorrect.  Are you updating from the *previous* time step's velocity as specified?" )
            if not ( bounces == cbounces or bounces == xbounces ):
                feedback.finish( 0.9,"Your count of bounces is incorrect." )

            feedback.add_feedback( "Your simulation data are all correct." )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        # Parameters of simulation
        n=5000          # number of data points to plot
        start=0.0       # start time of simulation
        end=4500.0      # ending time of simulation
        g=-8.87         # acceleration of gravity

        # State variable initialization
        t=np.linspace(start,end,n+1)    # time in seconds
        y=np.zeros(n+1)                 # height in meters
        v=np.zeros(n+1)                 # velocity in m/s
        y[0]=250000.0                   # initialize height to 1.0

        bounces=0
        for i in range(1,n+1):
            v[i]=v[i-1]+g*(t[i]-t[i-1])
            y[i]=y[i-1]+v[i-1]*(t[i]-t[i-1])
            if y[i]<=0.0:
                bounces+=1
                y[i]=0.0
                v[i]=-0.9*v[i]

        plt.plot(t,y,'m:')
        plt.show()
