{% from "yaml-macros.jinja" import homework_header %}
{{ homework_header(6,lesson_release=11,due_offset=16) }}

pages:
-
    type: Page
    id: hw06__welcome
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Welcome to CS 101.

        This assignment will seek to test the following objectives:

        `lesson 13`:

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include( "modules/numerics/plotting/objectives.yml",0 ) }}

        `lesson 14`:

        {{ indented_include( "modules/numerics/vector/objectives.yml",0 ) }}


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_1
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Fill arrays with function values

        Define

        $$
        h(x)
        =
        \frac{1}{\sqrt{2\pi}} \exp\left( -\frac{1}{2}x^2 \right)
        \text{.}
        $$

        Write a function `h( x )` for computing $h(x)$ according to the formula given above.  (`x` may be a `float` or an `array`.  Write your code so that it can handle both of these.  This doesn't need an `if` statement or anything, just you to be aware of possible input values.)

        Next, use `np.linspace` to create an array `xarray` for 41 uniformly spaced $x$ coordinates, $x \in [-4,4]$..  Then create `harray` for $h(x)$ values.

        **Your submission should include definitions for function `h` and arrays `xarray` and `harray`.**  Many are still confusing _printing_ a value and _returning_ a value.  Don't confuse those!

        <div class="alert alert-info">
        Keep in mind throughout that NumPy arrays require NumPy functions, not those from <code>math</code>.
        </div>

        (This assignment is based on Langtangen, Exercise 5.1.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ h,xarray,harray ]

    test_code: |
        try:
            import inspect

            # test functions exist
            if not inspect.isfunction( h ):
                feedback.finish( 0.0,'You don\'t seem to have defined the correct function.' )

            # test functions have correct parameters
            sub_args = inspect.getargspec( h ).args
            if len( sub_args ) != 1:
                feedback.finish( 0.0,'Your function doesn\'t seem to have the correct number of parameters.' )

            # test function accuracy
            from numpy import isclose
            import numpy as np

            def test_h( x ):
                from math import pi,sqrt
                from numpy import exp
                result = 1/sqrt(2*pi) * exp( -0.5*x**2 )
                return result

            test_xarray = np.linspace( -4,4,41 )
            test_harray = test_h( test_xarray )

            if not isclose( test_h( 0.5 ), feedback.call_user( h,0.5 ) ):
                feedback.finish( 0.0,'Your function `h( 0.5 )` doesn\'t seem to be working correctly.' )
            if not np.allclose( test_xarray,xarray ):
                feedback.finish( 1./3,'Your values for the set of `xarray` points are incorrect.' )
            if not np.allclose( test_harray,harray ):
                feedback.finish( 2./3,'Your values for the set of `harray` points are incorrect.' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np

        def h( x ):
            from math import pi,sqrt
            from numpy import exp
            result = 1/sqrt(2*pi) * exp( -0.5*x**2 )
            return result

        xarray = np.linspace( -4,4,41 )
        harray = h( xarray )


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_4
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Plot a function

        Make a plot of the function `h` over the range $x \in [-4,4]$ with 41 points.

        $$
        h(x)
        =
        \frac{1}{\sqrt{2\pi}} \exp\left( -\frac{1}{2}x^2 \right)
        \text{.}
        $$

        You will need to:

        -   plot the data using blue circles (NOT dots)
        -   change the plot range to match the specification above (use the `plt.xlim( a,b )` function, where you provide `a` and `b`)
        -   add a title to your plot, `'Formula Plot'`
        -   add axes labels to your plot, `'x'` and `'h(x)'`

        **Your submission should include a plot.  Make sure to `import matplotlib.pyplot as plt`.  Use `plt.plot` but _not_ `plt.show` in your submission.**  You will need to include the code from the previous exercise in order to calculate the values to be plotted.

        While there are several ways to specify colors and markers (`linecolor='red'`), for CS 101 homework assignments and exams, please use the `'rx'`-style short-format specification.

        <div class="alert alert-info">
        Keep in mind throughout that NumPy arrays require NumPy functions, not those from <code>math</code>.
        </div>

        (This assignment is based on Langtangen, Exercise 5.4.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ plt ]

    test_code: |
        try:
            import inspect

            from numpy import isclose
            import numpy as np

            if not plt:
                feedback.finish( 0.0,'You don\'t seem to have defined a plot.  Did you import MPL with the name `plt`?' )

            a = plt.gca()
            xlab = a.get_xaxis().get_label_text()
            ylab = a.get_yaxis().get_label_text()
            tit = a.get_title()
            xlim = a.get_xlim()

            if 'formula' in tit.lower() and 'plot' in tit.lower():
                feedback.add_feedback( 'Your title is labeled correctly.' )
            else:
                feedback.finish( 0.10,'Your title is labeled incorrectly.' )
            if 'x' == xlab.lower() and 'h(x)' == ylab.lower():
                feedback.add_feedback( 'Your axes are labeled correctly.' )
            else:
                feedback.finish( 0.20,'Your axes are labeled incorrectly.' )
            if np.all( isclose( (-4,4),xlim ) ):
                feedback.add_feedback( 'Your x-limits are correct.' )
            else:
                feedback.finish( 0.30,'Your x-limits are incorrect.' )

            lines = a.get_lines()
            if len( lines ) != 1:
                feedback.finish( 0.40,"You should plot exactly 1 line." )
            line = lines[ 0 ]
            marker = line.get_marker()
            color = line.get_color()
            if marker == "o" and (color == "b" or color == "blue"):
                feedback.add_feedback( 'Your plot markers are correct.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect.' )

            # test function accuracy

            def test_h( x ):
                from math import pi,sqrt
                from numpy import exp
                result = 1/sqrt(2*pi) * exp( -0.5*x**2 )
                return result

            test_xarray = np.linspace( -4,4,41 )
            test_harray = test_h( test_xarray )

            d = line.get_data()
            d[ 0 ].sort()
            d[ 1 ].sort()

            test_xarray.sort()
            test_harray.sort()

            if np.allclose( d[ 0 ],test_xarray ) and np.allclose( d[ 1 ],test_harray ):
                feedback.add_feedback( 'You\'ve plotted the correct data.' )
            else:
                feedback.finish( 2./3, 'You\'ve plotted incorrect data.' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        def h( x ):
            from math import pi,sqrt
            from numpy import exp
            result = 1/sqrt(2*pi) * exp( -0.5*x**2 )
            return result

        xarray = np.linspace( -4,4,41 )
        harray = h( xarray )

        plt.plot( xarray,harray,'bo' )
        plt.xlabel( 'x' )
        plt.ylabel( 'h(x)' )
        plt.title( 'Formula Plot' )
        plt.xlim( -4,4 )


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_9
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Plot a formula

        Make a plot of the function

        $$
        y(t)
        =
        v_0 t - \frac{1}{2} g t^{2}
        $$

        for $v_0 = 10$, $g = 9.81$, and $t \in [ 0,2 v_0/g ]$ with 21 points.

        You will need to[:](https://www.youtube.com/watch?v=zPGf4liO-KQ)  <!-- egg -->

        -   plot the data using a red solid line
        -   change the plot range to match the specification above (use the `plt.xlim( a,b )` function, where you provide `a` and `b`)
        -   add axes labels to your plot, `'time / s'` and `'height / m'`

        **Your submission should include a plot.  Make sure to `import matplotlib.pyplot as plt`.  Use `plt.plot` but _not_ `plt.show` in your submission.**

        (This assignment is based on Langtangen, Exercise 5.9.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ plt ]

    test_code: |
        try:
            import inspect

            from numpy import isclose
            import numpy as np

            v_0 = 10
            g = 9.81
            tlim = ( 0,2 * v_0 / g )

            if not plt:
                feedback.finish( 0.0,'You don\'t seem to have defined a plot.  Did you import MPL with the name `plt`?' )

            a = plt.gca()
            xlab = a.get_xaxis().get_label_text()
            ylab = a.get_yaxis().get_label_text()
            xlim = a.get_xlim()

            if 'time/s' == xlab.lower().replace( ' ','' ) and 'height/m' == ylab.lower().replace( ' ','' ):
                feedback.add_feedback( 'Your axes are labeled correctly.' )
            else:
                feedback.finish( 0.20,'Your axes are labeled incorrectly.' )
            if np.all( isclose( tlim,xlim ) ):
                feedback.add_feedback( 'Your x-limits are correct.' )
            else:
                feedback.finish( 0.40,'Your x-limits are incorrect.' )

            lines = a.get_lines()
            if len( lines ) != 1:
                feedback.finish( 0.40,"You should plot exactly 1 line." )
            line = lines[ 0 ]
            marker = line.get_linestyle()
            color = line.get_color()
            if marker == "-" and (color == "r" or color == "red"):
                feedback.add_feedback( 'Your plot markers are correct.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect.' )

            # test function accuracy
            from numpy import isclose
            import numpy as np

            t = np.linspace( 0,2*v_0/g,21 )
            y = v_0 * t - 0.5 * g * t ** 2

            d = line.get_data()
            d[ 0 ].sort()
            d[ 1 ].sort()

            t.sort()
            y.sort()

            if np.allclose( d[ 0 ],t ) and np.allclose( d[ 1 ],y ):
                feedback.add_feedback( 'You\'ve plotted the correct data.' )
            else:
                feedback.finish( 2./3, 'You\'ve plotted incorrect data.' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        v_0 = 10
        g = 9.81

        t = np.linspace( 0,2*v_0/g,21 )
        y = v_0 * t - 0.5 * g * t ** 2

        plt.plot( t,y,'r-' )
        plt.xlabel( 'time/s' )
        plt.ylabel( 'height/m' )
        plt.xlim( 0,2*v_0/g )


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_12mod
    value: 2
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Plot exact and inexact Fahrenheit-Celsius conversion formulas

        A simple rule to quickly compute the Celsius temperature from the Fahrenheit degrees is to subtract 30 and then divide by 2:

        $$
        C
        \approx
        \frac{F-30}{2}
        \text{.}
        $$

        Compare this curve against the exact curve

        $$
        C
        =
        \frac{5}{9} (F-32)
        $$

        in a plot.  Let $F$ vary between -$20$ and $120$ with 101 points.  You should include both the approximate and exact formulae, and the absolute value of their difference (the error).

        You will need to:

        -   plot the data using a black dashed line (`'--'`) for the approximate formula; a black solid line for the accurate formula; and a black dotted line (`':'`) for the error (preserve this order!)
        -   change the plot range to match the specification above (use the `plt.xlim( a,b )` function, where you provide `a` and `b`)
        -   add axes labels to your plot, `'temperature / F'` and `'temperature / C'`

        **Your submission should include a plot.  Make sure to `import matplotlib.pyplot as plt`.  Use `plt.plot` but _not_ `plt.show` in your submission.**

        (This assignment is based on Langtangen, Exercise 5.12.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ plt ]

    test_code: |
        try:
            import inspect

            from numpy import isclose
            import numpy as np

            tlim = ( -20,120 )

            if not plt:
                feedback.finish( 0.0,'You don\'t seem to have defined a plot.  Did you import MPL with the name `plt`?' )

            a = plt.gca()
            xlab = a.get_xaxis().get_label_text()
            ylab = a.get_yaxis().get_label_text()
            xlim = a.get_xlim()

            if 'temperature/f' == xlab.lower().replace( ' ','' ) and 'temperature/c' == ylab.lower().replace( ' ','' ):
                feedback.add_feedback( 'Your axes are labeled correctly.' )
            else:
                feedback.finish( 0.20,'Your axes are labeled incorrectly.' )
            if np.all( isclose( tlim,xlim ) ):
                feedback.add_feedback( 'Your x-limits are correct.' )
            else:
                feedback.finish( 0.40,'Your x-limits are incorrect.' )

            lines = a.get_lines()
            if len( lines ) != 3:
                feedback.finish( 0.40,"You should plot exactly 3 lines." )

            line_approx = lines[ 0 ]
            marker_approx = line_approx.get_linestyle()
            color_approx = line_approx.get_color()
            if marker_approx == "--" and color_approx in ('k',"black"):
                feedback.add_feedback( 'Your plot markers are correct for the approximate formula.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect for the approximate formula.' )

            line_actual = lines[ 1 ]
            marker_actual = line_actual.get_linestyle()
            color_actual = line_actual.get_color()
            if marker_actual == "-" and color_actual in ('k',"black"):
                feedback.add_feedback( 'Your plot markers are correct for the actual formula.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect for the actual formula.' )

            line_error = lines[ 2 ]
            marker_error = line_error.get_linestyle()
            color_error = line_error.get_color()
            if marker_error == ":" and color_error in ('k',"black"):
                feedback.add_feedback( 'Your plot markers are correct for the error formula.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect for the error formula.' )

            # test function accuracy
            from numpy import isclose
            import numpy as np

            def test_approx( F ):
                C = ( F - 30 ) / 2.
                return C
            def test_actual( F ):
                C = 5. / 9 * ( F - 32 )
                return C

            test_F = np.linspace( -20,120,101 )
            test_C1 = test_approx( test_F )
            test_C2 = test_actual( test_F )
            test_err= abs( test_C1-test_C2 )

            d_approx = line_approx.get_data()
            d_approx[ 0 ].sort()
            d_approx[ 1 ].sort()

            d_actual = line_actual.get_data()
            d_actual[ 0 ].sort()
            d_actual[ 1 ].sort()

            d_error = line_error.get_data()
            d_error[ 0 ].sort()
            d_error[ 1 ].sort()

            test_F.sort()
            test_C1.sort()
            test_C2.sort()
            test_err.sort()

            if np.allclose( d_approx[ 0 ],test_F ) and np.allclose( d_approx[ 1 ],test_C1 ) and \
               np.allclose( d_actual[ 0 ],test_F ) and np.allclose( d_actual[ 1 ],test_C2 ) and \
               np.allclose( d_error[ 0 ],test_F ) and np.allclose( d_error[ 1 ],test_err ):
                feedback.add_feedback( 'You\'ve plotted the correct data.' )
            else:
                feedback.finish( 2./3, 'You\'ve plotted incorrect data.' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        def C_approx( F ):
            C = ( F - 30 ) / 2.
            return C
        def C_actual( F ):
            C = 5. / 9 * ( F - 32 )
            return C

        F  = np.linspace( -20,120,101 )
        C1 = C_approx( F )
        C2 = C_actual( F )

        plt.plot( F,C1,'k--' )
        plt.plot( F,C2,'k-' )
        plt.plot( F,abs(C1-C2),'k:' )
        plt.xlabel( 'temperature / F' )
        plt.ylabel( 'temperature / C' )
        plt.xlim( -20,120 )

        plt.show()


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_13
    value: 3
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Plot the trajectory of a ball

        The formula for the trajectory of a ball is given by

        $$
        f(x)
        =
        x \tan \theta - \frac{1}{2 v_0^2} \frac{g x^2}{\cos^2 \theta} + y_0
        $$

        where $x$ is a coordinate along the ground, $g = 9.81$ is the acceleration due to gravity, $v_0$ is the magnitude of the initial velocity which makes an angle $\theta$ with the $x$-axis, and $(0,y_0)$ is the initial position of the ball.

        Write a program to calculate the trajectory $y = f(x)$ for $0 \leq x \leq 16$, with $y_0 = 10$, $v_0 = 10$, $\theta = 50 ^\circ$.  (Do Python trigonometric functions accept radians or degrees?)  Let $x$ vary between $0$ and $16$ with 101 points.

        You will need to:

        -   plot the trajectory data using a blue solid line (`'-'`)
        -   change the plot range to match the specification above for $0 \leq y \leq 10$ and for $x$ (use the `plt.ylim( a,b )` function or its counterpart for `x`, where you provide `a` and `b`)
        -   add axes labels to your plot, `'position / x'` and `'height / y'`

        **Your submission should include a plot.  Make sure to `import matplotlib.pyplot as plt`.  Use `plt.plot` but _not_ `plt.show` in your submission.**

        (This assignment is based on Langtangen, Exercise 5.13.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ plt ]

    test_code: |
        try:
            import inspect

            from numpy import isclose
            import numpy as np

            test_y_0 = 10
            test_x_0 = 16

            if not plt:
                feedback.finish( 0.0,'You don\'t seem to have defined a plot.  Did you import MPL with the name `plt`?' )

            a = plt.gca()
            xlab = a.get_xaxis().get_label_text()
            ylab = a.get_yaxis().get_label_text()
            xlim = a.get_xlim()
            ylim = a.get_ylim()

            if 'position/x' == xlab.lower().replace( ' ','' ) and 'height/y' == ylab.lower().replace( ' ','' ):
                feedback.add_feedback( 'Your axes are labeled correctly.' )
            else:
                feedback.finish( 0.20,'Your axes are labeled incorrectly.' )
            if np.all( isclose( ( 0,test_y_0 ),ylim ) ):
                feedback.add_feedback( 'Your y-limits are correct.' )
            else:
                feedback.finish( 0.40,'Your y-limits are incorrect.' )
            if np.all( isclose( ( 0,test_x_0 ),xlim ) ):
                feedback.add_feedback( 'Your x-limits are correct.' )
            else:
                feedback.finish( 0.40,'Your x-limits are incorrect.' )

            lines = a.get_lines()
            if len( lines ) != 1:
                feedback.finish( 0.40,"You should plot exactly 1 lines." )
            line = lines[ 0 ]

            marker = line.get_linestyle()
            color = line.get_color()
            if marker == "-" and (color == "b" or color == "blue"):
                feedback.add_feedback( 'Your plot markers are correct.' )
            else:
                feedback.finish( 0.50,'Your plot markers are incorrect.' )

            # test function accuracy
            from numpy import isclose
            import numpy as np

            def test_f( x,theta,y_0,v_0 ):
                import numpy as np
                g = 9.81
                y = x * np.tan( theta ) - 1/(2*v_0**2) * (g*x**2)/(np.cos( theta )**2) + y_0
                return y

            test_x     = np.linspace( 0,16,101 )
            test_v_0   = 10
            test_theta = 50 * 2*np.pi/360
            test_y     = test_f( test_x,test_theta,test_y_0,test_v_0 )

            d = line.get_data()
            d[ 0 ].sort()
            d[ 1 ].sort()

            test_x.sort()
            test_y.sort()

            if np.all( np.isclose( d[ 0 ],test_x ) ) and np.all( np.isclose( d[ 1 ],test_y ) ):
                feedback.add_feedback( 'You\'ve plotted the correct data.' )
            else:
                feedback.finish( 2./3, 'You\'ve plotted incorrect data.' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import numpy as np
        import matplotlib.pyplot as plt

        def f( x,theta,y_0,v_0 ):
            import numpy as np
            g = 9.81
            y = x * np.tan( theta ) - 1/(2*v_0**2) * (g*x**2)/(np.cos( theta )**2) + y_0
            return y

        x     = np.linspace( 0,16,101 )
        y_0   = 10
        v_0   = 10
        theta = 50 * 2*np.pi/360
        y     = f( x,theta,y_0,v_0 )

        plt.plot( x,y,'b-' )
        plt.xlabel( 'position / x' )
        plt.ylabel( 'height / y' )
        plt.xlim( 0,16 )
        plt.ylim( 0,10 )


-
    type: PythonCodeQuestion
    id: hw06__hpl_5_23
    value: 3
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Vectorize the midpoint rule for integration

        The midpoint rule for approximating an integral can be expressed as

        $$
        \int_a^b \text{d}x\, f(x)
        \approx
        h \sum_{i=1}^{n} f\left( a - \frac{1}{2}h + ih \right)
        \text{,}
        $$

        where $h = \frac{b-a}{n}$.

        Write a function `midpointint( f,a,b,n )` to compute the midpoint rule using the `numpy` `sum` function.

        Please be careful in generating `i`, which should range from `1` to `n` *inclusive*.  (You could, for instance, use a `range` and convert it to an `array`.)

        **Your submission should include a function `midpointint( f,a,b,n )`.**

        (This assignment is based on Langtangen, Exercise 5.23.)

    setup_code: |

    names_for_user: [ ]

    names_from_user: [ midpointint ]

    test_code: |
        try:
            import inspect

            from numpy import isclose
            import numpy as np

            # test functions exist
            if not inspect.isfunction( midpointint ):
                feedback.finish( 0.0,'You don\'t seem to have defined the correct function.' )

            # test functions have correct parameters
            sub_args = inspect.getargspec( midpointint ).args
            if len( sub_args ) != 4:
                feedback.finish( 0.0,'Your function doesn\'t seem to have the correct number of parameters.' )

            # test function accuracy
            from numpy import isclose
            def test_midpointint( f,a,b,n ):
                import numpy as np
                result = 0
                h = (b-a)/n
                i = np.array( range( 1,n+1 ) )
                result = h * np.sum( f( a - 0.5*h + i*h ) )
                return result

            if not isclose( test_midpointint( lambda x:2*x,2,4,1 ), feedback.call_user( midpointint,lambda x:2*x,2,4,1 ) ):
                feedback.finish( 0.0,'Your value for `midpointint( line,2,4,1 )` is incorrect.  (`line` is a function `2*x`; the expected value is 12.)' )

            if not isclose( test_midpointint( np.sin,0,np.pi,10 ), feedback.call_user( midpointint,np.sin,0,np.pi,10 ) ):
                feedback.finish( 0.5,'Your value for `midpointint( np.sin,0,np.pi,10 )` is incorrect.  The expected value is 2.0082' )

            feedback.finish( 1.0,'Success!  All values appear to be correct.' )

        except (NameError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( 0.0,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        def midpointint( f,a,b,n ):
            import numpy as np
            h = (b-a)/n
            i = np.array( range( 1,n+1 ) )
            result = h * np.sum( f( a - 0.5*h + i*h ) )
            return result
