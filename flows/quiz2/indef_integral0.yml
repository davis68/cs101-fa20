-
    type: PythonCodeQuestion
    id: quiz2__indef_integral0
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Indefinite Integration

        Consider the following integral:

        $$
        \int dx\, \text{arcsin}^{4} x
        \text{.}
        $$

        Solve this integral symbolically.  Store your result in a variable `result`, which should be a `sympy` expression.

        **$\text{arcsin}$ refers to the arcsine, not $a \times r \times c \times \text{sin}$.**  SymPy refers to arcsine as `asin`, not `arcsin`.

    setup_code: |

    initial_code: |
        import sympy
        x = sympy.S( 'x' )

    names_for_user: [ ]

    names_from_user: [ result ]

    test_code: |
        points = 0.0

        import sympy

        test_x = sympy.S( 'x' )
        test_result = sympy.integrate( sympy.asin( test_x ) ** 4,test_x )

        try:
            if type( result ) != type( test_result ):
                feedback.finish( points,'Your result seems to be incorrect.  I expect a SymPy expression.' )
            points += 0.25

            if result != test_result:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the integral.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        result = sympy.integrate( sympy.asin( x ) ** 4,x )
