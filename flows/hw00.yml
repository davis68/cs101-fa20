{% from "yaml-macros.jinja" import homework_header %}
{{ homework_header(0,lesson_release=0,due_offset=16) }}

pages:
-
    type: Page
    id: hw00__welcome
    content: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Welcome to CS 101.

        Programming is challenging, frustrating, occasionally exhilarating, and often tedious, much like mathematics.  We're going to survey a lot of ground this semester, and I'll do my best to help everyone keep up and stay engaged.

        I would like to know more about your background and how you feel coming into this course.  CS 101 is a required course for many majors, and my students often expect a fairly differentiated set of skills depending on their major.  Some of you have prior exposure to programming, and some have never thought seriously about coding at all before.  It's great for me to know what your background is, your hopes are, and how these will shape your experience with CS 101.

        This assignment will seek to test the following objectives:

        `lesson 1`:

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include( "modules/python/intro/objectives.yml",0 ) }}

        `lesson 2`:

        {{ indented_include( "modules/python/types/objectives.yml",0 ) }}


-
    type: SurveyTextQuestion
    id: hw00__why
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Why CS 101?

        Why are you taking CS 101 now?  What knowledge or experience do you hope to gain during this semester?

    answer_comment: |
        Whether you're here because of an intense professional interest, casual curiosity, or just because it's required, I hope you'll have a meaningful experience in the course.


-
    type: ChoiceQuestion
    id: hw00__bkgd
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   What programming have you done before?

        CS 101 is designed so that you need no prior experience with programming whatsoever.  However, a consistent proportion comes into the class with some previous exposure.  What have you seen before, if anything?

        'Lot' would mean more than two years of casual or serious programming; 'little' would mean less than that but still having done some.

    choices:
    -   "~CORRECT~ I have not programmed at all before."
    -   "~CORRECT~ I have programmed a little before."
    -   "~CORRECT~ I have programmed a lot before."


-
    type: InlineMultiQuestion
    id: hw00__ability
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   What programming have you done before?

        Let's suss this out a little bit more.  Here are some common basic programming tasks.  How comfortable would you be with each?

    question: |
        -   Open a file, process the data, and visualize the output of the process.

            [[blank_1]]

        -   Write a loop to automate a repetitive task.

            [[blank_2]]

        -   Write a function (subroutine, subprocedure, whatever the programming language calls it).

            [[blank_3]]

        -   Import a library or package to extend functionality.

            [[blank_4]]

        -   Write a test to make sure your program code works properly.

            [[blank_5]]

    answers:
        blank_1:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ N/A  (I don't know how to program.)
            -   ~CORRECT~ I couldn't do it yet.
            -   ~CORRECT~ I could do it with access to documentation.
            -   ~CORRECT~ I could do it without access to documentation.

        blank_2:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ N/A  (I don't know how to program.)
            -   ~CORRECT~ I couldn't do it yet.
            -   ~CORRECT~ I could do it with access to documentation.
            -   ~CORRECT~ I could do it without access to documentation.

        blank_3:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ N/A  (I don't know how to program.)
            -   ~CORRECT~ I couldn't do it yet.
            -   ~CORRECT~ I could do it with access to documentation.
            -   ~CORRECT~ I could do it without access to documentation.

        blank_4:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ N/A  (I don't know how to program.)
            -   ~CORRECT~ I couldn't do it yet.
            -   ~CORRECT~ I could do it with access to documentation.
            -   ~CORRECT~ I could do it without access to documentation.

        blank_5:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ N/A  (I don't know how to program.)
            -   ~CORRECT~ I couldn't do it yet.
            -   ~CORRECT~ I could do it with access to documentation.
            -   ~CORRECT~ I could do it without access to documentation.


-
    type: InlineMultiQuestion
    id: hw00__interest
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   What interests you the most?

        We will cover several applications of interest to scientists and engineers throughout the course, as well as having application in many other domains.  How much interest do you have in the following applications?

    question: |
        -   Data processing (data analytics, big data)

            [[blank_1]]

        -   Numerical programming (conventional scientific programming)

            [[blank_2]]

        -   Code development (software development)

            [[blank_3]]

        -   Scientific modeling (applying developed programs to challenges)

            [[blank_4]]

        -   Statistical analysis

            [[blank_5]]

        -   The Python programming language

            [[blank_6]]

        -   The MATLAB programming environment and language

            [[blank_7]]

    answers:
        blank_1:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_2:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_3:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_4:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_5:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_6:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.

        blank_7:
            type: ChoicesAnswer
            required: True
            choices:
            -   ~CORRECT~ Not relevant to me/Not sure what this is.
            -   ~CORRECT~ Not at all motivated.
            -   ~CORRECT~ Somewhat motivated.
            -   ~CORRECT~ Very motivated.


-
    type: MultipleChoiceQuestion
    id: hw00__platform
    value: 1
    credit_mode: proportional
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   What do you use now?

        Which desktop computing platforms are you comfortable using?

    choices:

    -   "~ALWAYS_CORRECT~ Microsoft Windows"
    -   "~ALWAYS_CORRECT~ Mac OS X/macOS"
    -   "~ALWAYS_CORRECT~ GNU/Linux"


-
    type: Page
    id: hw00__math
    content: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Mathematics Review

        Now that we know a bit more about your background, we'd like you to see what is expected from you mathematically in this course.  CS 101 requires Calculus I as a prerequisite, which for us means a basic facility in mathematical notation and comfort with symbolic manipulation in series and calculus operations.

        We expect you to solve the following problems by hand and report the resulting value.  (A calculator may be used if necessary to get a decimal value.)


-
    type: TextQuestion
    id: hw00__math_sum
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Infinite Summation Series

        $$
        \frac{2 \sqrt 2}{9801} \sum_{i=0}^{\infty} \frac{(4i)!(1+i)}{i!^4\left(5^{4i}\right)}
        $$

        Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23`, `0.123`, and `3.33e-8` each have three significant figures.)  You'll need about five terms to get it converged.

        <div class='alert alert-warning'>
        I am concerned with your understanding what this type of notation means, as we will employ it subsequently in the course.  Fortunately, the computer will do most of the work at that point.  To solve it now, you can use pencil and paper, a graphing calculator, a spreadsheet, Wolfram Alpha, Python, or anything else you prefer.
        </div>

        <!--
        from math import factorial

        sum = 0
        for i in range(0, 10):
            term = (factorial(4*i)*(1+i))/(factorial(i)**4*5**(4*i))
            sum += term
            print(f'{i}:  {term}  {sum}')
        print(f'{2*2**0.5/9801*sum}')

        -->

    answers:
        - type: float
          value: 0.0003190139
          rtol: 1e-2


-
    type: TextQuestion
    id: hw00__math_product
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Infinite Product

        _An infinite product is perhaps new to some of you.  It operates on the same principle as an infinite summation series, except that each term is multiplied together._

        $$
        \prod_{n=1}^{\infty}
        \left( \frac{ 4n^2 }{ 4n^2 - 1 } \right)
        $$

        Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23`, `0.123`, and `3.33e-8` each have three significant figures.)  This series converges slowly, so you should check at least five terms.

        <div class='alert alert-warning'>
        I am concerned with your understanding what this type of notation means, as we will employ it subsequently in the course.  Fortunately, the computer will do most of the work at that point.  To solve it now, you can use pencil and paper, a graphing calculator, a spreadsheet, Wolfram Alpha, Python, or anything else you prefer.
        </div>

    answers:
        - type: float
          value: 1.57
          rtol: 1e-2


-
    type: TextQuestion
    id: hw00__math_binomial
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Binomial Coefficient

        Evaluate this series for $a=1$, $b=2$, $n=5$.

        $$
        \sum_{i=0}^{n}
        {n \choose i} a^{n-i} b^i
        =
        \sum_{i=0}^{n}
        \frac{n!}{i!(n-i)!}
        a^{n-i} b^i
        $$

        <div class='alert alert-warning'>
        I am concerned with your understanding what this type of notation means, as we will employ it subsequently in the course.  Fortunately, the computer will do most of the work at that point.  To solve it now, you can use pencil and paper, a graphing calculator, a spreadsheet, Wolfram Alpha, Python, or anything else you prefer.
        </div>

    answers:
        - type: float
          value: 243
          rtol: 0.0001


-
    type: TextQuestion
    id: hw00__math_derivative_simple
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Derivative

        $$
        y = x^3
        $$

        Calculate $\frac{dy}{dx}$.  Answer in the form `5*x*x*x` for $5x^4$, including multiplication and exponentiation explicitly.  (Don't use the `^` to indicate an exponent—write it without that.)

    answers:
        - <sym_expr>3*x**2
        - <sym_expr>3*x*x


-
    type: TextQuestion
    id: hw00__math_derivative_complex
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Derivative

        $$
        z = x y + \sin \left( x y \right) + x
        $$

        Calculate $\frac{dz}{dy}$.  Answer in the form `2*cos(x)`, including multiplication and exponentiation explicitly.  (You need to explicitly indicate multiplication between two variables:  `ab` is different from `a*b`.)

    answers:
        - <sym_expr>x*cos(x*y) + x


-
    type: TextQuestion
    id: hw00__math_derivative_root
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Derivative

        $$
        y( x )
        =
        2 \sqrt{x-1}
        $$

        Calculate $\frac{dy}{dx}$.  Answer in the form `2*sqrt(x)`, including multiplication and trigonometric operations explicitly.

    answers:
        - <sym_expr>1/sqrt(x - 1)


-
    type: TextQuestion
    id: hw00__math_integrate_def
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Definite Integration

        $$
        \int_{0}^{1} dx\,x^{2} \exp\left( 3x \right)
        $$

        Calculate this integral.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23`, `0.123`, and `3.33e-8` each have three significant figures.)

    answers:
        - type: float
          value: 3.645
          rtol: 1e-2


-
    type: TextQuestion
    id: hw00__math_taylor_series
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Taylor Series

        A Taylor series is a way of approximating a function by its derivatives at a single point.

        $$
        f(a)
        + \frac{f'(a)}{1!} (x-a)
        + \frac{f''(a)}{2!} (x-a)^2
        + \frac{f'''(a)}{3!}(x-a)^3
        + \frac{f^{(4)}(a)}{4!}(x-a)^4
        + \cdots
        $$

        ![](repo:./img/hw00_series.png)

        <!--
            import numpy as np
            import matplotlib.pyplot as plt

            x = np.linspace( -3*np.pi,+3*np.pi,501 )
            y1 = np.cos(x)
            y2 = -(x-np.pi/2)
            y3 = -(x-np.pi/2)+1/6*(x-np.pi/2)**3
            y4 = -(x-np.pi/2)+1/6*(x-np.pi/2)**3-1/120*(x-np.pi/2)**5

            plt.plot(x,y1,'b-',label=r'$cos(x)$')
            plt.plot(x,y2,'g-',label='Linear expansion')
            plt.plot(x,y3,'m-',label='Cubic expansion')
            plt.plot(x,y4,'c-',label='Quintic expansion')

            plt.plot(np.pi/2,np.cos(np.pi/2),'ro')
            plt.plot(3*np.pi/4,np.cos(3*np.pi/4),'rx')

            plt.ylim((-2.1,+2.1))
            plt.legend()
            plt.show()
        -->

        Given a function

        $$
        f(x)
        =
        cos(x)
        \textrm{,}
        $$

        calculate the Taylor series expansion around $\frac{\pi}{2}$ for five terms (that is, to the quartic term of the series expansion).  Three of the terms will reduce to zero.

        Use this approximation to estimate the value of $f(\frac{3\pi}{4})$.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23`, `0.123`, and `3.33e-8` each have three significant figures.)

        <div class='alert alert-warning'>
        I am concerned with your understanding what this type of notation means, as we will employ it subsequently in the course.  Fortunately, the computer will do most of the work then.  To solve it now, you can use pencil and paper, a graphing calculator, a spreadsheet, Wolfram Alpha, Python, or anything else you prefer.
        </div>

    answers:
        - type: float
          value: -0.702
          rtol: 1e-2
    # y3 = -(x-np.pi/2)+1/6*(x-np.pi/2)**3


-
    type: PythonCodeQuestion
    id: hw00__transcription
    value: 2
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Composing a Short Program

        Type the following program in a text editor and execute it.  If your program does not work, check that you have copied the code correctly, _including spaces and line breaks_.  Once it works, you should submit it in the code block editor below.

        You should ignore the line numbers.  Those are often present to help us track the location of code as we work on it.  They are provided by a good code editor, not by the programmer.

        (This assignment is based on Langtangen, Exercise 1.8.)

        <img src="https://relate.cs.illinois.edu/course/cs101-fa20/f/img/hw00_code.png" width="100%;"/>

    setup_code: |

    names_for_user: []

    names_from_user: [ area_parallelogram,area_square,area_circle,volume_cone ]

    test_code: |

        from numpy import isclose

        if not feedback.check_scalar( 'area_parallelogram',10,area_parallelogram,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.00,'Your value for area_parallelogram is incorrect.' )
        if not feedback.check_scalar( 'area_square',4,area_square,rtol=1e-6,accuracy_critical=False ):
            feedback.finish( 0.25,'Your value for area_square is incorrect.' )
        if not feedback.check_scalar( 'area_circle',7.068591,area_circle,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.50,'Your value for area_circle is incorrect.' )
        if not feedback.check_scalar( 'volume_cone',11.780985,volume_cone,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.75,'Your value for volume_cone is incorrect.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        from math import pi

        h = 5
        b = 2
        r = 1.5

        area_parallelogram = h*b
        area_square = b**2
        area_circle = pi*r**2
        volume_cone = 1.0/3*pi*r**2*h


-
    type: PythonCodeQuestion
    id: hw00__debug_0
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding and Correcting Bugs

        *These exercises ask you to correct a candidate code by identifying the bugs which prevent it from executing and remedying them.*

        (This assignment is based on Langtangen, Exercise 1.9b.)

        Compute $s$ in meters when $s = v_0 t + \frac{1}{2} at^2$, with $v_0 = 3 \,\text{m}/\text{s}$, $t = 1 \,\text{s}$, $a = 2 \,\text{m}/\text{s}^2$.

    setup_code: |

    initial_code: |
        v0 = 3 m/s
        t = 1 s
        a = 2 m/s**2
        s = v0.t + 0,5.a.t**2
        print( s )

    names_for_user: []

    names_from_user: [ s ]

    test_code: |
        from numpy import isclose

        if not feedback.check_scalar( 's',4.0,s,rtol=1e-2,accuracy_critical=False ):
            feedback.finish( 0.0,'Your value for s is incorrect.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        v0 = 3 #m/s
        t = 1 #s
        a = 2 #m/s**2
        s = v0*t + 0.5*a*t**2
        print( s )


-
    type: PythonCodeQuestion
    id: hw00__debug_1
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding and Correcting Bugs

        *These exercises ask you to correct a candidate code by identifying the bugs which prevent it from executing and remedying them.*

        (This assignment is based on Langtangen, Exercise 1.9c.)

        You should verify the following two equations.  To do this, you should correctly calculate four quantities:  `apb_lhs`, `apb_rhs`, `amb_lhs`, `amb_rhs`, each corresponding to the appropriate part of the equation.  (`apb` means $a+b$, `amb` means $a-b$, and `lhs` and `rhs` respectively mean left-hand and right-hand sides).  You should then output the results of each statement to the screen.

        $$
        (a + b)^{2} = a^{2} + 2ab + b^{2}
        $$
        $$
        (a - b)^{2} = a^{2} - 2ab + b^{2}
        $$

    setup_code: |

    initial_code: |
        a = 3.3
        b = 5.3

        a2 = a^2
        b2 = b^2

        apb_lhs = a + b ** 2
        apb_rhs = a2 + 2ab + b2
        amb_lhs = a - b ** 2
        amb_rhs = a2 - 2ab + b2

        print( '(a+b)^2 = ', apb_lhs )
        print( 'a^2+2ab+b^2 = ', apb_rhs )
        print( '(a-b)^2 = ', amb_lhs )
        print( 'a^2-2ab+b^2 = ', amb_rhs )

    names_for_user: []

    names_from_user: [ a,b,apb_lhs,apb_rhs,amb_lhs,amb_rhs ]

    test_code: |
        from numpy import isclose

        if not feedback.check_scalar( 'a',3.3,a,rtol=1e-3,report_success=True,report_failure=True,accuracy_critical=False ):
            feedback.finish( 0.25,'a has the wrong value---it should be 3.3.' )
        if not feedback.check_scalar( 'b',5.3,b,rtol=1e-3,report_success=True,report_failure=True,accuracy_critical=False ):
            feedback.finish( 0.25,'b has the wrong value---it should be 5.3.' )
        if not feedback.check_scalar( 'apb_lhs',73.96,apb_lhs,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.5,'apb_lhs has the wrong value.  Did you use 3.3 and 5.3 for a and b?' )
        if not feedback.check_scalar( 'amb_rhs',4,amb_rhs,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.5,'amb_rhs has the wrong value.  Did you use 3.3 and 5.3 for a and b?' )
        if not feedback.check_scalar( 'amb_rhs',amb_lhs,amb_rhs,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.0,'apb_lhs and apb_rhs are not equal.' )
        if not feedback.check_scalar( 'apb_rhs',apb_lhs,apb_rhs,rtol=1e-3,accuracy_critical=False ):
            feedback.finish( 0.0,'amb_lhs and amb_rhs are not equal.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        a = 3.3
        b = 5.3

        a2 = a**2
        b2 = b**2

        apb_lhs = (a + b) ** 2
        apb_rhs = a**2 + 2*a*b + b**2
        amb_lhs = (a - b) ** 2
        amb_rhs = a**2 - 2*a*b + b**2

        print( '(a+b)^2 = ', apb_lhs )
        print( 'a^2+2ab+b^2 = ', apb_rhs )
        print( '(a-b)^2 = ', amb_lhs )
        print( 'a^2-2ab+b^2 = ', amb_rhs )


-
    type: PythonCodeQuestion
    id: hw00__debug_2
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding and Correcting Bugs

        *These exercises ask you to correct a candidate code by identifying the bugs which prevent it from executing and remedying them.*

        Given a quadratic equation,

        $$
        a x^{2} + b x + c = 0 \text{,}
        $$

        the two roots are

        $$
        x_{1} = \frac{-b + \sqrt{b^{2}-4ac}}{2a}
        \hspace{1cm}
        x_{2} = \frac{-b - \sqrt{b^{2}-4ac}}{2a}
        $$

        What are the problems with the following program?  Correct the program so that it solves the given equation for these values of `a`,`b`,`c`.

        For $a = 2$, $b = 5$, $c = 2$, the two roots should be $x_{1} = -\frac{1}{2}$ and $x_{2} = -2$.  These values allow you to test the code below to see if it is correct.

        <div class='alert alert-error'>
        Remember, just because a piece of code <em>runs</em>, that does not mean that it runs <em>correctly</em>.
        </div>

        (This assignment is based on Langtangen, Exercise 1.17.)

    setup_code: |

    initial_code: |
        a,b,c = 2,5,2
        q = b*b - 4*a*c
        q_sr = q**0.5
        x1 = (-b + q_sr) / 2*a
        x2 = (-b - q_sr) / 2*a
        print( x1,x2 )

    names_for_user: []

    names_from_user: [ x1,x2 ]

    test_code: |
        from numpy import isclose

        try:
            if not isclose( x1,-0.5,rtol=1e-3 ) or isclose( x2,-0.5,rtol=1e-3 ):
                feedback.finish( 0.0,'Your value for x1 is incorrect.' )
            if not isclose( x2,-2,rtol=1e-3 ) or isclose( x1,-2,rtol=1e-3 ):
                feedback.finish( 0.5,'Your value for x2 is incorrect.' )
        except:
            feedback.finish( 0.0,'Your values for x1,x2 are incorrect.' )

        feedback.finish( 1.0,'Success!  All values appear to be correct.' )


    correct_code: |
        a,b,c = 2,5,2
        q = b*b - 4*a*c
        q_sr = q**0.5
        x1 = (-b + q_sr) / (2*a)
        x2 = (-b - q_sr) / (2*a)
        print( x1,x2 )


-
    type: PythonCodeQuestion
    id: hw00__import_rad
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Trigonometry

        Calculate the value of $\sin 4$, assuming 4 is in radians and that the `sin` function available in Python accepts radians.  Store the result in a variable `sin4`.

    setup_code: |

    initial_code: |

    names_for_user: [ ]

    names_from_user: [ sin4 ]

    test_code: |
        try:
            import numpy as np
            from math import sin
            assert np.isclose( sin4,sin( 4 ),rtol=1e-6 )
        except:
            feedback.finish( 0.0,'Failure.  Did you define a variable `sin4`?' )
        feedback.finish( 1.0,'Success!  You calculated the value correctly.' )

    correct_code: |
        from math import sin
        sin4 = sin( 4 )

        import math
        sin4 = math.sin( 4 )


-
    type: PythonCodeQuestion
    id: hw00__import_deg
    value: 1
    timeout: 10
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Trigonometry

        Calculate the value of $\sin 4^{\circ}$.  Since the `sin` function available in Python accepts radians, you will need to convert degrees to radians.  `math.radians` is a function which accepts degrees and returns the radians equivalent.  Store the result in a variable `sin4d`.

    setup_code: |

    initial_code: |

    names_for_user: [ ]

    names_from_user: [ sin4d ]

    test_code: |
        try:
            import numpy as np
            from math import sin,radians
            assert np.isclose( sin4d,sin( radians( 4 ) ),rtol=1e-6 )
        except:
            feedback.finish( 0.0,'Failure.  Did you define a variable `sin4d`?  Is it correct?' )
        feedback.finish( 1.0,'Success!  You calculated the value correctly.' )

    correct_code: |
        from math import sin,radians
        sin4d = sin( radians( 4 ) )

        import math
        sin4d = math.sin( math.radians( 4 ) )

        import math
        rad4 = math.radians( 4 )
        sin4d = math.sin( rad4 )
