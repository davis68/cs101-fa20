-
    type: ChoiceQuestion
    id: quiz4__rectangle
    value: 0
    access_rules:
        remove_permissions:
            - see_correctness
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Rectangular Function

        *For this problem, you should compose a function which accomplishes a given task using the available code blocks arranged in the correct functional order.*

        The Rectangle function (also known as the gate function or PI function) is defined as,

        $$
        \pi(t)
        =
        \begin{cases}
        0               & \text{if } |t| > \frac{1}{2} \\
        \frac{1}{2}               & \text{if } |t| = \frac{1}{2} \\
        1 & \text{if } |t| < \frac{1}{2} \\
        \end{cases}
        $$

        Compose a function `rect` which accepts a single scalar value `t` and returns the modified value.

        For instance,

            rect(-0.75) == 0

        should be true.

        <div class="alert alert-info">
        <strong>Hint:</strong>  since MATLAB doesn't <code>return</code> from functions with a keyword, you'll need an <code>if</code>–<code>elseif</code>–<code>else</code>–<code>end</code> construction.
        </div>

        <div class="alert alert-success">
        <strong>Hint:</strong>  the best approach is to write a similar program yourself <em>first</em> and then compare it to the options offered here.
        </div>

        1.  `elseif abs(t) == 0.5`
        2.  `else`
        3.  `end`
        4.  `r_t = 1;`
        5.  `r_t = 0;`
        6.  `function [r_t] = rect(t)`
        7.  `elif abs(t) == 0.5`
        8.  `elseif abs(t) = 0.5`
        9.  `if abs(t) > 0.5`
        10. `r_t = 0.5;`

    choices:
    -   "6, 9, 5, 8, 10, 2, 4, 3, 3"
    -   "~CORRECT~ 6, 9, 5, 1, 10, 2, 4, 3, 3"
    -   "6, 9, 5, 7, 10, 3, 3"
    -   "6, 5, 3"
    -   "6, 9, 5, 1, 10, 3, 3"
