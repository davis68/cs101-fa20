# Minimize functions within bounds (`fminbnd`).
-
    type: InlineMultiQuestion
    id: quiz4__fminbnd1
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   MATLAB function optimization

        Consider the function

        $$
        y
        =
        f(x)
        =
        x^4 - \frac{\cos x}{x}
        \text{.}
        $$

        Identify the minimum value of $y$ on the interval $-2<x<0$ as well as the value of $x$ that yields this minimum.  Include _four_ decimal places of precision (output using `format short`, or `0.####`).  (Reading off a plot may help guide you but will likely not be accurate enough.)

        You can always check the documentation if using a built-in function to minimize $y$.

    question: |

        `xmin = ` [[blank_1]]
        `ymin = ` [[blank_2]]

    answers:

        blank_1:
            type: ShortAnswer
            width: 12em
            hint: "Use the format `-1.2345`."
            required: True
            correct_answer:
            - type: float
              value: -0.7947
              rtol: 0.01

        blank_2:
            type: ShortAnswer
            width: 12em
            hint: "Use the format `1.2345`."
            required: True
            correct_answer:
            - type: float
              value: 1.2803
              rtol: 0.01
