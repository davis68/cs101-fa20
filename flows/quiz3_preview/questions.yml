-
    type: ChoiceQuestion
    id: quiz3_preview__eqn_solving0
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Equation solving

        Consider locating a function zero.

        Which solution procedure would be most apt to yield the $x$ which gives $f(x)=0$?

    choices:

    -   "Random walk"
    -   "Hill climbing"
    -   "~CORRECT~ Newton's method"
    -   "Brute force"


-
    type: ChoiceQuestion
    id: quiz3_preview__eqn_solving1
    value: 1
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Equation solving

        Consider locating a function minimum.

        Which solution procedure would NOT be directly to yield the $x$ which gives a minimum of $f(x)$?  (I.e., differentiation or another technique would be necessary first.)

    choices:

    -   "Random walk"
    -   "Hill climbing"
    -   "~CORRECT~ Newton's method"
    -   "Brute force"


-
    type: TextQuestion
    id: quiz3_preview__min0
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Finding a minimum

        Consider the following equation.

        $$
        h( x ) = x \text{ln} x - 1
        $$

        Using any of the means introduced in class, find the value of $x$ for which $h(x^{*})$ is _minimized_.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23` and `3.33e-8` both have three significant figures.)

        <div class="alert alert-error">
        Don't use <code>0</code> or a negative number as a starting guess with <code>log</code>.  In general, I will not set you problems which have <code>nan</code> or <code>np.nan</code> as an answer.
        </div>

    answers:
        - type: float
          value: 0.36787945
          rtol: 1e-2


-
    type: TextQuestion
    id: quiz3_preview__quad
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Calculating an integral

        Consider the following integral.

        $$
        I
        =
        \int_0^\pi dx\,\left( 1 - \cos^{2} x \right)
        $$

        Calculate $I$ as a `float`.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23` and `3.33e-8` both have three significant figures.)  Only real solutions are acceptable (no complex roots).

    answers:
        - type: float
          value: 1.57079
          rtol: 1e-2


-
    type: TextQuestion
    id: quiz3_preview__zero
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Finding a zero

        The [Scorer function of the first kind $\text{Gi}$](https://dlmf.nist.gov/9.12) is a solution of the differential equation

        $$
        \frac{d^2y}{dx^2}-xy
        =
        \frac{1}{\pi}
        \text{.}
        $$

        $\text{Gi}$ may be written as the integral,

        $$
        \text{Gi}(x)
        =
        \frac{1}{\pi}
        \int_{0}^{\infty} dt\, \sin\left(\frac{t^3}{3} + xt\right)
        \text{.}
        $$

        Using any of the means introduced in class, find the value of $x$ for which $\text{Gi}(x^{*}) = 0$ on the interval $[-2,+2]$.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23` and `3.33e-8` both have three significant figures.)  **Use SciPy/NumPy, not SymPy, to solve this equation.**

        > Integrating over the interval $[0,+\infty]$ is _hard_.  I recommend using $10$ in place of $+\infty$, which gets you a convergent answer within about one percent of the correct answer at $Gi(0) = 0.20497...$.  I have been correspondingly permissive with the range of acceptable answers.

        <!--
        import numpy as np
        from scipy.integrate import quad
        from scipy.optimize import newton

        def integrand( t,x ):
            return np.sin( t**3/3 + x*t )
        def Gi( x ):
            N = 1 / np.pi
            integral = quad( integrand,0,10,args=(x,) )[0]
            return N * integral
        newton(Gi,0)
        -->

    answers:
        - type: float
          value: -0.731659410411887
          atol: 1e-1


-
    type: TextQuestion
    id: quiz3_preview__min_airy
    value: 1
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Finding a minimum

        The Airy function of the first kind $\text{Ai}$ is a solution of the differential equation

        $$
        \frac{d^2y}{dx^2}-xy
        =
        0
        \text{.}
        $$

        SciPy provides the Airy function of the first kind $\text{Ai}(x)$ as [`scipy.special.airy`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.airy.html).  Notably, this implementation returns a four-element vector, only the first element of which is $\text{Ai}(x)$ (the others being other solution components).  This means that you will need to put `airy` inside of a function which only returns the zeroth component if you want to plot or use `airy` with a minimization routine.

        Using any of the means introduced in class, find the value of $x$ for which $\text{Ai}(x^{*})$ is _minimized_ on the interval $[-5,0]$.  Your answer should have at least three significant figures, accurate to within 0.1%.  (E.g., `1.23` and `3.33e-8` both have three significant figures.)  **Use SciPy/NumPy, not SymPy, to solve this equation.**

        <!--
        from scipy.special import airy
        from scipy.optimize import minimize
        def Ai( x ):
            return airy( x )[ 0 ]
        minimize( Ai,-3 )
        -->

    answers:
        - type: float
          value: -3.24819761
          rtol: 1e-2


-
    type: PythonCodeQuestion
    title: "Brute Force"
    id: quiz3_preview__brute_force
    value: 4
    timeout: 30
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style-cbtf.html", 8) }}

        #   Cracking a PIN

        Your smartphone or tablet may require you to enter a short personal identification number, or PIN.  This may be any number in a range of lengths.  For our purposes, we will consider PINs to be of length 4, 5, or 6.

        We use digits `0`–`9` inclusive.  Since PINs can be of different lengths, you need to consider `1111`, `01111`, and `001111` as separate examples.

        We provide a testing function `check_pin( pin )` which accepts a four-digit guess for `pin`.  `check_pin` returns `True` if the PIN number matches the internal PIN.  The `pin` should be provided _as a string_, that is, '1234', rather than `[ '1','2','3','4' ]` or `[ 1,2,3,4 ]`.

        Compose a function `crack_pin()` which scans all possible PIN numbers in the valid ranges and returns the correct one.

        **Your submission should include a function `crack_pin()`.**  `check_pin` is already defined for your use.


    setup_code: |
        def set_pin():
            from numpy.random import choice
            global __real_value
            length = choice( [ 4,5,6 ] )
            __real_value = ''.join( choice( [ x for x in '0123456789' ],length,replace=True ) )
        set_pin()

        def check_pin( pin ):
            if not type( pin ) == str:
                raise Exception( 'PIN should be a string.' )
            return pin == __real_value

    initial_code: |

    names_for_user: [ set_pin,check_pin,__real_value ]

    names_from_user: [ crack_pin,__real_value ]

    test_code: |
        points = 0.0

        try:
            import inspect

            from numpy import isclose
            import numpy as np

            # 10% for function definition
            # test functions exist
            if not inspect.isfunction( crack_pin ):
                feedback.add_feedback( 'You don\'t seem to have defined the correct function `crack_pin`.' )
            sub_args = inspect.getargspec( crack_pin ).args
            if len( sub_args ) != 0:
                feedback.add_feedback( 'Your function `crack_pin` doesn\'t seem to have the correct number of parameters.' )
            points += 0.10

            if points < 0.10:
                feedback.finish( points,'You need to define the proper function first.' )

            # 90% for `crack_pin` correctness
            from numpy import isclose,allclose
            user_answer = feedback.call_user( crack_pin )
            feedback.add_feedback( f'Your code found the answer "{user_answer}".' )
            feedback.add_feedback( f'In this case, the correct PIN is "{__real_value}".' )
            if __real_value == user_answer:
                points += 0.9
                feedback.add_feedback( 'Your value for `crack_pin()` is correct for the actual value.' )
            else:
                feedback.add_feedback( 'Your value for `crack_pin()` is incorrect.' )

            if np.isclose( points,1.0 ):
                feedback.finish( points,"Congratulations! Your code passes all of our tests." )
            else:
                feedback.finish( points,"It looks like your code still needs some work to be viable." )

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( points,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

    correct_code: |
        import itertools
        def crack_pin():
            from itertools import product
            for length in [ 4,5,6 ]:
                for trial in product( '0123456789',repeat=length ):
                    guess = ''.join( trial )
                    if check_pin( guess ):
                        return guess
