from math import pi

h = 5.0     # height, m
b = 2.0     # base, m
r = 1.5     # radius, m

area_parallelogram = h * b
print( 'The area of the parallelogram is',area_parallelogram )

area_square = b**2
print( 'The area of the square is',area_square )

area_circle = pi * r**2
print( 'The area of the circle is',area_circle )

volume_cone = 1/3 * pi * r**2 * h
print( 'The volume of the cone is',volume_cone )
