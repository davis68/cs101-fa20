-
    type: Page
    id: matlab_model__video
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Languages for the Future

        [According to one survey](https://fossbytes.com/most-popular-programming-languages/), the twelve most popular languages (across all domains) are (as of August 2019):

        | Rank | Language | Application |
        | ---- | -------- | ----------- |
        |   1  | Java     | Applications development |
        |   2  | C        | Universal systems programming |
        |   3  | Python   | Universal scripting language |
        |   4  | C++      | Systems programming; GUIs |
        |   5  | C♯       | Windows desktop applications |
        |   6  | Visual Basic | Applications development |
        |   7  | JavaScript | Web development |
        |   8  | PHP      | Web development |
        |   9  | Objective-C | Systems programming (Apple) |
        |  10  | SQL      | Databases and data access |
        |  11  | Ruby     | Web development |
        |  12  | MATLAB   | Engineering & numerical applications |

        For an engineer or scientist, this list gives some insight into the overall utility of languages.  You have other considerations as well:  first of all, the main programming language of whichever company you work at.  Python is particularly popular as a second language, one used by almost everyone to stitch together larger projects or spin up quick tests and proofs of concept.  Another consideration is what is popular in your field:  statisticians tend to use R, while many bioinformaticians use MATLAB or Python.

        You've seen Python and MATLAB.  What else is worth considering?

        -   **C** is an extremely efficient systems programming language, meaning that when you need to optimize things close to the metal C is a good choice.
        -   **C++** is useful when you need to work with higher-level abstractions; for instance, if you don't want to work with "numbers in an array" but rather with a "matrix," C++ can help you.
        -   **R** is a statistics-oriented language.
        -   **Fortran** is a venerable and idiosyncratic language, but fantastically efficient and fast.  Many legacy engineering programs are written in Fortran and require Fortran to talk to them.
        -   **Julia** is a newer language seeking to bridge a gap in sophistication between Python-like ease-of-use and C-like speed and power.
        -   **Mathematica** is a domain-specific language targeting mathematical applications (and everything built on top of math).  It is developed in Champaign by Wolfram Research.
        
        Whatever you decide to pursue, keep Python in your back pocket.  It will undoubtedly be important for decades.
