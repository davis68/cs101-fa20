-
    type: Page
    id: matlab_intro__burly
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Beginning MATLAB

        MATLAB is a long-established engineering software suite which features its own programming language and a battery of toolboxes and legacy codebases.  It is particularly well-suited for linear algebraic problems, control dynamics, numerical analysis, and image processing.  On the down side, MATLAB has a relatively stiff syntax inherited from its roots in the late 1970s, as well as a hefty price tag.
        
        "MATLAB" refers both to the computing platform and to the language.  We will teach a subset of MATLAB that is compatible with [Octave](https://www.gnu.org/software/octave/), a freely-available open-source language and platform largely compatible with MATLAB.  If you are having difficulty accessing MATLAB, I encourage you to use Octave in its stead[.](https://i.imgur.com/WLpopHl.jpg)  <!-- egg -->

        The MATLAB language should look fairly familiar to you, since it was an influence on the original design of Python as a language.  In particular, PyPlot is modeled closely on MATLAB's plotting syntax so you won't need to relearn plotting.
        
        One more thing:  MathWorks, the developers of MATLAB, have created very extensive documentation and in particular videos demonstrating every nook and cranny of MATLAB.  In lieu of lecture videos, I will frequently point you at those in addition to these lesson notes.
        
        
        ##  The MATLAB Interface

        When you start MATLAB, you will see a multipanel layout including a file tree (`Current Folder`), a command window, and a list of variables (`Workspace`).

        ![](repo:./img/matlab_intro__burly_layout.jpg)

        Type commands into the command window.  This is similar to the interface of IPython.
        
        
        ##  The Octave Interface
        
        Octave can be run directly on the command line (like Python/IPython) or in a graphical user interface like MATLAB.  We will refer subsequently to "MATLAB" as encompassing both MATLAB and Octave behavior unless otherwise noted.
        
        ![](repo:./img/matlab_intro__burly_octave.png)
        
        You can choose to use either Octave or MATLAB for CS 101.  If there are any differences in their behavior that affect us, I will note those.  If you are in a department that will continue to use MATLAB exclusivel for assignments, you should prefer MATLAB.
        
        [A complete list of Octave functions is available here](https://octave.sourceforge.io/list_functions.php), and you should bookmark it if you prefer to use Octave as I will provide MathWorks MATLAB documentation links.  [Differences between MATLAB and Octave](https://en.wikibooks.org/wiki/MATLAB_Programming/Differences_between_Octave_and_MATLAB) will be noted along the way as necessary for this course.
        
        
        ##  Basic Elements & Syntax
        
        MATLAB behaves like a graphing calculator, perhaps even more so than Python.  MATLAB numbers by default are all `float`s.
        
        MATLAB operators are similar to Python's, but a few of them are different, notably exponentiation:
            
            a = 4 ^ 3
            b = 1 + a
        
        Note that each line you input automatically creates (and echoes) a variable called `ans`.  To suppress the echo, use a semicolon afterwards[:](https://farm4.static.flickr.com/3218/2987688664_e71bb18ea5.jpg)  <!-- egg -->
            
            c = a / 4;
