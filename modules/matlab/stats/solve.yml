-
    type: Page
    id: matlab_stats__solve
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Solving Matrix Equations
        
        -   [Lecture video](https://youtu.be/bstmPXEWbv0)

        The point has been made in an earlier lecture that we must distinguish linear algebraic or matrix operations, like matrix multiplication and exponentiation, from elementwise operations, which are marked with a `.` on the operator.

        Typically, the major reason an engineer would be interested in matrix equations at all is to solve them.  MATLAB is optimized to efficiently solve matrix problems.  Consider, for instance, the classical equation

        $$
        \underline{\underline{A}} \underline{x}
        =
        \underline{b}
        \text{;}
        $$

        that is, given a known matrix $\underline{\underline{A}}$ and a known resultant vector $\underline{b}$, find the required unknown values $\underline{x}$ (which may represent displacement, tension, temperature, etc.).  The formal way of solving this equation is again straightforward to write, using the matrix inverse operator:

        $$
        \underline{\underline{A}}^{-1} \underline{\underline{A}} \underline{x}
        =
        \underline{\underline{A}}^{-1} \underline{b}
        \rightarrow
        \underline{\underline{I}}
        \underline{x}
        =
        \underline{\underline{A}}^{-1} \underline{b}
        \rightarrow
        \underline{x}
        =
        \underline{\underline{A}}^{-1} \underline{b}
        \text{.}
        $$

        Indeed, MATLAB can quickly calculate such a result:

            A = [ 1 2 3 ; 3 2 2 ; 4 5 6 ];
            b = [ 3 4 7 ]';

            x = inv(A) * b;

        What about a larger matrix, with most nonzero numbers near the diagonal?  This is a common form in engineering problems:

            A = [ 1 -2 0 0 0 ; -2 1 -2 0 0 ; 0 -2 1 -2 0 ; 0 0 -2 1 -2 ; 0 0 0 -2 1 ];
            b = [ 1 2 3 4 5 ]';

            fprintf('%7.2f %7.2f %7.2f %7.2f %7.2f \n' , inv(A) )

        The matrix $\underline{\underline{A}}$ is called _sparse_, because most of its nonzero values are along the diagonal.  This makes it efficient to calculate with and to store in memory.  In contrast, the matrix $\underline{\underline{A}}^{-1}$ is _dense_, and thus more difficult to calculate with efficiently or to store.  This problem is exacerbated as the size of matrices varies up to $10,000 \times 10,000$ or more!

        Fortunately, it turns out that we don't generally need to calculate the inverse to solve the matrix equation $\underline{\underline{A}} \underline{x} = \underline{b}$.  A variety of more sophisticated methods are available, and MATLAB implements them behind a new operator, the left-division or `\` operator (the backslash on your keyboard, frequently above the `Return` key).

        To solve the matrix equation with `\`, simply use it with `x`:

            A = [ 1 -2 0 0 0 ; -2 1 -2 0 0 ; 0 -2 1 -2 0 ; 0 0 -2 1 -2 ; 0 0 0 -2 1 ];
            b = [ 1 2 3 4 5 ]';
            x = A \ b;

        MATLAB solves the problem behind the scenes for us.
