-
    type: Page
    id: matlab_fit__crimp
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Fitting Curves to Data
        
        -   [Lecture video](https://youtu.be/GJ65GbAWQJ8)

        Fitting or regression describes the technique of creating equations—typically polynomials—to model or predict behavior.  We move from a set of data, through the creation of an underlying equation with appropriate coefficients, to a functioning predictive model.  Ideally we will also assess expected error.

        [`polyfit`](https://www.mathworks.com/help/matlab/ref/polyfit.html) attempts to fit a polynomial of $n$th order to a set of points.  It returns a set of coefficients to a fitting polynomial.

            x = linspace( 0,1,11 );
            y = ( x - 0.5 ) .^ 2 + 0.01 * randn( size( x ) );
            mycoefs = polyfit( x,y,2 );

            x_fit = linspace( 0,1,51 );
            y_fit = polyval( mycoefs,x_fit );

            hold on
            plot( x,y,'ro' );
            plot( x_fit,y_fit,'r-' );

        Try changing the value of $n$ in `polyfit` to see how the fit is affected.

        How do we fit a more complicated curve though?  If it clearly has a high-order polynomial form, then just adjust the polynomial you're using to fit it.

            x = linspace( 0,1,11 );
            y = ( x - 0.5 ) .^ 5 + 0.001 * randn( size( x ) );
            mycoefs = polyfit( x,y,10 );

            x_fit = linspace( 0,1,51 );
            y_fit = polyval( mycoefs,x_fit );

            hold on
            plot( x,y,'ro' );
            plot( x_fit,y_fit,'r-' );

        If it has a power-law form, though, it's better to take advantage of logarithms to fit it.

            x = linspace( 0,1,6 );
            y = [ 2 4 7 10 25 60 100 ];

        1.  Take the logarithm of the $y$ data.

                logy = log( y );

                plot( x,logy,'bo' )

        2.  Fit the logarithmic form of the data against $x$; typically this will be a linear fit.

                mycoefs = polyfit( x,logy,1 );

        3.  Reconstruct the predicted exponential form.

                xfit = linspace( 0,1,51 );
                yfit = exp( mycoefs(1)*xfit + mycoefs(2) );

                hold on
                plot( x,y,'ro' )
                plot( xfit,yfit,'b-' )
        
        If the equation is more complicated, such as $\exp(-x^2)$, it will require some finesse to fit it.  You'll have a theoretical reason to expect such a form though, and you need be wary of overfitting, which we will discuss later.
        
        
        ##  Measuring Error
        
        We would like to estimate the error of a fit.  Traditionally, this is done using the $L^2$ metric:
        
        $$
        L^2 = | y_\text{known} - y_\text{fitted} |^2 \text{.}
        $$
        
        This is always positive and should be close to zero.
        
        We can think about error in two ways:
        
        1.  The difference between known points or observations and the model.
        
            The quantity $y_\text{known}-y_\text{fitted}$ is the _residual_.  The residual is helpful for telling you how much error there is and where it is located.

            A good fit has a small residual (but the magnitude is contextual).

                x = rand( 11,1 );
                y = rand( 11,1 );
                xf = 0:0.01:1;
                pf  = polyfit( x,y,3 );
                err = sum( abs( y - polyval( pf,x ) ) .^ 2 );
                figure;
                plot( x,y,'rx',xf,polyval( pf,xf ),'r-' );
                ylim( [ 0 1 ] );
                disp( err );
        
        2.  The difference between a known curve and the model.  This is possible if we have an analytical solution to compare to.  It tells us the estimated error at any point along the entire domain. 
        
        
        ##  Thoughts on Curve Fitting
        
        Modeling, like optimization, is a brilliant and deep field with dozens of niches to get lost in.  There are a lot of ways to describe the goodness-of-fit, a lot of ways to capture all the data, and a lot of ways to deceive yourself.
        
        ![](repo:./img/matlab_fit__crimp_curve_fitting.png)
