-
    type: Page
    id: matlab_fit__weave
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Splines
        
        -   [Lecture video](https://youtu.be/SCI0GYnypAM)

        Fitting a problem piecewise over intervals is a decent solution.  In its limit, we have _splines_, which fit data points smoothly and continuously, but retain some (not all) of the choppiness of high-order polynomial fits.

        [`spline`](https://www.mathworks.com/help/matlab/ref/spline.html)s are used to smoothly fit long segments of data without overfitting.  They use piecewise polynomials that are guaranteed to smoothly fit together at the seams.  They are first-order continuous, meaning they can be differentiated to find slopes and extrema.

        ![](repo:./img/matlab_fit__weave_splines.png)

            xvals = linspace( -5,5,11 );
            yvals = xvals.^3 + xvals + 0.05*randn( size( xvals ) );

            hold on
            plot( xvals,yvals,'ro' )

            x = linspace( -5,5,101 );
            y = spline( xvals,yvals,x );
            plot( x,y,'r-' );


        ##  Examples

        -   Brexit polling

                % This recapitulates the data we pulled last time.
                % Let's just examine one data set, column 2, 'Remain'
                poll = importdata( 'brexit.csv' );
                figure
                legend( gca,'show' )
                hold on
                plot( poll.data( :,2 ),'ko','DisplayName','Raw Data' );

                n = numel( poll.data( :,2 ) );
                x = linspace( 1,n,n );
                y = poll.data( :,2 )';
                x_fit = linspace( 1,n,2001 );

                %% We will compare several possible fits.
                % Linear interpolation
                y_interp1 = interp1( x,y,x_fit );
                plot( x_fit,y_interp1,'m--','DisplayName','Linear interpolation' );

                % Linear fit
                p_linear = polyfit( x,y,1 );
                y_linear = polyval( p_linear,x_fit );
                plot( x_fit,y_linear,'r--','DisplayName','Linear fit' );

                % Quadratic fit
                p_quad = polyfit( x,y,2 );
                y_quad = polyval( p_quad,x_fit );
                plot( x_fit,y_quad,'g--','DisplayName','Quadratic fit' );

                % Cubic fit
                p_cubic = polyfit( x,y,3 );
                y_cubic = polyval( p_cubic,x_fit );
                plot( x_fit,y_cubic,'r.','DisplayName','Cubic fit' );

                % Quartic fit
                p_fourth = polyfit( x,y,4 );
                y_fourth = polyval( p_fourth,x_fit );
                plot( x_fit,y_fourth,'g.','DisplayName','Quartic fit' );

                % Tenth-order polynomial fit
                p_tenth = polyfit( x,y,10 );
                y_tenth = polyval( p_tenth,x_fit );
                plot( x_fit,y_tenth,'k--','DisplayName','10th-order fit' );

                % Spline fit
                y_spline = spline( x,y,x_fit );
                plot( x_fit,y_spline,'b--','DisplayName','Spline fit' );

            -   [`brexit.csv`](repo:./resources/brexit.csv)
