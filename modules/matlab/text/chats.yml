-
    type: Page
    id: matlab_text__chats
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Text in MATLAB
        
        -   [Lecture video](https://youtu.be/KJwzgJ8vnG0)
        
        -   [Supporting documentation:  Text in String and Character Arrays](https://www.mathworks.com/help/matlab/matlab_prog/represent-text-with-character-and-string-arrays.html)
        
        MATLAB has two ways to represent text:
            
        1.  Character arrays, the legacy way, stores a vector or matrix of letters the same way numeric data are stored in a regular array.
        2.  String arrays have some advantages in storing text data.
        
        We examine both because both are now ubiquitous in MATLAB computing.  We will use the term "string" to refer to both character arrays and string components inside of string arrays.
        
        
        ##  Character Arrays

        In MATLAB, text is considered a regular array (note the `[]`):

            line1 = 'Did Descartes';
            line2 = 'Depart';
            line3 = 'With the thought';
            line4 = '"Therefore I''m not?"';
            clerihew = [ line1,'\n',line2,'\n',line3,'\n',line4,'\n' ];
            fprintf( clerihew );
            
        MATLAB uses `''` inside of a string to denote a single quote without ending the string.  By itself, of course, `''` represents an empty string.  (We of course use `\n` to stand for a new line or line break.)
        
        Text is displayed using [`'fprintf'`](https://www.mathworks.com/help/matlab/ref/fprintf.html), MATLAB's primary output method.  At this point, we'll just use `fprintf` to show strings we have already constructed, but later on we'll see what else it can do.
        
        Character arrays have the shortcoming that, if two-dimensional, they can break your program in unexpected ways:

            A = [ 'HELLO';'WORLD' ];
            A( 2,3 )
            C = [ 'HELLO';'WORLD!' ];
            
        **The latter won't work in MATLAB, but will in Octave.**
        
        
        ##  Strings

        MathWorks now recommends using [_strings and string arrays_](https://www.mathworks.com/help/matlab/ref/string.html), available in versions R2016b and newer.
        
        These are denoted by double quotes `""`, and in most respects behave the same as character arrays.  They are sometimes more convenient ni their behavior, however.
        
            dwarf_planets = ["Pluto"; "Ceres"; "Eris";
                             "Makemake"; "Haumea"];
                             
        **Octave doesn't really distinguish strings and character arrays, but for the most part the behavior is the same.**
        
        [_Cell arrays_](https://www.mathworks.com/help/matlab/ref/cell.html) are also a reasonable solution.  We will incidentally run into them, but we won't focus on them.  (They are like Python lists.)
