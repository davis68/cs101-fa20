-
    type: TextQuestion
    id: numerics_equation__mince_zero_graphic
    value: 1
    title: "Checkpoint:  Finding a Zero Graphically"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding a Zero Graphically

        Consider the following equation.

        $$
        x^2 + x^3 = \exp x
        $$

        Using a graphical technique, find the value of $x$ which satisfies this equation.

        That is, I expect you to plot the left-hand side versus $x$, the right-hand side versus $x$, and find the intersection (thus $x = x^{*}$).  **A version of this question will be asked on a future quiz.**

        <!--
            import numpy as np
            from scipy.optimize import newton
            def f(x):
                return x**2+x**3-np.exp(x)
            newton(f,1)
            newton(f,5)
            newton(f,-1)

            import matplotlib.pyplot as plt
            x = np.linspace(-10,10,1001)
            plt.plot(x,f(x),'r-')
            plt.ylim(-10,+10)

            plt.plot(x,x**2-x**3,'b-')
            plt.plot(x,np.exp(x),'g-')
            plt.show()
        -->

    answers:
        - type: float
          value: 1.24256
          atol: 0.1
        - type: float
          value: 5.02446
          atol: 0.1

-
    type: TextQuestion
    id: numerics_equation__mince_zero_newton
    value: 1
    title: "Checkpoint:  Finding a Zero Numerically"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding a Zero Numerically

        Consider the following equation.

        $$
        \cos x + 2 = - x^3 + 3 x
        $$

        Using Newton's method as discussed in the lecture, find the value of $x$ for which $f(x^{*}) = 0$.  Your answer should be real; increase the tolerance to verify if any imaginary components go to zero.

        For reference, this is the code we developed.  You may also import and use `scipy.optimize.newton` if you prefer.  **A version of this question will be asked on a future quiz.**

            def dfdx( f,x,h=1e-3 ):
                return ( f( x+h ) - f( x ) ) / h

            def newton( f,x0,tol=1e-3 ):
                d = abs( 0 - f( x0 ) )
                while d > tol:
                    x0 = x0 - f( x0 ) / dfdx( f,x0 )
                    d = abs( 0 - f( x0 ) )
                return ( x0,f( x0 ) )

    answers:
        - type: float
          value: -1.95692
          atol: 0.001


-
    type: TextQuestion
    id: numerics_equation__mince_zero_any
    value: 1
    title: "Checkpoint:  Finding a Zero Numerically"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Finding a Zero Numerically

        Consider the following equation[.](https://www.quora.com/What-happens-if-you-snort-plutonium/answer/Josh-Manson)  <!-- egg -->

        $$
        f( x ) = \cos( x ) + 2 x
        $$

        Using any of the means discussed in the lecture, find the value of $x$ for which $f(x^{*}) = 0$.  **A version of this question will be asked on a future quiz.**

    answers:
        - type: float
          value: -0.450085
          atol: 0.001
