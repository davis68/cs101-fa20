-
    type: Page
    id: numerics_equation__scrys
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Locating Function Minima
        
        -   [Lecture video](https://youtu.be/nXsIRSLKCf0)

        `scipy.optimize.minimize( f,x0 )` seeks to _minimize_ a function (rather than locate a zero of it).  `minimize`

            import matplotlib.pyplot as plt
            import numpy as np
            import scipy.optimize

            def f( x ):
                return x**2 + x - 1

            x = np.linspace( -10,10,1000 )
            xstar = scipy.optimize.minimize( f,x0=3 )

            plt.plot( x,f( x ),'r--', xstar[ 'x' ],f( xstar[ 'x' ] ),'ro' )
            plt.show()

        A graphical solution can be used to determine a good guess for `minimize`.

        Since `minimize` only locates minima, you should be clever in preparing `f` when searching for a maximum.  For instance, you can negate a function to help.
        
        **In summary**:

        | If you need to find this: | Use this:                 |
        | ------------------------- | ------------------------- |
        | minimum of a function     | `scipy.optimize.minimize` |
        | zero of a function        | `scipy.optimize.newton`   |
        |                           | graphical method          |
        | solution to a function    | subtract and find zeroes  |
