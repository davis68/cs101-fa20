-
    type: Page
    id: numerics_greedy__wutsup
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Limitations of Brute Force
        
        -   [Lecture video](https://youtu.be/w_L0u7xwBW4)

        In `numerics/brute` we wrote that:

        > The goal of a brute-force search is to search the domain for the ${x}^*$ which yields the optimal $f({x}^*)$.

        The problem is that most domains are far too large, with too many parameters, to realistically explore exhaustively.

        For instance, consider the problem of cracking a password.  If each character can take one of 86 characters (digits, letters, special characters), then the brute-force search looks like this:

        | Number of characters | Possible combinations | Time |
        | -------------------- | --------------------- | ---- |
        | 1 | 86 | $8.6 \times 10^{-6} \,\text{s}$ |
        | 2 | 7,396 | $7.4 \times 10^{-4} \,\text{s}$ |
        | 3 | 636,056 | $6.4 \times 10^{-2} \,\text{s}$ |
        | 4 | 54,700,816 | $5.4 \,\text{s}$ |
        | 5 | 4,704,270,176 | $470.4 \,\text{s}$ |
        | 10 | $2.2 \times 10^{19}$ | $1.9 \times 10^{14} \,\text{s}$ |
        | 20 | $4.9 \times 10^{38}$ | $4.9 \times 10^{31} \,\text{s}$ |

        Clearly, breaking a password by brute force soon becomes unmanageable.  (We assume that a password attempt takes $1 \times 10^{-7}$ s to execute.)

        Once we have reached the limits of our available computational power, we must instead search _heuristically_, or using rules of thumb that may guide us to an acceptable solution (although not one guaranteed to be globally optimal).

        In many cases, a "good-enough" solution is fine.  If we have a figure of (relative) merit, we can classify candidate solutions by how good they are.  Heuristic algorithms don't guarantee the "best" solution, but are often adequate (and the only viable choice!).


        #   When to Use Hill Climbing

        So brute-force optimization has hard limits due to the computational power required.  Hill climbing is an appropriate heuristic alternative for some scenarios.

        Hill-climbing describes locating an arbitrary point on a function surface, then stepping uphill (and only uphill) until one cannot step uphill anymore.  Hill-climbing is a _greedy_ algorithm, we say, meaning that it finds the closest local maximum (more commonly, minimum).  HC thus works well for convex functions which do not have multiple extremal peaks and valleys.

        Hill-climbing approaches require a figure of merit which provides relative correctness information rather than a single `True`/`False` result.  Thus a hill-climbing approach would not work well for, say, a password attack, but it could work reasonably well for the `pirate`/`'BLACKBEARD'` example.
