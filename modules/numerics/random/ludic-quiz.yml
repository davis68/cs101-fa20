-
    type: MultipleChoiceQuestion
    id: numerics_random__ludic_ranges0
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    credit_mode: proportional
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        Which of the following values cannot possibly be returned by the function call `np.random.randint( 12,17 )`?  Check all that are invalid.

    choices:
    -   "~CORRECT~ 17"
    -   "~CORRECT~ 18"
    -   "~CORRECT~ 13.5"
    -   "12"


-
    type: ChoiceQuestion
    id: numerics_random__ludic_ranges1
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        If

            a = np.random.randint( 0,10,size=( 5,5 ) ) + 1

        what results from

            print( a[ 1,5 ] )

    choices:
    -   "10"
    -   "3.5"
    -   "0"
    -   "~CORRECT~ An error occurs."


-
    type: ChoiceQuestion
    id: numerics_random__ludic_ranges2
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        If

            x = np.random.uniform( size=(10000000,) ) * 2  + 1

        what would be an approximate _mean_ value of `x`?

    choices:
    -   "0.5"
    -   "~CORRECT~ 2"
    -   "1"
    -   "1.5"


-
    type: ChoiceQuestion
    id: numerics_random__ludic_ranges3
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        If

            x = np.random.uniform()

        returns values from $[0,1)$, how can we linearly transform `x` to make it span the possible range $[-1,-0.5)$?

    choices:
    -   "x / 2"
    -   "~CORRECT~ x / 2 - 1"
    -   "x / 4 - 1"
    -   "x / 2 - 2"


-
    type: ChoiceQuestion
    id: numerics_random__ludic_ranges4
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        How do the distributions of values between

            x = np.random.uniform()

        and

            y =  np.random.normal()

        differ?

    choices:
    -   "~CORRECT~ `x` is evenly distributed whereas `y` is not."
    -   "`y` is evenly distributed whereas `x` is not"
    -   "Both `x` and `y` are evenly distributed across the range."


-
    type: ChoiceQuestion
    id: numerics_random__ludic_ranges5
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        If

            x = range( 10,30,3 )

        which of the following is a possible value returned by

            np.random.choice(x)

        (More than one choice may be correct.)

    choices:
    -   "11"
    -   "~CORRECT~ 13"
    -   "14"
    -   "30"


-
    type: MultipleChoiceQuestion
    id: numerics_random__ludic_ranges6
    value: 1
    title: "Checkpoint:  Random Distribution Ranges"
    credit_mode: proportional
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distribution Ranges

        If

            x = range( 1,10,2 )

        which of the following is a possible value returned by

            np.random.shuffle(x)

        (More than one choice may be correct.)

    choices:
    -   "[ 1,2,3,4,5,6,7,8,9 ]"
    -   "~CORRECT~ [ 1,3,5,7,9 ]"
    -   "~CORRECT~ [ 1,5,9,3,7 ]"
    -   "[ 1,5,3,7 ]"


-
    type: MultipleChoiceQuestion
    id: numerics_random__ludic_ranges7
    value: 1
    title: "Checkpoint:  Random Distributions"
    credit_mode: proportional
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Random Distributions

        If

            x =  ['a','b','c','d']

        which of the following is a possible value returned by

            np.random.choice(x)

        (More than one choice may be correct.)

    choices:
    -   "'ab'"
    -   "~CORRECT~ 'a'"
    -   "~CORRECT~ 'c'"
    -   "~CORRECT~ 'd'"


-
    type: ChoiceQuestion
    id: numerics_random__ludic_coin
    value: 1
    title: "Checkpoint:  A Fair Coin"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   A Fair Coin

        Consider the following code which should reproduce a fair coin (that is, one with the same probability of a head or a tail when flipped):

            import numpy as np
            x = _____
            if x < 0.5:
                print( 'head' )
            else:
                print( 'tail' )

        Which of the following choices correctly implements the program?

    choices:

    -   "~CORRECT~ `np.random.uniform()`"
    -   "`np.random.randint()`"
    -   "`np.random.normal()`"
    -   "`np.random.shuffle()`"


-
    type: ChoiceQuestion
    id: numerics_random__ludic_die
    value: 1
    title: "Checkpoint:  A Fair Die"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   A Fair Die

        Consider the following code which should reproduce a fair die (that is, one with the same probability of any side appearing when thrown):

            import numpy as np
            x = _____
            print( x )

        Which of the following choices correctly implements the program?

    choices:

    -   "~CORRECT~ `np.random.randint( 1,7 )`"
    -   "`np.random.uniform() * 6`"
    -   "`np.random.normal() * 6`"
    -   "`np.random.randint( 1,6 )`"
