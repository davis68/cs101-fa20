-
    type: Page
    id: python_flow__grasp
    content: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Use Functions to Modularize Code

        Many of the programs we have written previously can be adapted into functions.  **Any piece of code which executes many times is a good candidate for being written as a function.**  We call this _modularization_[.](https://en.wikipedia.org/wiki/Equal-loudness_contour)  <!-- egg -->

        Following are some examples of programs which move specific bits of logic into a function for reusability.

        -   Write a function for a user to create a new password.  The function `validate_password` should test the password against certain rules.

                def validate_password( trial ):
                    if len( trial ) < 8:
                        # must be 8 characters at a minimum
                        return False
                    if trial.isupper() or trial.islower():
                        # must have both upper- and lower-case letters
                        return False
                    if trial.isalpha() or trial.isdigit():
                        # must have letters and numbers
                        return False
                    return True  # password is OK

        -   Write a function `isclose` which assesses whether two values `a` and `b` are sufficiently near each other to be consider "equal".  Have the relative tolerance be 0.001.  (The relative tolerance is defined as $\frac{|a-b|}{\min( |a|, |b| )}$.)

                def isclose( a,b ):
                    return ( abs( a-b ) / min( a,b ) ) <= 1e-3
