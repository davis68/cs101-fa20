-
    type: Page
    id: python_flow__birch
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Flowcharts

        We can diagram a program by dividing it into various logical parts and drawing arrows in between the boxes.

        Flowcharts are effectively skeletons for a program.  They don't show the details of calculations, but they are a great way to think about the overall structure of a program.

        There are many different elements of professional flowcharts, but we need only a few:

        **Arrow**.  Indicates control flow, or the way that the program can in principle execute.

        ![](repo:./img/python_flow__birch_arrow.png)

        **Process Block**.  Shows a set of operations, typically statements or functions.

        ![](repo:./img/python_flow__birch_process.png)

        **Decision Diamond**.  Shows a choice to be made with at least two possibilities, typically an `if` block.

        ![](repo:./img/python_flow__birch_decision.png)

        **Terminal**.  Indicates the end of a program.  May be shown once or many times by convenience, and is frequently omitted because implied.

        ![](repo:./img/python_flow__birch_terminal.png)

        **Input/Output**.  Indicates a need for user input (often the `input` command) or output[.](https://66.media.tumblr.com/8285c7a6ec9307e7101ea179c79d25dc/tumblr_nlextre2dw1tanofjo1_1280.png)  <!-- egg -->

        ![](repo:./img/python_flow__birch_io.png)

        Most of the programs we have designed so far are linear:

        ![](repo:img/python_flow__birch_flowchart_linear.png)

        When our code makes a decision due to a conditional term, we indicate this by drawing a diamond and providing alternate paths of execution.  The following describe sequentially more complex programs.

        ![](repo:img/python_flow__birch_flowchart_if.png)

        ![](repo:img/python_flow__birch_flowchart_ifelse.png)

        ![](repo:img/python_flow__birch_flowchart_if2.png)

        (`True` and `False` do not have defined directions out of the decision diamond, so you should label them when constructing a flowchart.)

        We will often leave the end of the program implicit rather than explicit.  In this case, no `end program` block will appear after the process blocks are complete.

        - [Video:  "The Art of Writing Software", _Computer History Museum_](https://www.youtube.com/watch?v=QdVFvsCWXrA)
