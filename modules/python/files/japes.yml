-
    type: Page
    id: python_files__japes
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Reading Data from Files

        So far we have always accessed files using the `read` method, but the `readlines` method is frequently more convenient.  What's the difference?

        -   `read` returns the entire file as a single string.  This means that we have to figure out how to break the string up into logical pieces before we can use it.  Sometimes this is good (image data); sometimes this introduces an extra step.

        -   `readlines` returns the file line-by-line as a `list` of strings.  This is very convenient for use in `for` loops, since it already breaks the file up part of the way.

        Compare the following, again with [`words.txt`](repo:resources/words.txt) in your working directory:

        1.  With `read()`:

                myfile = open( 'words.txt','r' )
                mydata = myfile.read()
                myfile.close()

                print( type( mydata ) )
                print( len( mydata ) )
                print( mydata[ 0 ] )

        2.  With `readlines()`:

                myfile = open( 'words.txt','r' )
                mydata = myfile.readlines()
                myfile.close()

                print( type( mydata ) )
                print( len( mydata ) )
                print( mydata[ 0 ] )

        Although we introduced `read` first, many—perhaps most—of the applications later on will rely on `readlines`.  You should become comfortable with both of them.
