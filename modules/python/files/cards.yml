-
    type: Page
    id: python_files__cards
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Files

        Files are stored on disk as a collection of bytes in order, so they must be read the same way.  Interestingly, much like the `input` command, reading a file is one-way:  to back up, you must open the file again.

        Our basic process will consist of:

        1.  Open the file to read.
        2.  Read all of the data into memory.
        3.  Close the file.
        4.  Use the data which were loaded.

        In code, this process will look something like the following.  (First download the file [`words.txt`](repo:resources/words.txt) to your working Python directory.)

            # 1.  Open the file to read.
            myfile = open( 'words.txt','r' )

            # 2.  Read all of the data into memory.
            mydata = myfile.read()

            # 3.  Close the file.
            myfile.close()

            # 4.  Use the data which were loaded.
            print( mydata )

        Note that `myfile` has the new type of `file`, which provides the `read`, `write`, and `close` methods.

        #   Loading Files

        We always open a file using the `open` command, built-into Python.  It accepts two arguments:  a file name, as a string; and a file _mode_, a one-character string which specifies how we intend to use the file:

        | Mode  | Application       |
        | ----- | ----------------- |
        | `'r'` | reading from file |
        | `'w'` | writing to file   |
        | `'a'` | appending to file |

        At first, we will mainly read data from files rather than writing to files.

        #    Reading Data from Files

        We read the entire file as a single string using the file method [`read`](https://docs.python.org/3/tutorial/inputoutput.html#methods-of-file-objects).  This loads everything in the file from disk into memory.  (Incidentally, this means that we can close the file right away since we don't need it anymore.)

        For many files, the string will contain multiple lines of text.  How can a string include multiple lines?  The line break or newline character, written `'\n'`, represents the beginning of a new line.

        For instance, with [`words.txt`](repo:resources/words.txt) in your working directory, try the following:

            myfile = open( 'words.txt','r' )
            mydata = myfile.read()
            myfile.close()

            print( type( mydata ) )
            print( len( mydata ) )
            print( mydata[ 0 ] )

        We will only use text-based files in CS 101.  (The alternative is to use a *binary* encoding, which makes data values look as they would in memory rather than as a string representation.)

        #   Closing Files

        You should always close a file when you're done with it.  This is helpful so that:

        1.  You won't accidentally try to read from the file again, which would yield an empty string.
        2.  You won't corrupt or lost the data in the file.  (This is particularly troublesome when writing to a file.)
