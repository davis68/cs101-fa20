-
    type: Page
    id: python_files__dicer
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Accessing Data in Bite-Sized Pieces

        The `read` method returns the contents of a file as a single string.  Frequently this is too large a piece to process, so we need to break it up first.  This lets us work with one word at a time, for instance, or one number.

        Sometimes this process is called _tokenization_, because the string gets broken into separate pieces called _tokens_.  We won't officially use this terminology, but it's common enough that I or your lab section leaders will inadvertently use it.


        #   Working with String Data

        The string method `split` can break a string up into a `list` of multiple strings which are smaller pieces.

            myfile = open( 'words.txt','r' )
            mydata = myfile.read()
            myfile.close()

            mydata_as_list = mydata.split( '\n' )
            print( type( mydata_as_list ) )
            print( len( mydata_as_list ) )
            print( mydata_as_list[ 0 ] )

        A common pattern will be to tokenize by lines (as above), and then tokenize again by commas or by spaces:

            datafilepath = 'skyscrapers.txt'
            datafile = open( datafilepath,'r' )
            data = datafile.read()
            datafile.close()

            data_as_lines = data.split( '\n' )
            for line in data_as_lines:
                building,height = line.split( ',' )
                height = float( height )
                print( f'The skyscraper {building} has a height of {height} m.' )

        -   [`skyscrapers.txt`](repo:resources/skyscrapers.txt)

        That last block of code generated a single error, didn't it?  The problem can be seen by checking each line of the file first:

            for line in data_as_lines:
                print( line )
                building,height = line.split( ',' )
                height = float( height )
                print( f'The skyscraper {building} has a height of {height} m.' )

        There's an additional blank line in there.  Try this:

            for line in data_as_lines:
                if len( line.split( ',' ) ) < 2:
                    continue
                building,height = line.split( ',' )
                height = float( height )
                print( f'The skyscraper {building} has a height of {height} m.' )

        **The `continue` keyword means that the rest of that iteration gets skipped**, and the code continues as if nothing happened except the dummy variable `line` were updated.  (See below.)

        Many times data read from a file have additional whitespace characters which require `strip`ping.  Recall that the `strip` method is a string method to remove whitespace from both ends of a string.

        The following program reads in numbers from a file [`numbers.txt`](repo:./resources/numbers.txt) and attempts to convert them into `float`s.

            datafile = open( 'numbers.txt','r' )
            data = datafile.read()
            datafile.close()

            rows = data.split( '\n' )




            data_as_lines = data.split( '\n' )
            for line in data_as_lines:
                building,height = line.split( ',' )
                height = float( height )
                print( f'The skyscraper {building} has a height of {height} m.' )


        #   Putting Strings Back Together

        While we're at it, how can we put a string which has been tokenized into a `list` of strings back together?

        Since `split` is string method which returns a list, to put things back together we need a string method which accepts a list and returns a string.  Thus, `join`.

            my_list = [ 'There','overtook','me','and','drew','me','in' ]
            ' '.join( my_list )

        This syntax is a bit counterintuitive.  The separator is used to invoke the `join` method on the list of strings.
