-
    type: Page
    id: python_intro__masks
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Basic Python Syntax

        Literals, operators, and names combine to form _expressions_, which are mathematical-like statements which _return_ a value (or result in a value).  Since the result of an expression often needs to be reused, we can store it with a name as a _variable_.

        ##  Expressions and Statements

        Expressions are like phrases:  they are generally compound terms made up of names, literals, and operators.  Some expressions include:

            7 + 8
            x + 5
            ( 1 / 2 )
            ( 2 + 3 ) / 5

        In order to resolve an expression, one works from the inside out.  Each subexpression has a value which is fed into the next highest expression.  Consider the following nested expression[:](https://en.wikipedia.org/wiki/Baum%C3%A9_scale)  <!-- egg -->

            ((4 + 5) * (3 / 2)) / (4 * 7)

        We refer to pieces of this expression, like `4 + 5`, as expressions, subexpressions, or terms—it doesn't really matter which.  To resolve this, you need to iteratively solve the innermost term:

            ((4 + 5) * (3 / 2)) / (4 * 7)
            ((  9  ) * ( 1.5 )) / (  28 )
            (       13.5      ) / 28
            0.48214285714285715

        Statements, in contrast, are like sentences.  Statements cause something to change as a result of their execution, while expressions just generate values.  Expressions may be part of statements.  Some example statements are:

            x = 5 / 3

            import math
            radius = 5
            print( 2 * math.pi * radius )

            assert 3 > -3

        Each of these have an effect:

        -   `x = 5 / 3` defines a new variable `x` with value `1.6666666666666667`
        -   The cluster of statements imports a library and prints the result of an area calculation based on $\pi$.
        -   `assert 3 > -3` is used to check that a test (the inequality) is true.

        Expressions frequently use operators.  You should become familiar with the following numeric operators:

        -   `+`, `-`, `*`, `/` arithmetic
        -   `**` exponentiation
        -   `%` modulus (remainder)
        -   `//` floor division
        -   `=` assignment

        You do not need to be familiar with the bitwise operators (which manipulate the binary representation of a value), but be careful:  `^` means `XOR`, exclusive or, _not_ exponentiation.

        Modulus, the remainder operator, frequently strikes students as odd when they first encounter it.  Why would we need to know the remainder?  Lots of cases use this behavior though, like when you need to figure out which day of the week is nine days from now:

            today = 1
            nine_days = ( today + 9 ) % 7  # only seven days in a week

        What sorts of things behave like the modulus operator?  Clocks, calendars, the child's game "eenie-meenie-miney-moe", musical scales, and all circumstances in which we wrap back around to the beginning repeatedly.


        ##  Variables

        Variables name memory locations so that data can be accessed after the fact.

            x = 5
            y = 4
            z = x * y
            print( z )

        In this example, `x`, `y`, and `z` are variables which store persistent but changeable values.

        It's a good idea to give variables names that reflect their use.  That is, the following relation holds:

        $$
        \texttt{x}
        <
        \texttt{l}
        <
        \texttt{length}
        <
        \texttt{length_in_meters}
        $$

        Actually, that last one (`length_in_meters`) may be a little over-the-top.  We also want our variable names to be short enough to type quickly.  `length` is sufficient in this context.


        ##  Programs

        Now that we have all of these elements, we can offer a concise statement of what it is that we're making when we program:

        > A _program_ is a series of statements carried out on a computer in pursuit of a goal.

        There are three ways to write Python programs:

        1.  Via direct interaction (`python.exe` or online), like a graphing calculator
        2.  As a script (in a text editor), saving a set of commands for future use
        3.  In a _notebook_ (Jupyter), an interactive browser

        You will use all three methods throughout the semester.
