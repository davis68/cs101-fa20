-
    type: Page
    id: python_loops__zoink
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   The Accumulator Pattern
        
        Frequently, we have available a large set of data or a formula for which we would like to know one or more summary values.  We set up a counter or accumulator of some kind, and then fill it in by examining the formula or data.  We commonly employ loops to accumulate values (although this is not always necessary).
        
        The idea of an accumulator pattern is to
        
        1.  Set up an accumulator (frequently a zero, but see below).
        2.  Loop through a container or collection (such as `range` or a list).
        3.  Each time some value occurs, add (multiply, etc.) it to the accumulator.
        
        For convenience, we will frequently use a shorthand for accumulators, `x += 1`.  This is the same thing as writing `x = x + 1`.  There is an analogous `-=`, `*=`, etc.  You will frequently see these used and should get comfortable with them as shorthand[.](https://en.wikipedia.org/wiki/Texas_Instruments_TI-99/4A)  <!-- egg -->
        
        
        ##  Summation Accumulator
        
        A very common mathematical operation which we may consider an accumulator pattern is _summation_.
        
        $$
        \sum_{i=1}^{10} \frac{1}{i^{2}}
        $$
        
        To calculate this by hand, we could calculate each term in a table and then add them together.
        
        | Term | Series | Value | Running Total |
        | ---- | ------ | ----- | ------------- |
        |  $1$ | $\frac{1}{1}$ | $1$ | $1$ |
        |  $2$ | $\frac{1}{2^2} = \frac{1}{4}$ | $0.25$ | $1.25$ |
        |  $3$ | $\frac{1}{3^2} = \frac{1}{9}$ | $0.1111$ | $1.3611$ |
        |  $4$ | $\frac{1}{4^2} = \frac{1}{16}$ | $0.0625$ | $1.4236$ |
        |  $5$ | $\frac{1}{5^2} = \frac{1}{25}$ | $0.04$ | $1.4636$ |
        |  $6$ | $\frac{1}{6^2} = \frac{1}{36}$ | $0.0278$ | $1.4913$ |
        |  $7$ | $\frac{1}{7^2} = \frac{1}{49}$ | $0.0204$ | $1.5118$ |
        |  $8$ | $\frac{1}{8^2} = \frac{1}{64}$ | $0.0156$ | $1.5274$ |
        |  $9$ | $\frac{1}{9^2} = \frac{1}{81}$ | $0.0123$ | $1.5398$ |
        | $10$ | $\frac{1}{10^2} = \frac{1}{100}$ | $0.010$ | $1.5500$ |
        
        This is a bit awkward to carry out manually, and as you can imagine this grows in difficulty as more and more terms are added, or the limit to infinity is taken.  Better, we can employ a loop to set up this program.  We will demonstrate this with both a `while` loop and a `for` loop:
        
        Example of summation with a `for` loop:
        
            total = 0
            for i in range( 1,11 ):
                total += 1 / i ** 2
        
        Example of summation with a `while` loop:
        
            total = 0
            i = 0
            while i <= 10:
                i += 1
                total += 1 / i ** 2
        
        Most accumulator patterns will look something like these:  a collection is set up (frequently a zero or equivalent for the calculation), followed by the process of sifting the data into the summary value.
        
        
        ##  Product Accumulator
        
        A sum naturally counts from zero, so we set up the procedure to count upwards from zero.  A product, on the other hand, shouldn't be set up starting from zero, but from _one_—otherwise the value would never change!
        
        $$
        \prod_{i=}^{10} \frac{4 n^2}{4 n^2-1}
        $$
        
        | Term | Series | Value | Running Product |
        | ---- | ------ | ----- | --------------- |
        |  $1$ | $\frac{4}{3}$ | $1.333$ | $1.333$ |
        |  $2$ | $\frac{4\times 4}{4\times 4-1} = \frac{16}{15}$ | $1.067$ | $1.422$ |
        |  $3$ | $\frac{4\times 9}{4\times 9-1} = \frac{36}{35}$ | $1.029$ | $1.463$ |
        |  $4$ | $\frac{4\times 16}{4\times 16-1} = \frac{64}{63}$ | $1.016$ | $1.486$ |
        |  $5$ | $\frac{4\times 25}{4\times 25-1} = \frac{100}{99}$ | $1.016$ | $1.501$ |
        |  $6$ | $\frac{4\times 36}{4\times 36-1} = \frac{144}{143}$ | $1.007$ | $1.512$ |
        |  $7$ | $\frac{4\times 49}{4\times 49-1} = \frac{196}{195}$ | $1.005$ | $1.519$ |
        |  $8$ | $\frac{4\times 64}{4\times 64-1} = \frac{256}{255}$ | $1.004$ | $1.525$ |
        |  $9$ | $\frac{4\times 81}{4\times 81-1} = \frac{324}{323}$ | $1.003$ | $1.530$ |
        | $10$ | $\frac{4\times 100}{4\times 100-1} = \frac{400}{399}$ | $1.003$ | $1.534$ |
        
        Example of summation with a `for` loop:
        
            prod = 1
            for i in range( 1,11 ):
                prod += (4 * i**2) / (4 * i**2 - 1)
        
        Example of summation with a `while` loop:
        
            prod = 0
            i = 0
            while i <= 10:
                i += 1
                prod += (4 * i**2) / (4 * i**2 - 1)
        
        For CS 101, there are at least five different major ways to set up accumulator patterns using loops.  Right now we are only concerned with two of them:  the _summation accumulator_ and the _product accumulator_, both of which we have demonstrated above.


        ##  Other Accumulators
        
        Built-in functions in Python or library-provided functions can also act as accumulators.  Since an accumulator variable is a way of summarizing or aggregating information in a single place, certain functions also serve as single-line accumulators.  Some of these include:
        
        -   `sum`.
        -   `max` and `min`.
        -   `str.count`.  For instance, `'Abracadabra`.count('a')'`
        
        Since there are so many ways to aggregate or accumulate a summary value, we can't show you "the" accumulator pattern.  Rather, it's a way of writing code that you'll come to recognize as you see and implement examples.
