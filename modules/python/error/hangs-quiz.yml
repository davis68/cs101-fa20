-
    type: ChoiceQuestion
    id: python_error__hangs_try1
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following program:

            a = [ "1",2,"3",0 ]
            x = 0
            for e in a:
                try:
                    x += e
                except:
                    x += 1

        After it has run, what is the final *value* of `x`?

    choices:

    -   "1"
    -   "2"
    -   "3"
    -   "~CORRECT~ 4"
    -   "5"


-
    type: ChoiceQuestion
    id: python_error__hangs_try2
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following program:

            x = "1 2 3".split()
            x = ','.join( x )
            try:
                print( x.append( 4 ) )
            except:
                print( type( x ) )

        After it has run, what is printed by the program?

    choices:

    -   "`TypeError`"
    -   "`[ 1,2,3,4 ]`"
    -   "`<class 'list'>` (output from calling the `type` function on a list)"
    -   "~CORRECT~ `<class 'str'>` (output from calling the `type` function on a string)"


-
    type: ChoiceQuestion
    id: python_error__hangs_exc1
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following Python program.

            d = { 'red':0,'green':1,'blue':2,'jale':3,'ulfire':4,'octarine':5 }
            for c in [ 'red','green,'blue' ]:
                try:
                    print( d[ c ] + 5 )
                except:
                    continue

        Which uncaught exception will cause this program to cease execution?

    choices:

    -   "`ValueError`"
    -   "`KeyError`"
    -   "`TypeError`"
    -   "~CORRECT~ `SyntaxError`"
    -   "None of the above—the program will execute normally."


-
    type: ChoiceQuestion
    id: python_error__hangs_try3
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following Python program.

            d = { "T":2,"U":1,"C":4,"K":1 }
            for c in "FRIAR":
                try:
                    print( d[ c ] + 3 )
                except KeyError:
                    pass

        Which uncaught exception will cause this program to cease execution?


    choices:

    -   "`IndexError`"
    -   "`KeyError`"
    -   "`TypeError`"
    -   "~CORRECT~ None of the above—the program will execute normally."


-
    type: ChoiceQuestion
    id: python_error__hangs_try4
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following Python program.

            ???
            sin(math.pi)+cos(math.pi)

        What should replace the three question marks to produce a program that runs without raising an exception?

    choices:

    -   "~CORRECT~ <pre><code>from math import sin,cos<br/>import math</code></pre>"
    -   "<pre><code>from math import *<br/>import sin,cos</code></pre>"
    -   "<pre><code>import pi,sin,cos from math</code></pre>"
    -   "<pre><code>import math as pi<br/>import math as sin<br/>import math as cos</code></pre>"


-
    type: ChoiceQuestion
    id: python_error__hangs_try5
    value: 1
    title: "Checkpoint: Exception Handling"
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        Consider the following Python program.

            d = { "T":2,"U":1,"C":4,"K":1 }
            for c in "CKTU":
                print( "FRIAR" + d[ c ] )

        Which uncaught exception will cause this program to cease execution?

    choices:

    -   "`IndexError`"
    -   "`KeyError`"
    -   "~CORRECT~ `TypeError`"
    -   "`IndexError`"


-
    type: ChoiceQuestion
    id: python_error__hangs_try6
    title: "Checkpoint: Exception Handling"
    value: 1
    prompt: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Exception handling

        For this problem, your job is to put the lines of code below in the proper order to create a function that accomplishes a task. We will completely ignore indentation.

        1.  `except:`
        2.  `def is_close( a,b,rtol=1e-3 ):`
        3.  `try:`
        4.  `return None`
        5.  `def is_close( a,b,rtol )`
        6.  `rtol = 1e-3`
        7.  `return ( abs(a-b)/abs(b) <= rtol )`
        8.  `return ( (a-b)/b <= rtol )`

        The function you should write is called `is_close`, and it should accept a two numbers, `a` and `b`.  An optional third argument is the relative tolerance `rtol` with default value `1e-3`.  `is_close` returns `True` or `False` depending on whether the numbers are closer than `rtol`:

        $$
        \frac{|a-b|}{|b|} \leq \texttt{rtol} \rightarrow \texttt{True}
        \hspace{3cm}
        \frac{|a-b|}{|b|} > \texttt{rtol} \rightarrow \texttt{False}
        \text{.}
        $$

        The code should return `None` if the calculation fails (for instance, if the parameters `a` or `b` are non-numeric).

        What is the proper selection and ordering of the given lines of code?

    choices:

    -   "5,6,3,7,1,4"
    -   "2,7"
    -   "2,3,8,1,4"
    -   "~CORRECT~ 2,3,7,1,4"
    -   "5,6,3,8,1,4"
