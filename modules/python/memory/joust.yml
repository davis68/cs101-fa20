-
    type: Page
    id: python_memory__joust
    content: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Mutability

        From the Python docs, "Objects whose value can change are said to be mutable; objects whose value is unchangeable once they are created are called immutable." [[0](https://docs.python.org/3/reference/datamodel.html)].

        Some things can change value:  `list`s, for instance.  Other things cannot:  `str`s are _immutable_.

            s = "good advise"
            s[ 9 ] = 'c'                 # nope, this doesn't work

            s = s[ :9 ] + 'c' + s[ 10: ]    # this way is correct

        #   `tuple`

        There is also an _immutable_ analogue of the `list`:  the `tuple`.  The `tuple` is basically a list of values separated by commas.  You've used them already.

            x,y,z = 1,2,3

        Here, a `tuple` is used to assign a list of values to a list of variables.

            1,2,3
            ( 1,2,3 )

            a = ( 1,2,3 )
            a[ 0 ]          # indexing works as for lists

        Since `( 1.0 )` reduces to `1.0`, a one-element tuple requires an extra comma:  `( 1.0, )`

        Immutable data types include:  `str`, `tuple`.

        Mutable data types include:  `list`, `dict`.

        This phenomenon explains why many `list` methods change the `list` in place:

        -   `sort` sorts in place
        -   `reverse` reverses in place
        -   `append` appends to the `list` in place

        Thus, these all `return None`, or rather, return nothing.  If you assign to the result of one of these, you clobber your variable:

            x = [ 1,2,3,4,5 ]
            x = x.sort()
            print( x )          # nothing results

        In short:  remember which functions are _fruitful_ (return a value), and which are not (do not return a value).  You can always test this in Python if you are confused.
