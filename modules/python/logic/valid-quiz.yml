-
    type: MultipleChoiceQuestion
    id: boolean_operators
    value: 1
    credit_mode: proportional
    title: "Checkpoint:  Boolean Logic and Operators"
    prompt: |
        #   True or False
        ```
        A = True
        B = False
        ```
        Which of the following will evaluate to True?
    choices:
    - "```A == B ```"
    - "```not A```"
    - "~CORRECT~ ```A != B```"
    - "~CORRECT~ ```A or B```"
    - "~CORRECT~ ```not B```"


-
    type: InlineMultiQuestion
    id: python_logic__valid_bool0
    value: 1
    title: "Checkpoint:  Boolean Values"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Boolean Values

        For each of the following expressions, what value should `x` have in order to make the statement `True`?

        Type the answer such as `True` or `5`.  Python is case-sensitive (that is, `true` is not the same as `True`).

    question: |

        | Expression |   `x`    |
        | ---------- | ---------- |
        | `x and True` | `x = ` [[blank0]] |
        | `x or True` | `x = ` [[blank1]] |
        | `(x == 5) and (not False)` | `x = ` [[blank2]] |

    answers:

        blank0:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> True

        blank1:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> True
            - <plain> False

        blank2:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 5


-
    type: MultipleChoiceQuestion
    id: compound_expressions
    value: 1
    credit_mode: proportional
    title: "Checkpoint:  Compound expressions"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #  Compound Expressions

        Which option(s) below evaluate to true given a = true and b = false?

    choices:
    - "~CORRECT~ `(a and a) or b`"
    - " `(not a) or (not (a and True))`"
    - " `False or b or (b and a) or (not a)`"
    - "~CORRECT~ `a and True and (a or b)`"
    - "~CORRECT~ `not (a and b)`"


-
    type: ChoiceQuestion
    id: python_logic__valid_bool1
    value: 1
    title:  "Checkpoint:  Logic"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Logic

        Which of the following statements will evalue to `True`?

    choices:
    -   <code>not (True and (False or True))</code>
    -   <code>False or not (3 > 1)</code>
    -   ~CORRECT~ <code>(True or not False) or (12 > 10)</code>
    -   <code>(1 < 2) and (3 <= 2)</code>
    -   <code>(1 == 1) and not (-1 < 0)</code>


-
    type: MultipleChoiceQuestion
    id: find_the_operator
    value: 1
    credit_mode: proportional
    title: "Find the Operator"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #  Find the Operator

        Given the following expression

        ```
        (True ___ False) ___ ___ False
        ```

        Which operators could fill in the blanks to make the expression evaluate to true?

    choices:
    - "~CORRECT~ `or · and · not`"
    - " `or · or · or`"
    - " `and · and · not`"
    - "~CORRECT~ `or · or · not`"
    - "~CORRECT~ `and · or · not`"
