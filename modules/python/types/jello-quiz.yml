-
    type: InlineMultiQuestion
    id: python_types__jello_indexing
    value: 1
    title: "Checkpoint:  Indexing Strings"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Indexing Strings

        Consider the following string.

            line = '''Mind your own business.'''

        How would you access each of the following letters through indexing?  Use either positive or negative indices.

    question: |
        | Character |   Index    |
        | --------- | ---------- |
        |   `'M' `  | [[blank0]] |
        |   `'y'`   | [[blank1]] |
        |   `'w'`   | [[blank2]] |
        |   `'.'`   | [[blank3]] |

    answers:
        blank0:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 0
            - <plain> -23

        blank1:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 5
            - <plain> -18

        blank2:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 11
            - <plain> -12

        blank3:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 22
            - <plain> -1


-
    type: InlineMultiQuestion
    id: python_types__jello_slicing
    value: 1
    title: "Checkpoint:  Slicing Strings"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Slicing Strings

        Consider the following string.

            stanza = '''The age demanded that we dance
            And jammed us into iron pants.'''

        What range would you use to select each of the following substrings?  Prefer negative indices to count down for values near the end.  Although a blank value may be correct in Python, you'll need to input a number rather than an empty answer or a space to complete the question.

        (If you see a `\n`, this is a shorthand representing a line break inside of a string.)

    question: |
        | Substring | Range |
        | --------- | ----- |
        | `'age'` | `stanza[` [[blank0]] `:` [[blank1]] `]` |
        | `'pants.'` | `stanza[` [[blank2]] `:` [[blank3]] `]` |
        | `'pants'` | `stanza[` [[blank4]] `:` [[blank5]] `]` |
        | `'he age demanded that we dance`<br/>`And jammes us into iron pant` | `stanza[` [[blank6]] `:` [[blank7]] `]` |

    answers:
        blank0:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 4
            - <plain> -57

        blank1:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 7
            - <plain> -54

        blank2:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> -6
            - <plain> 55

        blank3:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 61
            - <case_sens_regex>grep\s*

        blank4:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> -6
            - <plain> 55

        blank5:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> -1
            - <plain> 60
            - <case_sens_regex>grep\s*

        blank6:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> 1
            - <plain> -60

        blank7:
            type: ShortAnswer
            width: 4em
            required: True
            correct_answer:
            - <plain> -2
            - <plain> 59


-
    type: TextQuestion
    id: indexing_and_slicing
    value: 1
    title: "Checkpoint: Indexing a String"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Indexing Strings

        ```
        x = 'Do. Or do not. There is no try.'
        print(x[14:-1])
        ```

        If the above code runs, what part of the string results?

    answers:
        - <plain>There is no try


-
    type: TextQuestion
    id: reconstructing_strings
    value: 1
    title: "Checkpoint: Reconstructing Strings"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Reconstructing Strings

        Consider the following two strings.

        ```
        stringA = "It was cold all summer long"

        stringB = "It was hot all winter long"
        ```

        `stringC` rearranges the two strings from above.

        ```
         stringC = stringA[0:-11] + stringB[-11:]
        ```

        Write the sentence output of `stringC`.  Make sure to include string indicators (quotes) at the ends, if appropriate.

    answers:
        - <plain>"It was cold all winter long"
        - <plain>'It was cold all winter long'


-
    type: MultipleChoiceQuestion
    id: stride_while_slicing_strings
    value: 1
    title: "Checkpoint: Stride while Slicing Strings"
    credit_mode: proportional
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Stride while Slicing Strings

        The third parameter in string slicing specifies the **stride**, which refers to how many indexes to move forward after the first character is retrieved.

        If the third parameter is not specified, then the stride defaults to 1, retrieving every character in the string.

        Consider the string.

        ```
        motto = "Don't give up"
        ```

        The output of both slices below will be the same, resulting in `"Don't"`

        ```
        motto[0:5] # Stride defaults to 1

        motto[0:5:1]
        ```

        Select the option(s) that would retrieve every other character, resulting in `"Dntgv p"`

    choices:
        - "~CORRECT~ `motto[0::2]`"
        - "`motto[0:2]`"
        - "`motto[0:2:1]`"
        - "~CORRECT~ `motto[::2]`"
        - "`motto[0:2:2]`"
        - "~CORRECT~ `motto[:13:2]`"
