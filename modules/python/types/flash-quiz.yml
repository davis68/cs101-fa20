-
    type: ChoiceQuestion
    id: python_types__flash_imag_value
    value: 1
    title:  "Checkpoint:  Attributes of Complex Numbers"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Attributes of Complex Numbers

        Consider the following program.

            a = -5.0
            b = 3
            c = a + b*1j

        What is the _value_ of `c.imag`?

    choices:

    -   "~CORRECT~ `3.0`"
    -   "`3j`"
    -   "`5.0`"
    -   "`-5+3j`"


-
    type: ChoiceQuestion
    id: python_types__flash_imag_type
    value: 1
    title:  "Checkpoint:  Attributes of Complex Numbers"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Attributes of Complex Numbers

        Consider the following program.

            a = -5.0
            b = 3
            c = a + b*1j

        What is the _type_ of `c.imag`?

    choices:

    -   "`int`"
    -   "~CORRECT~ `float`"
    -   "`complex`"


-
    type: PythonCodeQuestion
    id: writing_attributes_complex_numbers
    value: 1
    timeout: 10
    title: "Checkpoint: Writing Attributes of Complex Numbers"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Writing Attributes of Complex Numbers

        Define a variable named `complex`, for which `print(complex.real)` would output `5.0` and `print(complex.imag)` would output `8.5`.

    names_for_user: []

    names_from_user: [ complex ]

    test_code: |
        points = 0.0
        try:
            import inspect
            from numpy import isclose
            import numpy as np

            # 100% for `complex`
            if isclose(5 + 8.5j, complex):
                points += 1
            else:
                feedback.add_feedback('Your value for complex is incorrect. Remember, the format of complex numbers is a + bj')

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (ValueError):
            feedback.finish( points,'Are any arrays the correct sizes, and are you not trying to use them with any `if` statements?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your solution.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
      complex = 5 + 8.5j
