-
    type: PythonCodeQuestion
    id: python_types__shark_import0
    value: 1
    timeout: 10
    title: "Checkpoint:  Importing a Module"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Importing a Module

        Import the `itertools` module.

    setup_code: |

    initial_code: |

    names_for_user: [ ]

    names_from_user: [ itertools ]

    test_code: |
        pts = 0.0

        try:
            import numpy as np
            assert itertools.__dir__()[ 0 ] == '__name__'
            pts = 1.0

        except (NameError,AssertionError):
            feedback.finish( 0.0,'Have you imported `itertools` using the `import` keyword?' )
        except:
            feedback.finish( 0.0,'Something broke.  Check your `import` statement for the `itertools` module.' )

        if np.isclose( pts,1.0 ):
            feedback.finish( pts,'Success!  `itertools` has been imported.' )
        else:
            feedback.finish( pts,'Keep trying.  If you get stuck, please ask a TA for help on Piazza or office hours.' )


    correct_code: |
        import itertools


-
    type: PythonCodeQuestion
    id: python_types__shark_import1
    value: 1
    timeout: 10
    title: "Checkpoint:  Composing Expressions"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Composing Expressions

        Write an expression for the following formula.  You may assume that `m` and `n` are already defined.

        $$
        y = \frac{(m+n)!}{(m+1)(n-1)!}
        $$

        (Note that the factorial refers only to the second term of the denominator, not the entire denominator.)

        You will need `factorial` from the `math` module.

    setup_code: |
        m,n = 5,4

    initial_code: |
        # m,n already have been defined

    names_for_user: [ m,n ]

    names_from_user: [ y ]

    test_code: |
        pts = 0.0

        try:
            import numpy as np
            import math
            test_y = math.factorial( m+n ) / ( m+1 ) / math.factorial( n-1 )
            assert np.isclose( y,test_y,rtol=1e-6 )
            pts = 1.0

        except NameError:
            feedback.finish( pts,'Have you imported `factorial` from `math`?  Have you defined `y`?' )
        except AssertionError:
            feedback.finish( pts,'Check your expression for y.' )
        except:
            feedback.finish( pts,'Something broke.  Check your `import` statement for the factorial.' )

        if np.isclose( pts,1.0 ):
            feedback.finish( pts,'Success!' )
        else:
            feedback.finish( pts,'Keep trying.  If you get stuck, please ask a TA for help on Piazza or office hours.' )

    correct_code: |
        import math
        y = math.factorial( m+n ) / ( m+1 ) / math.factorial( m+1 )


-
    type: PythonCodeQuestion
    id: python_types__shark_from
    value: 1
    timeout: 10
    title: "Checkpoint:  Importing Names"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Importing Names

        Import `log10` from the `math` module.  (This is the logarithm base 10, $\log_{10}(x)$.)

        Your answer should include a `from` keyword.

    setup_code: |

    initial_code: |

    names_for_user: [ ]

    names_from_user: [ log10 ]

    test_code: |
        pts = 0.0

        try:
            import numpy as np
            assert np.isclose( log10( 5 ),np.log10( 5 ),rtol=1e-6 )
            pts = 1.0

        except NameError:
            feedback.finish( 0.0,'Have you imported `log10` from `math` (instead of importing `math` altogether)?' )
        except:
            feedback.finish( 0.0,'Your code doesn\'t seem to be executable at all.  If you get stuck, please ask a TA for help on Piazza or office hours.' )

        if np.isclose( pts,1.0 ):
            feedback.finish( pts,'Success!  `log10` has been imported.' )
        else:
            feedback.finish( pts,'Keep trying.  If you get stuck, please ask a TA for help on Piazza or office hours.' )

    correct_code: |
        from math import log10


-
    type: PythonCodeQuestion
    id: math_library_exp_func
    value: 1
    title: "Checkpoint: Importing Libraries"
    access_rules:
        add_permissions:
            - change_answer
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Using the `math` Library

        Import the function `exp` from `math` to solve the following expression for `x = 1.25`:

        $$
        \frac{(e^{x} + e^{-x})}{2}
        $$

        Save the answer in the variable `E`.

    names_for_user: []
    names_from_user: [E]
    timeout: 10
    test_code: |
        points = 0.0

        try:
            import inspect
            from numpy import isclose,allclose
            import numpy as np
            import math

            def test_exp( E ):
                E2 = 2 * E
                E2 = E2 - math.exp(1.25)
                return E2

            if isclose( test_exp(E), math.exp(-1.25) ):
                points = points + 1
            else:
                feedback.add_feedback( 'Your value for seconds is incorrect.' )

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all. Are all the variables defined?' )
        except (ValueError):
            feedback.finish( points,'Are all variables and functions in the right order?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Have you used a function that isn\'t defined? Did you import the correct library?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code seems to be taking too long.' )
        if points < 1.0:
            feedback.finish( points,'Please check your solution again.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )
    correct_code: |
        import math
        x = 1.25
        E1 = math.exp(x) + math.exp(-x)
        E = E1 / 2
