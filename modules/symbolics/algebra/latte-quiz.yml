-
    type: PythonCodeQuestion
    id: symbolics_algebra__latte_eqn0
    value: 1
    timeout: 10
    title:  "Checkpoint:  Solving Expressions Analytically"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Solving Expressions Analytically

        Consider the following equation:

        $$
        \tan a
        =
        2
        \text{.}
        $$

        In order to solve this, you first have to restate it as being equal to zero:

        $$
        \tan a - 2
        =
        0
        \text{.}
        $$

        Next, you feed this left-hand expression to `sympy` and ask it to `solve` for $a$.  Your answer should be stored in a variable `result`, which will be a `list` containing one `sympy` expression.

    setup_code: |

    initial_code: |
        import sympy
        a = sympy.S( 'a' )

    names_for_user: [ ]

    names_from_user: [ result ]

    test_code: |
        points = 0.0

        try:
            if not type( result ) == list:
                feedback.finish( points,'You haven\'t provided a list of solutions `result`.' )
            points += 0.1

            if not len( result ) == 1:
                feedback.finish( points,'Your list `result` doesn\'t have the proper number of entries.  (I expect 1.)' )
            points += 0.1

            import sympy
            if not type( result[ 0 ] ) is sympy.atan:
                feedback.finish( points,'Your result seems to be incorrect.  I expect a trigonometric function.' )
            points += 0.4

            test_a = sympy.S( 'a' )
            test_eqn = sympy.tan( test_a ) - 2
            test_result = sympy.solve( test_eqn,test_a )
            if result[ 0 ] != test_result[ 0 ]:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.4

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the equation.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        a = sympy.S( 'a' )
        result = sympy.solve( sympy.tan( a ) - 2,a )


-
    type: PythonCodeQuestion
    id: symbolics_algebra__latte_eqn1
    value: 1
    timeout: 10
    title:  "Checkpoint:  Solving Expressions Analytically"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Solving Expressions Analytically

        Consider the following equation:

        $$
        x^3 + 4 x^2 + 4 x + 16
        =
        0
        \text{.}
        $$

        Solve for $x$.  Your answer should be stored in a variable `result`, which will be a `list` containing three `sympy` expressions.

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ result ]

    test_code: |
        points = 0.0

        try:
            if not type( result ) == list:
                feedback.finish( points,'You haven\'t provided a list of solutions `result`.' )
            points += 0.1

            if not len( result ) == 3:
                feedback.finish( points,'Your list `result` doesn\'t have the proper number of entries.  (I expect 3.)' )
            points += 0.1

            import sympy
            if not type( result[ 0 ] ) is sympy.Integer:
                feedback.finish( points,'Your result seems to be incorrect.  I expect an integer.' )
            points += 0.3

            test_x = sympy.S( 'x' )
            test_result = sympy.solve( 'x**3 + 4*x**2 + 4*x + 16',test_x )

            if result[ 0 ] != test_result[ 0 ] and \
               result[ 1 ] != test_result[ 1 ] and \
               result[ 2 ] != test_result[ 2 ]:
                feedback.finish( points,'Your results seem to be incorrect.' )
            points += 0.5

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the equation.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        result = sympy.solve( x**3 + 4*x**2 + 4*x + 16,x )


-
    type: PythonCodeQuestion
    id: symbolics_algebra__latte_eqn2
    value: 1
    timeout: 10
    title:  "Checkpoint:  Solving Expressions Analytically"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Solving Expressions Analytically

        Consider the following equation:

        $$
        e^{\pi i} - x
        =
        0
        \text{.}
        $$

        `sympy` provides $e$ as `E`, $\pi$ as `pi`, and $i = \sqrt{-1}$ as `I`.

        Solve for $x$.  Your answer should be stored in a variable `result`, which will be a `list` containing one `sympy` expression.

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ result ]

    test_code: |
        points = 0.0

        try:
            if not type( result ) == list:
                feedback.finish( points,'You haven\'t provided a list of solutions `result`.' )
            points += 0.1

            if not len( result ) == 1:
                feedback.finish( points,'Your list `result` doesn\'t have the proper number of entries.  (I expect 1.)' )
            points += 0.1

            import sympy
            if not type( result[ 0 ] ) is type( sympy.S( '-1' ) ):
                feedback.finish( points,'Your result seems to be incorrect.  I expect a number.' )
            points += 0.3

            test_x = sympy.S( 'x' )
            test_result = sympy.solve( sympy.E**(sympy.pi*sympy.I)-test_x,test_x )
            if result[ 0 ] != test_result[ 0 ]:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.5

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the equation.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        result = sympy.solve( sympy.E**(sympy.pi*sympy.I)-x,x )
