-
    type: InlineMultiQuestion
    id: symbolics_algebra__abbes_params
    value: 3
    title: "Checkpoint:  Parameters and Unknowns"
    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Parameters and Unknowns

        There is a subtle but pervasive distinction made in mathematical and physical equations that we need to address up front:  the difference between _variables_ and _parameters_.

        Consider again the quadratic equation:

        $$
        a x^2 + b x + c = 0 \\
        x = \frac{-b\pm \sqrt{b^2-4ac}}{2a}
        $$

        We call $x$ the variable or the unknown, while we refer to $a$, $b$, and $c$ as parameters.  Essentially, the variable or unknown is the quantity that is expected to vary while the parameters, once chosen, remain constant.

        What differs is how these are _treated_:  we say that the quadratic equation has one variable or unknown and three parameters.  We don't say that the quadratic equation has four variables even though we don't know $a$, $b$, and $c$ at the time we write down the equation.

        Confusingly, sometimes function _arguments_ are called function _parameters_ by programmers, but please don't confuse this use of "parameter" with the mathematical equation's "parameter."

    question: |

        In the following equations, please identify which symbols refer to unknowns and which refer to parameters.  Due to common mathematical usage, parameters tend to use letters like $a$, $b$, and $c$, while unknowns tend to use $t$, $x$, $y$, and $z$:  but not always!  You'll need to infer the intent of the equation's author sometimes or check the text to know for sure.

        I'll also throw in some _constants_, which are fixed values like $\pi$ that you'll either recognize from experience or I'll indicate to you.

        Getting a sense of this is a little tricky, but it shouldn't feel arbitrary.  Ask a TA or the instructor for insight if you are confused about why a quantity is a variable or a parameter.

        $mx+b$ (the equation of a line)

        - $m$, [[blank_00]]
        - $x$, [[blank_01]]
        - $b$, [[blank_02]]

        $a \cos \phi + a \phi \sin \phi$ (the $x$ component of the involute of a circle)
        - $a$, [[blank_0]]
        - $\phi$, [[blank_1]]

        $PV = nRT$, where $R = 8.31446…$ is the gas constant (the ideal gas law relating pressure, volume, and temperature of $n$ moles of gas)
        - $P$, [[blank_2]]
        - $V$, [[blank_3]]
        - $n$, [[blank_4]]
        - $R$, [[blank_5]]
        - $T$, [[blank_6]]

        $\frac{k}{18 \text{ln} \frac{r_2}{r_1} \times 10^5}$ (capacitance of two coaxial cylinders in microfarads for material of dielectric constant $k$)
        - $k$, [[blank_7]]
        - $r_1$, [[blank_8]]
        - $r_2$, [[blank_9]]

        $2\pi \int_c^d dy (x-k)\sqrt{1+\left( \frac{dx}{dy} \right)^2}$ (the area of surface of revolution of an arc from $(a,c)$ to $(b,d)$ about $x=k$)
        - $\pi$, [[blank_10]]
        - $c$, [[blank_11]]
        - $d$, [[blank_12]]
        - $x$, [[blank_13]]
        - $k$, [[blank_14]]

    answers:
        choice_00:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_01:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_02:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_0:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_1:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_2:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_3:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_4:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_5:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - Parameter
            - ~CORRECT~ Constant
        choice_6:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_7:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_8:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_9:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_10:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - Parameter
            - ~CORRECT~ Constant
        choice_11:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_12:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
        choice_13:
            type: ChoicesAnswer
            required: True
            choices:
            - ~CORRECT~ Unknown/variable
            - Parameter
            - Constant
        choice_14:
            type: ChoicesAnswer
            required: True
            choices:
            - Unknown/variable
            - ~CORRECT~ Parameter
            - Constant
