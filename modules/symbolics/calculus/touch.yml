-
    type: Page
    id: symbolics_calculus__touch
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Taylor Series Expansion

        As you may recall from calculus, a Taylor series is a way of approximating a function by its derivatives at a single point.

        $$
        f(a)
        + \frac{f'(a)}{1!} (x-a)
        + \frac{f''(a)}{2!} (x-a)^2
        + \frac{f'''(a)}{3!}(x-a)^3
        + \cdots
        $$

        ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Sintay_SVG.svg/300px-Sintay_SVG.svg.png)

        Essentially, what a Taylor series does is to break out the $n$th-degree component of a function's approximation at a point.  The sum of these yields the actual function as $n \rightarrow \infty$.

        `sympy` can expand a series at a particular point of a given function.

        $$
        \frac{1}{1-x}
        \approx
        1 + x + x^{2} + x^{3} + \cdots
        $$

            import sympy
            x = sympy.S( 'x' )
            f = 1 / ( 1 - x )
            sympy.series( f,x,0,4 )

        ![](repo:./img/symbolics_calculus__touch_series.png)
        <!--
            import sympy
            x = sympy.S('x')
            plot = sympy.plotting.plot( 1 / ( 1 - x ),( x,-1,1 ),ylim=( 0,2 ) )
            for i in range( 1,7 ):
                plot.append( sympy.plotting.plot( sympy.series( 1 / ( 1 - x ),x,0,i ).removeO(),( x,-1,1 ) )[ 0 ] )
            for i in range( 7 ):
                plot[ i ].line_color = [ 'r','g','b','m','c','y','k' ][ i % 7 ]
            plot.show()
        -->

        $$
        \left .
        \cos x
        \right|_{a=\pi}
        \approx
        -1 + \frac{\left(x-\pi\right)^{2}}{2} - \frac{\left(x-\pi\right)^{4}}{24} + \cdots
        $$

            import sympy
            x = sympy.S( 'x' )
            f = sympy.cos(x)
            sympy.series( f,x,0,6 )

        ![](repo:./img/symbolics_calculus__touch_series.png)
        <!--
            import sympy
            x = sympy.S('x')
            plot = sympy.plotting.plot( 1 / ( 1 - x ),( x,-1,1 ),ylim=( 0,2 ) )
            for i in range( 1,7 ):
                plot.append( sympy.plotting.plot( sympy.series( 1 / ( 1 - x ),x,0,i ).removeO(),( x,-1,1 ) )[ 0 ] )
            for i in range( 7 ):
                plot[ i ].line_color = [ 'r','g','b','m','c','y','k' ][ i % 7 ]
            plot.show()
        -->

        You should remove the spurious $O$ term by calling the method `removeO()` on the resulting series.


        #   Linearization

        Linearization means taking only the constant and linear components of a series expansion.  Linearization allows linear mathematical analysis techniques to be applied to more complicated nonlinear functions.

        If you are simulating a complicated function which is computationally expensive, sometimes it is worth it to linearize the function.  The benefit gained by the cheaper calculation can outweigh the error introduced by the approximation.

        $$
        f(x)
        =
        \sqrt(x)
        $$

        $$
        \left.
        \hat{f}(x)
        \right|_{x=a}
        =
        \sqrt{a} + \frac{1}{2\sqrt{a}}(x-a)
        $$

        We can obtain the linearization from `sympy` as well:

            f = sympy.sqrt( x )
            fhat = sympy.series( f,x,a,2 )

        (Keep in mind that the final number is the _number of terms_ in the series, not the _order_ of the resulting expression.)

        You should remove the spurious $O$ term by calling the method `removeO()` on the result of the linearization.

            fhat = fhat.removeO()

        Here is the linearization evaluated at $x=\frac{1}{2}$:

        ![](repo:./img/symbolics_calculus__touch_linear.png)
        <!--
            f = sympy.sqrt( x )
            fhat = sympy.series( f,x,1/2,2 ).removeO()
            fplot = sympy.plotting.plot( f,( x,1/4,3/4 ) )
            fhatplot = sympy.plotting.plot( fhat,( x,1/4,3/4 ) )
            fplot.append( fhatplot[0] )
            fplot.show()
        -->

        If we say "linearize", we mean obtain through the linear term (2 terms, constant and linear).
