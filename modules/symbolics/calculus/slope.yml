-
    type: Page
    id: symbolics_calculus__slope
    content: |

        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Differentiation

        Since we can easily represents symbolic quantities with `sympy`, manipulations such as differentiation and integration are straightforward—at least assuming an expression can be found.  (We can almost always find a _numerical_ solution, but sometimes a symbolic solution can be elusive.)

        Here is a first-order differentiation with respect to $x$:

            >>> import sympy
            >>> sympy.init_printing()
            >>> x = sympy.S( 'x' )
            >>> sympy.diff( x**2,x )
            2·x

        `sympy.diff` can accept two or three arguments.  For two arguments, `diff` expects an expression to differentiate as well as the variable to differentiate by[.](https://en.m.wikipedia.org/wiki/R%C3%A5bjerg_Mile)  <!-- egg -->

            >>> x,y = sympy.S( 'x,y' )
            >>> sympy.diff( x**y,y )
             y
            x ⋅log(x)

            >>> sympy.diff( x**y,x )
             y
            x ⋅y
            ────
             x

        For three arguments, `diff` can take the second or higher derivative as specified.

            >>> sympy.diff( x**y,x,2 )
             y
            x ⋅y⋅(y - 1)
            ────────────
                  2
                 x


        #   Integration

        Similarly, definite and indefinite integrals can be taken using `sympy.integrate`.

        **Indefinite integrals**.  Note that `sympy` doesn't explicity include the constant of integration $C$.

        $$
        \int dx\,x^{2}
        $$

            >>> sympy.integrate( x**2,x )
             3
            x
            ──
            3

        $$
        \int dx\,x^{y}
        $$

            >>> sympy.integrate( x**y,x )
            ⎧ y + 1
            ⎪x
            ⎪──────  for y ≠ -1
            ⎨y + 1
            ⎪
            ⎪log(x)  otherwise
            ⎩

        **Definite integrals**.

        $$
        \int_{0}^{\frac{\pi}{2}} dx\,\cos x
        $$

            >>> sympy.integrate( sympy.cos( x ),( x,0,sympy.pi/2 ) )
            1

        $$
        \int_{0}^{1} dx\,\sqrt{x}
        $$

            >>> sympy.integrate( sympy.sqrt( x ),( x,0,1 ) )
            2/3

        **Multiple integrals**.  Since multiple integrals have separate bounds, the expression in `sympy` becomes convoluted.  Multiple steps may be necessary for clarity.

        $$
        \int_{0}^{1} dy \int_{-1}^{+1} dx\, 2\sin^{2} x + 3 y
        $$

            >>> sympy.integrate( sympy.integrate( 2*sympy.sin(x)**2+3*y,( x,-1,+1 ) ),( y,0,1 ) )
            -2⋅sin(1)⋅cos(1) + 5

        $$
        \int_{0}^{1} dy \int_{0}^{y} dx\, \exp \left( \frac{x}{y} \right)
        $$

            >>> sympy.integrate( sympy.integrate( sympy.exp( x/y ),( x,0,y ) ),( y,0,1 ) )
              1   ℯ
            - ─ + ─
              2   2

        `sympy` can be quite useful to check your results in physics and mathematics courses where you need to solve integrals—but of course it doesn't show your work!


        #   References

        -   [Ivan Savov, "Taming Math and Physics Using `SymPy`"](https://minireference.com/static/tutorials/sympy_tutorial.pdf)
