-
    type: PythonCodeQuestion
    id: symbolics_calculus__touch_series
    value: 1
    timeout: 10
    title:  "Checkpoint:  Taylor Series"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Taylor Series

        Consider the following expression for $f$:

        $$
        f( x )
        =
        2 \sqrt{x-1}
        \text{.}
        $$

        Calculate the six-term Taylor series expansion about $x=5$.  Store your result in a variable `taylor`, which should be a `sympy` expression.  (In this case, don't worry about `removeO()`.)

        <div class="alert alert-warning">
        You should use <code>sympy.sqrt</code> instead of <code>**0.5</code>, as the latter introduces an imprecise float into your result.
        </div>

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ taylor ]

    test_code: |
        points = 0.0

        import sympy
        test_x = sympy.S( 'x' )
        test_taylor = sympy.series( 2*sympy.sqrt( test_x-1 ),test_x,5,6 )

        try:
            if not type( taylor ) == type( test_taylor ):
                feedback.finish( points,'Your result seems to be the incorrect type.' )
            points += 0.25

            if taylor != test_taylor:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the series expansion.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        taylor = sympy.series( 2*sympy.sqrt( x-1 ),x,5,6 )


-
    type: PythonCodeQuestion
    id: symbolics_calculus__touch_series_hx00
    value: 1
    timeout: 10
    title:  "Checkpoint:  Taylor Series"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Taylor Series

        Consider the following expression for $f$:

        $$
        f( x )
        =
        \cos x
        \text{.}
        $$

        Calculate the four-term Taylor series expansion about $x=\frac{\pi}{2}$.  Store your result in a variable `taylor`, which should be a `sympy` expression.  (In this case, don't worry about `removeO()`.)
        
        This is largely the same problem you solved in `hw00`.  Compare your ability to calculate the answer by hand there to your ability now.  (As a preview of the next lesson, you can remove the $O$ term and convert the answer to a `float` in one step:  `float(taylor.removeO().replace(x,3*sympy.pi/4))`.)

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ taylor ]

    test_code: |
        points = 0.0

        import sympy
        test_x = sympy.S( 'x' )
        test_taylor = sympy.series( sympy.cos(test_x),test_x,sympy.pi/2,4 )

        try:
            if not type( taylor ) == type( test_taylor ):
                feedback.finish( points,'Your result seems to be the incorrect type.' )
            points += 0.25

            if taylor != test_taylor:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the series expansion.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        taylor = sympy.series(sympy.cos(x),x,sympy.pi/2,4)
        
        # To get the `float` value at 3*pi/4;
        float(sympy.series(sympy.cos(x),x,sympy.pi/2,4).removeO().replace(x,3*sympy.pi/4)) 


-
    type: PythonCodeQuestion
    id: symbolics_calculus__touch_linear
    value: 1
    timeout: 10
    title:  "Checkpoint:  Linearization"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Linearization

        Consider the following function:

        $$
        g( x )
        =
        \cos^{2} x + \cos x
        \text{.}
        $$

        Linearize the function about $x=\frac{\pi}{2}$.  Store your result in a variable `lin`, which should be a `sympy` expression.

        You should remove the spurious $O$ term by calling the method `removeO()` on the result of the linearization.

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ lin ]

    test_code: |
        points = 0.0

        import sympy
        test_x = sympy.S( 'x' )
        test_lin = sympy.series( sympy.cos( test_x )**2 + sympy.cos( test_x ),test_x,sympy.pi/2,2 ).removeO()

        try:
            if not type( lin ) == type( test_lin ):
                feedback.finish( points,'Your result seems to be the incorrect type.' )
            points += 0.25

            if lin != test_lin:
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the series expansion.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        lin = sympy.series( sympy.cos( x )**2 + sympy.cos( x ),x,sympy.pi/2,2 ).removeO()


-
    type: PythonCodeQuestion
    id: symbolics_calculus__touch_lineardiff
    value: 1
    timeout: 10
    title:  "Checkpoint:  Linearization"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Linearization

        Consider the following function, which you linearized in the preceding exercise.

        $$
        g( x )
        =
        \cos^{2} x + \cos x
        \text{.}
        $$

        Having linearized the function about $x=\frac{\pi}{2}$, calculate the difference between the real expression $g$ and the linearized approximation $\hat{g}$.  Store your result in a variable `lindiff`, which should be a `sympy` expression.

        You should remove the spurious $O$ term by calling the method `removeO()` on the result of the linearization.  Also keep in mind that `series` accepts a _number of terms_, not an _order_, so you'll need to think about what _linearizes_ the function (makes it only a function of $x$).

    setup_code: |

    initial_code: |
        import sympy

    names_for_user: [ ]

    names_from_user: [ lindiff ]

    test_code: |
        points = 0.0

        import sympy
        test_x = sympy.S( 'x' )
        test_lin = sympy.series( sympy.cos( test_x )**2 + sympy.cos( test_x ),test_x,sympy.pi/2,2 ).removeO()
        test_lindiff1 = sympy.cos( test_x )**2 + sympy.cos( test_x ) - test_lin
        test_lindiff2 = test_lin - sympy.cos( test_x )**2 + sympy.cos( test_x )

        try:
            if not ((type( lindiff ) == type( test_lindiff1 )) or (type( lindiff ) == type( test_lindiff2))):
                feedback.finish( points,'Your result seems to be the incorrect type.' )
            points += 0.25

            if not ((lindiff == test_lindiff1) or (lindiff == test_lindiff2)):
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the series expansion.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        lin = sympy.series( sympy.cos( x )**2 + sympy.cos( x ),x,sympy.pi/2,2 ).removeO()
        lindiff = sympy.cos( x )**2 + sympy.cos( x ) - lin
