-
    type: Page
    id: symbolics_physics__brick
    content: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Physical Expressions

        Many physical situations have concise and elegant symbolic descriptions.  In this lesson, we examine and solve some of these.

        `sympy` does have a powerful [`sympy.physics`](https://docs.sympy.org/latest/modules/physics/index.html) package which provides more advanced physical descriptions for many systems, including classical and quantum mechanics.  Rather than introduce the new data types and data structures in that package, however, we will focus on simply symbolic descriptions of physical mathematics.

        Since we often need to substitute particular values for symbolic quantities, we can use `subs( symbol,value )` to do so:

            x = sympy.S( 'x' )
            sympy.sqrt( x ).subs( x,2 )

        -   **Fluid Mechanics**.  We consider a few examples describing how fluids flow and what the pressure, velocity, and other characteristics are.

            -   _Barometer_.  A barometer measures atmospheric pressure $p_{\text{atm}}$ by allowing a liquid to adjust to an equilibrium position $h$ where the weight of the liquid $\gamma$ plus its vapor pressure $p_{\text{vapor}}$ balances the downward force of the atmosphere,

                $$
                p_{\text{atm}}
                =
                \gamma h + p_{\text{vapor}}
                \text{.}
                $$

                Represent this equation using SymPy and solve for $\gamma$, the (specific) weight of the liquid which satisfies an observed atmospheric pressure and height $h$.

                    p_atm,g,h,p_vapor = sympy.S( 'p_atm,g,h,p_vapor' )
                    g_soln = sympy.solve( p_atm - ( g * h + p_vapor ),gamma )

                (We can't use `gamma` because of the the $\Gamma$ function, built in to SymPy.)

            -   _Atmospheric pressure by altitude_.  The basic equation for calculating the atmospheric pressure $P$ at various altitudes is

                $$
                P
                =
                P_b \exp \left[ - \frac{0.284 h}{8.314 T_b} \right]
                $$

                where $P_b$ is the baseline pressure (pressure at the Earth's surface), $h$ is the height above sea level, and $T_b$ is the standard temperature, in this case 288.15 K.  All units are SI.

                Plot this equation from $h = 0$ to $h = 11000$ with $P_b = 101325$.

                    h = sympy.S( 'h' )
                    P_eqn = 101325 * sympy.exp( -0.284 * h / ( 8.314 * 288.15 ) )
                    sympy.plotting.plot( P_eqn,( h,0,11000 ) )

            -   _Volumetric rate of flow_.  The amount of liquid $Q$ that flows through a ring of radius $r$ at a velocity $v_z$ can be calculated with an integral:

                $$
                Q
                =
                \int_{r_i}^{r_o} dr\, v_z (2 \pi r)
                \text{.}
                $$

                ![](repo:./img/symbolics_physics__bignug_annulus.png)

                Solve this integral and simplify the result for $Q$.

                    r_i,r_o,r,v_z = sympy.S( 'r_i,r_o,r,v_z' )
                    Q = sympy.integrate( v_z * 2 * sympy.pi * r,( r,r_i,r_o ) )
                    Q_soln = sympy.simplify( Q )

                The maximum rate of flow in this ring will occur where $\frac{\partial v_z}{\partial r} = 0$:

                $$
                r_m
                =
                \left[ \frac{r_o^2 - r_i^2}{2 \log \frac{r_o}{r_i}} \right]^{\frac{1}{2}}
                \text{.}
                $$

                Locate the radius where maximum flow occurs if $r_i = 1$ and $r_o = 2$.

                    r_i,r_o = sympy.S( 'r_i,r_o' )
                    r_m = sympy.sqrt( ( r_o**2 - r_i**2 ) / ( 2 * sympy.log( r_o/r_i ) ) )
                    r_m_soln = r_m.subs( r_o,2 ).subs( r_i,1 )

        -   **Thermodynamics**.  Thermodynamics is a highly symbolic science (and a very practical and numerical application).  Since most of you haven't seen significant thermodynamics coursework yet, I'll simply demonstrate a few solutions without deep explanations.

            -   _Supercooling air_.  The enthalpy of a substance $H$ describes how much heat is needed to maintain some state.  Thermodynamics is frequently concerned with the change in enthalpy $\Delta H$,

                $$
                \Delta H
                =
                \int_{T_1}^{T_2} dT\,C_P
                \text{,}
                $$

                where $T_1$, $T_2$ are temperatures and $C_P$ is the heat capacity.

                Calculate the enthalpy change required to cool air at $101325 \,\text{Pa}$ from $298.15 \,\text{K}$ to $59.63 \,\text{K}$.  For air, $C_P = 29.10$.

                    T_1,T_2,T,C_P = sympy.S( 'T_1,T_2,T,C_P' )
                    dH = sympy.integrate( C_P,( T,T_1,T_2 ) )
                    dH.subs( T_1,298.15 ).subs( T_2,59.63 ).subs( C_P,29.10 )

                (Note that we ignore the possibility of a phase change.)

            -   _Steel casting_.  The entropy $S$ due to a change in conditions can be described as the heat lost due to a process.  (It's better not to think of it as "disorder", a vague concept.)  The change in entropy can be written

                $$
                \Delta S
                =
                m
                \int_{T_1}^{T_2} dT\,\frac{C_P}{T}
                \text{,}
                $$

                where $m$ is the mass.

                Calculate the entropy change due to quenching $40 \,\text{kg}$ of steel at $450 \,\text{C}^\circ$ in oil at $25 \,\text{C}^\circ$.  (The resulting temperature will be $46.52 \,\text{C}^\circ$ for $150 \,\text{kg}$ of oil.)  $C_P = 2500$ in SI units.

                    m,T_1,T_2,T,C_P = sympy.S( 'm,T_1,T_2,T,C_P' )
                    dS = m * sympy.integrate( C_P / T,( T,T_1,T_2 ) )
                    dS.subs( m,40 ).subs( T_1,450+273.15 ).subs( T_2,46.52+273.15 ).subs( C_P,2500 )

                (Note that thermodynamic processes require Kelvin, not degrees centigrade, to be correct.)

        <!--
        -   **Heat transfer**.  The one-dimensional heat conduction equation through an area $A$ is

            $$
            \frac{1}{\alpha}
            \frac{\partial T(x,t)}{\partial t}
            =
            \frac{\partial^2 T(x,t)}{\partial x^2} + \frac{Q}{k}
            $$

            for thermal diffusivity $\alpha$, temperature $T$ as a function of position $x$ and time $t$, and thermal conductivity $k$.

            A solution for this problem requires the values at the boundaries ($x=0$, $x=1$) and at the initial time $t=0$.
        -->

        -   **Orbital mechanics**.  Kepler's third law may be written

            $$
            \frac{T^2}{a^3}
            =
            \frac{4\pi^2}{GM}
            \text{,}
            $$

            where $T$ is the orbital period of the planet; $a$ is the semi-major axis of its orbit; $G$ is the gravitional constant; and $M$ is the mass of the planet.

            ![](repo:./img/symbolics_physics__bignug_planets.png)

                >>> import sympy
                >>> sympy.init_printing()
                >>> T,a,G,M = sympy.S( 'T,a,G,M' )
                >>> sympy.solve( T**2/a**3-4*sympy.pi**2/(G*M),T )
                ⎡          _____           _____⎤
                ⎢         ╱   3           ╱   3 ⎥
                ⎢        ╱   a           ╱   a  ⎥
                ⎢-2⋅π⋅   ╱   ─── , 2⋅π⋅   ╱   ─── ⎥
                ⎣     ╲╱    G⋅M       ╲╱    G⋅M ⎦

                >>> T_func = sympy.lambdify( [ a,G,M ],soln )

            Now solve for the orbital period of the planet Earth, which has an observed orbital period of $$.  The semi-major axis is $1.4960 \times 10^{11} \,\text{m}$; the gravitational constant in this unit system is $6.674 \times 10^{-11} \,\text{m}^3 \text{kg}^{-1} \text{s}^{-2}$; and the mass of the sun is $1.988 \times 10^{30} \,\text{kg}$.

                >>> T_func( 1.496e11,6.674e-11,1.988e30 )
                [-31562832.27326783, 31562832.27326783]

            Taking the physical positive solution, $3.156 \times 10^{7} \,\text{s} \approx 365.31 \,\text{day}$.

        -   **Climate modeling**.  [Notebook available here.](repo:./resources/climate.ipynb)


        #   Units

        Physical quantities have units.  Units are typically organized by length, mass, and time into the SI, Imperial, cgs, and other associated systems.  `sympy` provides the [`sympy.physics.units`](https://docs.sympy.org/latest/modules/physics/units/index.html) package which is organized along the lines of the SI and cgs unit systems.  **I will not test the `units` package on a quiz**, so consider this section informational against future need.

        Basic SI units can be used to obtain values for physical constants such as $c$:

            import sympy
            sympy.init_printing()
            from sympy.physics.units import speed_of_light, meter, second
            speed_of_light
            speed_of_light.convert_to( meter / second )

        Many SI and SI-derived units are defined in `sympy.physics.units`, but rather priggishly they have declined to introduce other unit systems.


        #   References

        -   [Ivan Savov, "Taming Math and Physics Using `SymPy`"](https://minireference.com/static/tutorials/sympy_tutorial.pdf)
