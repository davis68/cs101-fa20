-
    type: PythonCodeQuestion
    id: symbolics_physics__spats_lambdify
    value: 1
    timeout: 10
    title:  "Checkpoint:  Making a Function"
    access_rules:
        add_permissions:
            - see_correctness
            - change_answer

    prompt: |
        {% from "yaml-macros.jinja" import indented_include %}
        {{ indented_include("modules/style.html", 8) }}

        #   Making a Function

        Consider the following expression for $y$:

        $$
        y
        =
        2 \sqrt{x-1}
        \text{.}
        $$

        Convert this symbolic expression into a function named `y_func` using `sympy.lambdify`.

    setup_code: |   

    initial_code: |

    names_for_user: [ ]

    names_from_user: [ y_func ]

    test_code: |
        points = 0.0

        import sympy
        test_x = sympy.S( 'x' )
        test_y = 2 * sympy.sqrt( test_x - 1 )
        test_y_func = sympy.lambdify( [ test_x ],test_y )

        try:
            if not type( y_func ) == type( test_y_func ):
                feedback.finish( points,'Your result seems to be the incorrect type.' )
            points += 0.25

            from math import isclose
            if not isclose( feedback.call_user( y_func,5 ),test_y_func( 5 ) ):
                feedback.finish( points,'Your result seems to be incorrect.' )
            points += 0.75

        except (NameError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Do you have a `SyntaxError` or `NameError`?' )
        except (TypeError,ValueError,IndexError,AttributeError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you using a variable with the wrong type or index, or attempting to use a function that isn\'t defined in a module you `import`ed?' )
        except (RuntimeError,OSError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you trying to use an `input` or `open` statement?' )
        except (RecursionError):
            feedback.finish( points,'Your code doesn\'t seem to be executable at all.  Are you calling a function in its own definition?' )

        if points < 1.0:
            feedback.finish( points,'Check your expression solving the derivative.' )
        feedback.finish( 1.0,'Success!  All values appear to be correct.' )

    correct_code: |
        import sympy
        x = sympy.S( 'x' )
        y = 2 * sympy.sqrt( x - 1 )
        y_func = sympy.lambdify( [ x ],y )
